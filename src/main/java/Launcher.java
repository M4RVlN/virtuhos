import javafx.application.Application;
import javafx.stage.Stage;
import network.tcp.Client;
import network.tcp.Server;
import window.SceneManager;
import window.Scenes;

public class Launcher extends Application {
    @Override
    public void start(Stage primaryStage) {
        SceneManager.Init(primaryStage, "VirtuHoS", Scenes.MAIN_MENU);
        SceneManager.show();

        SceneManager.getStage().setOnCloseRequest(windowEvent -> {
            if(Client.INSTANCE.isConnected())
                Client.INSTANCE.close();
            if(Server.INSTANCE.isRunning())
                Server.INSTANCE.stop();
        });
    }

    public static void main(String[] args) {
        launch(args);
    }
}
