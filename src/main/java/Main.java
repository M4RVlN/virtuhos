import config.AppConfig;
import config.AppProperties;
import java.io.File;
import java.util.Scanner;
import model.House;
import network.tcp.Server;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import util.xml.XMLUtil;

public class Main {
    public static void main(String[] args) {
        AppConfig.loadSettings();
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.OFF);

        //Program arguments
        for (int i = 0; i < args.length; i++) {
            switch (args[i]) {
                case "-headless" -> {
                    if (args.length < (i + 2)) {
                        System.err.println("Missing parameter for house directory. Use -headless <dir>");
                        System.exit(1);
                    }

                    int status = startServerHeadless(new File(args[++i]));
                    System.exit(status);
                }
            }
        }

        Launcher.main(args);
    }

    private static int startServerHeadless(File file) {
        System.out.println("Starting server in headless mode");
        System.out.println("House directory: " + file.getAbsolutePath());

        try {
            House house = XMLUtil.houseFromXML(file);
            Server.INSTANCE.start(AppProperties.SERVER_PORT.getInt(), house);

            System.out.println("Use \"stop\" to stop the server");

            Scanner in = new Scanner(System.in);
            while (in.hasNext()) {
                String cmd = in.next();
                if (cmd.equals("stop")) {
                    Server.INSTANCE.stop();
                    return 0;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }
}
