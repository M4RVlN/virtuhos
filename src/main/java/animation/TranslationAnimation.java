package animation;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.util.Duration;
import util.MathUtil;
import util.Vector2;

public class TranslationAnimation {
    public static final double MIN_DURATION = 250;
    public static final double MAX_DURATION = 1500;

    private final Timeline timeline;

    /**
     * Erstellt eine Animation die eine <code>Node</code> zu einer Postion über eine Dauer bewegt
     * @param node Die <code>Node</code> welche animiert werden soll
     * @param to Position des Endpunkts
     * @param duration Zeitraum der Animation
     */
    public TranslationAnimation(Node node, Vector2 to, Duration duration) {
        this.timeline = new Timeline();
        this.timeline.setCycleCount(1);

        final KeyValue keyX = new KeyValue(node.layoutXProperty(), to.x);
        final KeyValue keyY = new KeyValue(node.layoutYProperty(), to.y);
        final KeyFrame frame = new KeyFrame(duration, keyX, keyY);
        this.timeline.getKeyFrames().add(frame);
    }

    /**
     * Erstellt eine Animation die eine <code>Node</code> zu einer Postion über eine Dauer bewegt
     * @param node Die <code>Node</code> welche animiert werden soll
     * @param to Position des Endpunkts
     * @param val Zeitraum der Animation im Intervall [0, 1] wobei 0 => MAX_DURATION und 1 => MIN_DURATION
     */
    public TranslationAnimation(Node node, Vector2 to, double val) {
        this(node, to, Duration.millis(MathUtil.lerp(MAX_DURATION, MIN_DURATION, val)));
    }

    /**
     * Startet die Animation
     */
    public void play() {
        timeline.play();
    }
}
