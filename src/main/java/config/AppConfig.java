package config;

import java.io.*;
import java.util.Properties;

public final class AppConfig {
    public static final String fileName = "config.properties";

    private static Properties properties;

    public static void saveSettings() {
        File config = new File(fileName);
        try (Writer writer = new FileWriter(config)) {
            properties.store(writer, null);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void loadSettings() {
        File config = new File(fileName);
        if (config.exists() && !config.isDirectory()) {
            try (FileReader reader = new FileReader(config)) {
                properties = new Properties();
                properties.load(reader);

                //Check if properties are missing and add them
                Properties def = getDefaultProperties();
                for(String key : def.stringPropertyNames()) {
                    if(properties.getProperty(key) == null)
                        properties.setProperty(key, def.getProperty(key));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            properties = getDefaultProperties();
            saveSettings();
        }
    }

    private static Properties getDefaultProperties() {
        Properties props = new Properties();

        for (AppProperties prop : AppProperties.values())
        {
            props.setProperty(prop.key, prop.defaultValue);
        }

        return props;
    }

    public static String getString(String key) {
        return properties.getProperty(key);
    }

    public static boolean getBoolean(String key) {
        return Boolean.parseBoolean(properties.getProperty(key));
    }

    public static int getInt(String key) {
        return Integer.parseInt(properties.getProperty(key));
    }

    public static double getDouble(String key) {
        return Double.parseDouble(properties.getProperty(key));
    }
}
