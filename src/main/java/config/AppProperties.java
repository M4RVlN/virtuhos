package config;

public enum AppProperties {
    SERVER_PORT("serverPort", "6969"),
    DB_HOST("dbHost", "localhost"),
    DB_NAME("dbName", "VirtuHoS"),
    DB_PORT("dbPort", "3306"),
    DB_USER("dbUser", ""),
    DB_PASSWORD("dbPassword", ""),
    INTERACTION_HOST("interactionHost", "localhost"),
    INTERACTION_PORT("interactionPort", "8005"),
    BBB_PASSWORD("bbbPassword", "NOMOD"),
    BBB_ADMIN_PASSWORD("bbbAdminPassword", "MOD"),
    BBB_KILL_OLD_MEETINGS("bbbKillOldMeetings", "false");

    public final String key;
    public final String defaultValue;

    AppProperties(String key, String defaultValue) {
        this.key = key;
        this.defaultValue = defaultValue;
    }

    public String getString() {
        return AppConfig.getString(key);
    }

    public boolean getBoolean() {
        return AppConfig.getBoolean(key);
    }

    public int getInt() {
        return AppConfig.getInt(key);
    }

    public double getDouble() {
        return AppConfig.getDouble(key);
    }
}
