package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.HallSortingMode;
import model.Person;
import model.Room;
import network.database.AnalysisCircle;
import network.database.AnalysisData;
import network.database.AnalysisLine;
import network.tcp.Client;
import network.tcp.events.AnalysisDataRequest;
import network.tcp.events.HallRegroupRequest;
import util.Vector2;
/**
 * Diese Klasse dient als Schnittstelle der GUI und den anderen Teilprojekten. In dieser Klasse
 * ist es möglich die Analyse anzuzeigen und wieder zu entfernen oder die Personen in der Halle
 * nach den Kriterien der WB-Halle-3 zu sortieren.
 *
 * */

public class AdminController extends ClientRenderController {

    @FXML private Button AnalyseShowButton;
    @FXML private Button AnalyseCloseButton;
    @FXML private Button CreateAccountButton;
    @FXML private Button DeleteAccountButton;
    @FXML private HBox AnalyseInfo;

    private boolean isAnalyseShown = false;

    private final HashMap<AnalysisLine, Line> analysisLineNodes = new HashMap<>();
    private final HashMap<AnalysisCircle, Circle> analysisCircleNodes = new HashMap<>();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
        AnalyseShowButton.setDisable(false);
        AnalyseCloseButton.setDisable(true);
        AnalyseInfo.setVisible(false);

        CreateAccountButton.setOnAction(e-> {
            Stage newStage = new Stage();

            newStage.initModality(Modality.WINDOW_MODAL);
            newStage.initStyle(StageStyle.UTILITY);
            newStage.initOwner(((Button) e.getSource()).getScene().getWindow());
            
            FXMLLoader fl = new FXMLLoader(ClassLoader.getSystemResource("view/CreateAccountView.fxml"));
            try {
                Parent root = fl.load();
                newStage.setScene(new Scene(root));
                newStage.show();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

        });
        DeleteAccountButton.setOnAction(e-> {
            Stage newStage = new Stage();

            newStage.initModality(Modality.WINDOW_MODAL);
            newStage.initStyle(StageStyle.UTILITY);
            newStage.initOwner(((Button) e.getSource()).getScene().getWindow());

            FXMLLoader fl = new FXMLLoader(ClassLoader.getSystemResource("view/DeleteAccountView.fxml"));
            try {
                Parent root = fl.load();
                newStage.setScene(new Scene(root));


                newStage.show();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

        });
    }

    @Override
    public void onPersonMove(Person person, Vector2 pos, boolean animation) {
        super.onPersonMove(person, pos, animation);

        if(isAnalyseShown)
            updateAnalysis();
    }

    @Override
    public void onPersonLeave(Person person) {
        super.onPersonLeave(person);

        if(isAnalyseShown)
            removeAnalysis(person.getId());
    }

    @Override
    public boolean isEditor() {
        return false;
    }

    /**
     * Schnittstelle um Analyse anzuzeigen
     * */
    public void showAnalyse() {
        Client.INSTANCE.sendEvent(new AnalysisDataRequest(null));
    }
    /**
     * Ruft von gegebenen Daten die Zeichnenfunktion auf
     *
     * @param data, die Daten, die von der DB geholt werden um Personen zu verbinden und einzufärben
     * */
    public void renderAnalysis(AnalysisData data) {
        if(isAnalyseShown)
            return;
        isAnalyseShown = true;

        //Enable/Disable buttons
        AnalyseShowButton.setDisable(true);
        AnalyseCloseButton.setDisable(false);
        AnalyseInfo.setVisible(true);

        //Draw data
        for(AnalysisLine line : data.getLines()) {
            drawLine(line);
        }
        for(AnalysisCircle circle : data.getCircles()) {
            drawCircle(circle);
        }
    }
    /**
     * Entfernt die Zeichnung , die durch renderAnalysis hervorgerufen wurde
     *
     * */
    public void removeAnalysis() {
        if(!isAnalyseShown)
            return;
        isAnalyseShown = false;

        //Clear data
        for(Line shape : analysisLineNodes.values()) {
            rendererCanvas.getChildren().remove(shape);
        }
        for(Circle shape : analysisCircleNodes.values()) {
            rendererCanvas.getChildren().remove(shape);
        }
        analysisLineNodes.clear();
        analysisCircleNodes.clear();

        //Enable/Disable buttons
        AnalyseShowButton.setDisable(false);
        AnalyseCloseButton.setDisable(true);
        AnalyseInfo.setVisible(false);
    }
    /**
     * Entfernt die Anzeige der Analysis anhand der UUID
     *
     * @param personId , ID der Person um die Analyse zu entfernen
     * */
    public void removeAnalysis(UUID personId) {
        Iterator<Map.Entry<AnalysisLine, Line>> lineIterator = analysisLineNodes.entrySet().iterator();
        Iterator<Map.Entry<AnalysisCircle, Circle>> circleIterator = analysisCircleNodes.entrySet().iterator();

        //Remove lines
        while(lineIterator.hasNext()) {
            Map.Entry<AnalysisLine, Line> entry = lineIterator.next();
            AnalysisLine lineData = entry.getKey();

            if(lineData.getLineIdFrom().equals(personId) || lineData.getLineIdTo().equals(personId)) {
                rendererCanvas.getChildren().remove(entry.getValue());
                lineIterator.remove();
            }
        }
        //Remove Circles
        while(circleIterator.hasNext()) {
            Map.Entry<AnalysisCircle, Circle> entry = circleIterator.next();
            AnalysisCircle circleData = entry.getKey();

            if(circleData.getCircleId().equals(personId)) {
                rendererCanvas.getChildren().remove(entry.getValue());
                circleIterator.remove();
            }
        }
    }
    /**
     * Updatefunktion für die Analysisanzeige
     * */
    public void updateAnalysis() {
        //Update lines
        for(Map.Entry<AnalysisLine, Line> entry : analysisLineNodes.entrySet()) {
            Vector2 from = getHouse().getPerson(entry.getKey().getLineIdFrom()).getPosition();
            Vector2 to = getHouse().getPerson(entry.getKey().getLineIdTo()).getPosition();
            Line line = entry.getValue();
            line.setStartX(from.x);
            line.setStartY(from.y);
            line.setEndX(to.x);
            line.setEndY(to.y);
        }
        //Update circles
        for(Map.Entry<AnalysisCircle, Circle> entry : analysisCircleNodes.entrySet()) {
            Vector2 pos = getHouse().getPerson(entry.getKey().getCircleId()).getPosition();
            Circle circle = entry.getValue();
            circle.setCenterX(pos.x);
            circle.setCenterY(pos.y);
        }
    }

    /**
     * Zeichnet eine Linie zwischen zwei Personen mit gegebenen Dicke und Farbe
     * @param data Daten für die Linie
     * */
    public void drawLine(AnalysisLine data) {
        Person personFrom = getHouse().getPerson(data.getLineIdFrom());
        Person personTo = getHouse().getPerson(data.getLineIdTo());

        if(personFrom == null || personTo == null)
            return;

        Vector2 from = personFrom.getPosition();
        Vector2 to = personTo.getPosition();
        //Erstellt die Linie mit den Parameter
        Line l = new Line (from.x, from.y, to.x, to.y);
        l.setStrokeWidth(data.getLineWidth());
        l.setStroke(data.getLineColor());

        //Linie und Kreis werden hinzufügt
        rendererCanvas.getChildren().add(l);
        analysisLineNodes.put(data, l);
    }
    /**
     * Zeichnet einen Kreis auf die Person aus der data
     *
     * @param data ,Daten aus der DB um Personen einzuordnen
     * */
    public void drawCircle(AnalysisCircle data) {
        Person person = getHouse().getPerson(data.getCircleId());

        if(person == null)
            return;

        Vector2 pos = person.getPosition();

        //ersatz, analysekreis wird auf person draufgelegt
        Circle c = new Circle(Person.PERSON_SIZE.x / 2);
        c.setCenterX(pos.x);
        c.setCenterY(pos.y);
        c.setFill(data.getCirceColor());

        rendererCanvas.getChildren().add(c);
        analysisCircleNodes.put(data, c);
    }

    /**
     *  Entfernt alle Linie und Kreise die durch Analyse erzeugt wurden
     * */
    public void removeShowAnalyse(){
        removeAnalysis();
    }

    /**
     * Schnittstelle für Halle für Random Sort
     * */
    public void zufallSort() {
        ArrayList<Room> hallList = getHouse().getHallList();
        //Send Mode-Event to every Hall
        for (Room hall : hallList) {
            Client.INSTANCE.sendEvent(new HallRegroupRequest(null, hall.getId(), HallSortingMode.RANDOM));
        }
    }

    /**
     * Schnittstelle für Halle um User nach hoher Bindung zu sortieren
     * */
    public void highSort() {
        ArrayList<Room> hallList = getHouse().getHallList();
        //Send Mode-Event to every Hall
        for (Room hall : hallList) {
            Client.INSTANCE.sendEvent(new HallRegroupRequest(null, hall.getId(), HallSortingMode.USUAL));
        }
    }

    /**
     * Schnittstelle für Halle um User nach niedriger Bindung zu sortieren
     * */
    public void lowSort() {
        ArrayList<Room> hallList = getHouse().getHallList();
        //Send Mode-Event to every Hall
        for (Room hall : hallList) {
            Client.INSTANCE.sendEvent(new HallRegroupRequest(null, hall.getId(), HallSortingMode.INVERSE));
        }
    }
}
