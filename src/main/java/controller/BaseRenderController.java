package controller;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.UUID;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import model.Chair;
import model.Content;
import model.House;
import model.HouseElement;
import model.Office;
import model.OfficeType;
import model.Room;
import model.Table;
import util.Vector2;
import util.xml.XMLUtil;
import window.SceneManager;

/**
 * Basisklasse zum Rendern und Bearbeiten der Gebäudeansicht.
 * Benötigt eine ScrollPane mit fx:id "rendererScrollPane" und als Kind eine AnchorPane mit fx:id "rendererCanvas"
 * um das Gebäude zu rendern.
 */
public abstract class BaseRenderController extends Controller implements Initializable {
    public static final double resizeCornerSize = 8;
    public static final double documentHeight = 22;
    public static final double documentWidth = 16;
    public static final double textOffset = 8;
    public static final double fontSize = 18;
    public static final double fontSizePerson = 18;
    public static final double edgeOffset = 1;

    public static final int GRID_SIZE = 40;
    public static final double maxRoomWidth = 1000;
    public static final double maxRoomHeight = 1000;

    private final HashMap<UUID, Group> nodes = new HashMap<>();
    protected House house;

    @FXML
    protected AnchorPane rendererCanvas;

    @FXML
    protected ScrollPane rendererScrollPane;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            loadHouse(XMLUtil.defaultHouseTemplate());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Bestimmt ob die Editor-Funktionen aktiviert werden
     * @return true wenn es sich um den Editor handelt, sonst false
     */
    public abstract boolean isEditor();

    protected HashMap<UUID, Group> getNodes() {
        return nodes;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;

        getNodes().clear();
        rendererCanvas.getChildren().clear();

        rendererCanvas.setPrefSize(house.getSize().x, house.getSize().y);
    }

    public void renderHouse() {
        for(Room r : house.getRoomList()) {
            addRoomRenderer(r);
            for(Content c : r.getContent()) {
                if(c instanceof Table) {
                    addTableRenderer((Table)c, r);
                } else if(c instanceof Chair) {
                    addChairRenderer((Chair) c, r);
                }
            }
        }
    }


    /**
     * Initialisiert den Renderer mit der momentanen House-Instanz
     * @param house Die Instanz des Hauses
     */
    public void loadHouse(House house) {
        setHouse(house);
        renderHouse();
    }

    /**
     * Überprüft ob bereits ein Haus mit LoadHouse geladen wurde
     */
    public boolean isHouseLoaded() {
        return house != null;
    }

    /**
     * Fügt der Renderansicht einen Raum hinzu
     * @param room Gibt die Eigenschaften für die Darstellung des Raums an
     */
    protected Group addRoomRenderer(Room room) {
        Vector2 pos = room.getPosition();
        Vector2 size = room.getSize();

        //Create UI node
        Group roomRenderGroup = new Group();
        roomRenderGroup.setLayoutX(pos.x);
        roomRenderGroup.setLayoutY(pos.y);

        Rectangle shape = new Rectangle(size.x, size.y);
        roomRenderGroup.getChildren().add(shape);

        Text text = new Text();
        roomRenderGroup.getChildren().add(text);
        Font font = Font.font("Arial Bold", fontSize);
        text.setFont(font);
        text.setTextAlignment(TextAlignment.LEFT);
        text.setText(room.getName());
        text.setLayoutX(textOffset);
        text.setLayoutY(size.y - textOffset);
        text.setFill(Paint.valueOf("#ffffff"));
        text.setStrokeWidth(0.7);
        text.setStroke(Paint.valueOf("#000000"));

        Rectangle corner = new Rectangle(resizeCornerSize, resizeCornerSize);
        roomRenderGroup.getChildren().add(corner);
        corner.setLayoutX(size.x - resizeCornerSize);
        corner.setLayoutY(size.y - resizeCornerSize);

        //Style
        shape.getStyleClass().addAll("Room", room.getClass().getSimpleName());

        if(room instanceof Office && ((Office) room).getType() == OfficeType.CONFERENCE)
            shape.getStyleClass().add("Conference");

        corner.getStyleClass().add("resize_corner");

        if(isEditor())
            shape.getStyleClass().add("movable");
        else
            corner.setVisible(false);

        rendererCanvas.getChildren().add(roomRenderGroup);
        nodes.put(room.getId(), roomRenderGroup);
        return roomRenderGroup;
    }

    /**
     * Fügt der Renderansicht einen Tisch hinzu
     * @param table Gibt die Eigenschaften für die Darstellung des Tisches an
     * @param room Gibt den Raum an in den der Stuhl platziert wird
     */
    protected Group addTableRenderer(Table table, Room room) {
        Vector2 pos = table.getPosition();
        Vector2 size = table.getSize();

        Group tableRenderGroup = new Group();

        //Attach to Room
        Group roomNode = nodes.get(room.getId());
        roomNode.getChildren().add(roomNode.getChildren().size() - 2, tableRenderGroup);

        //Set Position
        tableRenderGroup.setLayoutX(pos.x - roomNode.getLayoutX());
        tableRenderGroup.setLayoutY(pos.y - roomNode.getLayoutY());

        Rectangle shape = new Rectangle(size.x, size.y);
        tableRenderGroup.getChildren().add(shape);

        //Render Documents
        Rectangle document = new Rectangle(documentWidth, documentHeight);
        tableRenderGroup.getChildren().add(document);
        document.setLayoutX(size.x - ((size.x + documentWidth) / 2));
        document.setLayoutY(size.y - ((size.y + documentHeight) / 2));

        Rectangle add_document = new Rectangle(documentWidth, documentWidth);
        tableRenderGroup.getChildren().add(add_document);
        add_document.setLayoutX(size.x - ((size.x + documentWidth) / 2));
        add_document.setLayoutY(size.y - ((size.y + documentWidth) / 2));

        //Render resize corner
        Rectangle corner = new Rectangle(resizeCornerSize, resizeCornerSize);
        tableRenderGroup.getChildren().add(corner);
        corner.setLayoutX(size.x - resizeCornerSize);
        corner.setLayoutY(size.y - resizeCornerSize);

        //Style
        shape.getStyleClass().addAll("Table");
        corner.getStyleClass().add("resize_corner");
        document.getStyleClass().add("Document");
        add_document.getStyleClass().add("addDocument");

        if (this.isEditor()) {
            add_document.setVisible(false);
            document.setVisible(false);
            shape.getStyleClass().add("movable");
        } else {
            corner.setVisible(false);
            document.setVisible(true); //true zum aktivieren
            add_document.setVisible(false);
        }

        nodes.put(table.getId(), tableRenderGroup);
        return tableRenderGroup;
    }

    /**
     * Fügt der Renderansicht einen Stuhl hinzu
     * @param chair Gibt die Eigenschaften für die Darstellung des Stuhls an
     * @param room Gibt den Raum an in den der Stuhl platziert wird
     */
    protected Group addChairRenderer(Chair chair, Room room) {
        Vector2 pos = chair.getPosition();
        Vector2 size = chair.getSize();

        Group chairRenderGroup = new Group();

        //Attach to Room
        Group roomNode = nodes.get(room.getId());
        roomNode.getChildren().add(roomNode.getChildren().size() - 2, chairRenderGroup);

        //Set Position
        chairRenderGroup.setLayoutX(pos.x - roomNode.getLayoutX());
        chairRenderGroup.setLayoutY(pos.y - roomNode.getLayoutY());

        Circle shape = new Circle(size.x / 2);
        shape.setCenterX(size.x / 2);
        shape.setCenterY(size.x / 2);
        chairRenderGroup.getChildren().add(shape);

        //Style
        shape.getStyleClass().addAll("Chair");

        if(isEditor())
            shape.getStyleClass().add("movable");

        nodes.put(chair.getId(), chairRenderGroup);
        return chairRenderGroup;
    }

    /**
     * Erneuert die Parameter für die Position und Skalierung des Raums in der Renderansicht
     * @param element Die neuen Parameter für das Element
     */
    protected boolean updateRenderer(HouseElement element) {
        Vector2 pos = element.getPosition();
        Vector2 size = element.getSize();
        Group node = nodes.get(element.getId());

        //Update group position
        node.setLayoutX(pos.x - node.getParent().getLayoutX());
        node.setLayoutY(pos.y - node.getParent().getLayoutY());

        //Update size of shape
        Node shape = node.getChildren().get(0);
        if(shape instanceof Rectangle) {
            ((Rectangle)shape).setWidth(size.x);
            ((Rectangle)shape).setHeight(size.y);
        } else if(shape instanceof Circle) {
            ((Circle)shape).setRadius(size.x / 2);
        } else {
            return false;
        }

        if(element instanceof Room || element instanceof Table) {
            //Update position of resize corner
            Rectangle resizeCorner = (Rectangle) node.getChildren().get(node.getChildren().size() - 1);
            resizeCorner.setLayoutX(size.x - resizeCornerSize);
            resizeCorner.setLayoutY(size.y - resizeCornerSize);

            //Update Room
            if(element instanceof Room) {
                //Update text
                Text text = (Text) node.getChildren().get(node.getChildren().size() - 2);
                text.setLayoutX(textOffset);
                text.setLayoutY(size.y - textOffset);
                text.setText(((Room) element).getName());

                //Update content
                for(Content c : ((Room)element).getContent()) {
                    updateRenderer(c);
                }
            }
        }
        
        return true;
    }

    protected void changeOfficeType(Office office, OfficeType type) {
        if(office.getType() == type)
            return;

        //Change type in model
        office.setType(type);

        //Change type in renderer
        Group node = nodes.get(office.getId());
        if(node != null) {
            Rectangle shape = (Rectangle)node.getChildren().get(0);
            switch (type) {
                case CONFERENCE -> {
                    shape.getStyleClass().add("Conference");
                    office.setName("Konferenz");
                }
                case DEFAULT -> {
                    shape.getStyleClass().remove("Conference");
                    office.setName("B\u00fcro");
                }
            }
        }
        updateRenderer(office);
    }

    /**
     * Entfernt ein Element von der Renderansicht
     * @param element Das Element der entfernt werden soll
     */
    protected boolean removeRenderer(HouseElement element) {
        Node node = nodes.get(element.getId());

        if(node == null)
            return false;

        Parent parent = node.getParent();
        if(parent instanceof AnchorPane) {
            ((AnchorPane)parent).getChildren().remove(node);
        } else if(parent instanceof Group) {
            ((Group)parent).getChildren().remove(node);
        } else {
            return false;
        }

        nodes.remove(element.getId());
        return true;
    }

    public static Vector2 screenToSceneSpace(Vector2 pos) {
        Stage stage = SceneManager.getStage();
        Vector2 someOffset = new Vector2(3, -28);
        return new Vector2(pos.x - stage.getX() + someOffset.x, pos.y - stage.getY() + someOffset.y);
    }

    /**
     * Transformiert die Screen Space Position in die Render Space Position unter Berücksichtigung der ScrollPane
     * @param pos Position in Screen Space
     * @return Position in Render Space
     */
    public Vector2 sceneToRenderSpace(Vector2 pos) {
        double xOffset = rendererScrollPane.getHvalue() * (rendererCanvas.getLayoutBounds().getWidth() - rendererScrollPane.getViewportBounds().getWidth());
        double yOffset = rendererScrollPane.getVvalue() * (rendererCanvas.getLayoutBounds().getHeight() - rendererScrollPane.getViewportBounds().getHeight());
        return new Vector2(pos.x + xOffset - rendererScrollPane.getLayoutX(), pos.y + yOffset - rendererScrollPane.getLayoutY());
    }

    /**
     * Transformiert die Screen Space Position in die Render Space Position unter Berücksichtigung der ScrollPane und
     * der Größe des zu bewegenden Objekts um dieses zu zentrieren.
     * @param pos Position in Screen Space
     * @param shapeSize Größe des Objekts
     * @return Zentrierte Position in Render Space
     */
    public Vector2 sceneToRenderSpace(Vector2 pos, Vector2 shapeSize) {
        return sceneToRenderSpace(new Vector2(pos.x - (shapeSize.x / 2), pos.y - (shapeSize.y / 2)));
    }

    /**
     * Zieht die Position an das Gitter
     * @param pos Originale Position
     * @return Positon auf dem Gitter
     */
    public Vector2 toGridPosition(Vector2 pos) {
        return new Vector2(Math.floor(pos.x / GRID_SIZE) * GRID_SIZE,
                Math.floor(pos.y / GRID_SIZE) * GRID_SIZE);
    }
}
