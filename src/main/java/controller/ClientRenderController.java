package controller;

import animation.TranslationAnimation;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.OverrunStyle;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import model.Chair;
import model.Content;
import model.House;
import model.Person;
import model.Room;
import model.Table;
import network.tcp.Client;
import network.tcp.events.DocumentCreateRequest;
import network.tcp.events.DocumentGetInfoRequest;
import network.tcp.events.PersonMoveRequest;
import util.CollisionMath;
import util.Vector2;
import window.SceneManager;
import window.Scenes;

public class ClientRenderController extends BaseRenderController {
    private final HashMap<UUID, Group> userNodes = new HashMap<>();
    private UUID myId;

    private final ContextMenu docContextMenu = new ContextMenu();
    private Group docTable;
    private double contextScreenX;
    private double contextScreenY;

    @FXML
    private Slider Hall_Slider;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    /**
     * Initialisiert den Controller
     * @param house Das Model des Hauses
     * @param myId ID des eigenen <code>Person</code> Objekts
     */
    public void init(House house,  UUID myId) {
        setMyId(myId);
        loadHouse(house);
    }

    /**
     * Gibt die ID für den lokalen Benutzer aus
     * @return ID des eigenen <code>Person</code> Objekts
     */
    public UUID getMyId() {
        return myId;
    }

    /**
     * Setzt die ID für den lokalen Benutzer
     * @param myId ID des eigenen <code>Person</code> Objekts
     */
    public void setMyId(UUID myId) {
        this.myId = myId;
    }

    @Override
    public void renderHouse() {
        super.renderHouse();
        for(Room r : house.getRoomList()) {
            for (Person p : r.getUsers()) {
                addPersonRenderer(p);
            }
        }
    }

    @Override
    protected Group addTableRenderer(Table table, Room room) {
        Group tableGroup = super.addTableRenderer(table,room);

        tableGroup.setOnContextMenuRequested(event -> {
            contextScreenX = event.getScreenX();
            contextScreenY = event.getScreenY();
            if (house.getRoom(house.getPerson(myId)) == room) {
                docTable = tableGroup;
                getDocContextMenu().getItems().clear();
                MenuItem addDoc = new MenuItem("Dokument hinzufügen");
                addDoc.setOnAction(event2 -> {
                    FileChooser fileChooser = new FileChooser();
                    fileChooser.setTitle("Dokument auswählen");
                    try {
                        File filesrc = fileChooser.showOpenDialog(SceneManager.getStage());
                        if(filesrc == null) return;

                        byte[] data = Files.readAllBytes(Path.of(filesrc.getAbsolutePath()));

                        String b64_data = Base64.getEncoder().encodeToString(data);
                        Client.INSTANCE.sendEvent(new DocumentCreateRequest(null, b64_data, filesrc.getName(), myId));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
                getDocContextMenu().getItems().addAll(addDoc, new SeparatorMenuItem());
                Client.INSTANCE.sendEvent(new DocumentGetInfoRequest(null, room.getId()));
            }
        });

        return tableGroup;
    }


    /**
     * Fügt der Renderansicht eine Person hinzu
     *
     * @param person Gibt die Eigenschaften für die Darstellung des Raums an
     */
    protected void addPersonRenderer(Person person) {
        Vector2 pos = person.getPosition();
        Vector2 size = person.getSize();

        Group personRenderGroup = new Group();

        // Set Position
        personRenderGroup.setLayoutX(pos.x);
        personRenderGroup.setLayoutY(pos.y);

        Ellipse face = new Ellipse(size.x / 2, size.y / 2);

        personRenderGroup.getChildren().add(face);

        Tooltip tooltip = new Tooltip(person.getName());
        tooltip.setFont(Font.font("Arial Bold", 12));
        Tooltip.install(personRenderGroup, tooltip);

        // Person name anzeigen
        Label name = new Label(person.getName().substring(0,Math.min(2, person.getName().length())));
        Font font = Font.font("Arial Bold", fontSizePerson);
        name.setFont(font);
        name.setTextAlignment(TextAlignment.CENTER);
        name.setAlignment(Pos.CENTER);
        name.setLayoutX(-size.x / 2);
        name.setLayoutY(size.y / 2);
        name.setMaxWidth(size.x);
        name.setMinWidth(size.x);
        name.setWrapText(true);
        name.setTextOverrun(OverrunStyle.CENTER_ELLIPSIS);
        name.getStyleClass().add("Person-Name");

        personRenderGroup.getChildren().add(name);

        // Style
        face.getStyleClass().add("Person");

        if(person.getId().equals(getMyId()))
            face.getStyleClass().add("Self");
        else if(person.isAdmin())
            face.getStyleClass().add("Admin");


        rendererCanvas.getChildren().add(personRenderGroup);
        userNodes.put(person.getId(), personRenderGroup);

        // Events
        if(this instanceof UserController && !person.getId().equals(getMyId()))
            return;

        personRenderGroup.addEventFilter(MouseEvent.MOUSE_PRESSED, mouseEvent -> OnMouseDown(mouseEvent, person));
        personRenderGroup.addEventFilter(MouseEvent.MOUSE_DRAGGED, mouseEvent -> OnMouseDragged(mouseEvent, person));
        personRenderGroup.addEventFilter(MouseEvent.MOUSE_RELEASED, mouseEvent -> OnMouseUp(mouseEvent, person));
    }

    /**
     * Erneuert die Position der Person in der Renderansicht mit Animation
     *
     * @param person Die Person, deren Position erneuert werden soll
     * @param pos    Die Position an der die Person angezeigt werden soll
     */
    protected void translatePersonRenderer(Person person, Vector2 pos) {
        Group personRenderGroup = userNodes.get(person.getId());
        TranslationAnimation animation = new TranslationAnimation(personRenderGroup, pos, Hall_Slider.getValue());
        animation.play();
    }

    /**
     * Erneuert die Position der Person in der Renderansicht unabhängig vom Model
     *
     * @param person Die Person, deren Position erneuert werden soll
     * @param pos    Die Position an der die Person angezeigt werden soll
     */
    protected void updatePersonRenderer(Person person, Vector2 pos) {
        Group personRenderGroup = userNodes.get(person.getId());
        personRenderGroup.setLayoutX(pos.x);
        personRenderGroup.setLayoutY(pos.y);
    }

    /**
     * Erneuert die Position der Person in der Renderansicht
     *
     * @param person Die Person, deren Position erneuert werden soll
     */
    public void updatePersonRenderer(Person person) {
        Vector2 pos = person.getPosition();
        updatePersonRenderer(person, pos);
    }

    /**
     * Entfernt die Person aus der Renderansicht
     *
     * @param person Die zu entfernende Person
     */
    protected void removePersonRenderer(Person person) {
        Group node = userNodes.get(person.getId());

        if (node == null)
            return;

        rendererCanvas.getChildren().remove(node);

        userNodes.remove(person.getId());
    }

    /**
     * Event um eine neue Person dem Model und der Ansicht hinzuzufügen
     *
     * @param person Die Person, die hinzugefügt werden soll
     * @return Gibt an, ob das hinzufügen der Person erfolgreich war
     */
    public boolean onPersonJoin(Person person) {
        if(getHouse().addPerson(person)) {
            addPersonRenderer(person);
            return true;
        }
        return false;
    }

    /**
     * Event um eine Person im Model und der Ansicht zu bewegen
     *
     * @param person Die Person, die bewegt werden soll
     * @param pos Die neue Position der Person
     * @param animation Soll die Bewegung animiert werden
     */
    public void onPersonMove(Person person, Vector2 pos, boolean animation) {
        getHouse().movePerson(person, pos);

        if(animation)
            translatePersonRenderer(person, pos);
        else
            updatePersonRenderer(person, pos);
    }

    /**
     * Event um eine Person aus dem Model und der Ansicht zu entfernen
     *
     * @param person Die Person, die entfernt werden soll
     */
    public void onPersonLeave(Person person) {
        getHouse().removePerson(person);
        removePersonRenderer(person);
    }

    /**
     * Schnittstelle um Server zu verlassen
     * */
    public void logoutPressed() {
        Client.INSTANCE.close();
        SceneManager.loadScene(Scenes.MAIN_MENU);
    }

    @Override
    public boolean isEditor() {
        return false;
    }

    // =============
    // Person Events
    // =============

    private Vector2 dragEventStartPosition;

    /**
     * Handler des Startevents für das verschieben einer Person in der Renderansicht
     *
     * @param mouseEvent Parameter des Auslöserevents
     * @param person     Die Person, welche verschoben wird
     */
    public void OnMouseDown(MouseEvent mouseEvent, Person person) {
        // Verschieben nur bei Linksklick
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            dragEventStartPosition = person.getPosition();
            mouseEvent.consume();
        }
    }

    /**
     * Handler des Events während des Verschiebens einer Person in der Renderansicht
     *
     * @param mouseEvent Parameter des Auslöserevents
     * @param person     Die Person, welche verschoben wird
     */
    public void OnMouseDragged(MouseEvent mouseEvent, Person person) {
        // Verschieben nur bei Linksklick
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            Vector2 mousePos = sceneToRenderSpace(new Vector2(mouseEvent.getSceneX(), mouseEvent.getSceneY()));

            updatePersonRenderer(person, mousePos);
            mouseEvent.consume();
        }
    }

    /**
     * Handler des Schlussevents für das Verschieben einer Person in der Renderansicht
     *
     * @param mouseEvent Parameter des Auslöserevents
     * @param person     Die Person, welche verschoben wird
     */
    public void OnMouseUp(MouseEvent mouseEvent, Person person) {
        // Verschieben nur bei Linksklick
        if (mouseEvent.getButton() == MouseButton.PRIMARY) {
            Vector2 pos = sceneToRenderSpace(new Vector2(mouseEvent.getSceneX(), mouseEvent.getSceneY()));

            pos = avoidCollision(person, pos);
            pos = movePersonAtRoomEdge(person, pos);
            pos = sitPersonOnChair(person, pos);

            Client.INSTANCE.sendEvent(new PersonMoveRequest(null, person.getId(), pos.x, pos.y));

            mouseEvent.consume();
        }
    }

    /**
     * Sucht eine passende Position, die möglichst nahe der ursprünglichen Position ist.
     * @param person Die Person, welche verschoben wird
     * @param pos Die Position an welche die Person verschoben werden soll
     * @return Vector2 Die neue Position, ohne Kollisionen. pos, falls keine gefunden wurde oder es keine Kollision gab
     */
    private Vector2 avoidCollision(Person person, Vector2 pos){
        if(getHouse().getRoom(pos) == null) return pos;

        Vector2 delta;
        boolean invalidPosition;
        int loopCounter = 0;
        CollisionMath.GoldenSpiral gs = CollisionMath.newGoldenSpiral();
        double personRadius = person.getSize().y / 2;
        Room originalRoom = getHouse().getRoom(pos);

        do {
            delta = gs.next(); //neue offset der Goldenen Spirale
            invalidPosition = false; //wird true, falls es eine Kollision gibt

            //Suche nach Tischen, mit denen es eine Kollision gibt
            for (Content c : getHouse().getRoom(pos).getContent()) {
                if(!(c instanceof Table)) continue; //Alles außer Tische ignorieren

                if (CollisionMath.overlapCircleRectangle(pos.add(delta), personRadius, c.getPosition(), c.getSize())) {
                    invalidPosition = true;
                    break;
                }
            }

            //Suche nach Personen, mit denen es eine Kollision gibt
            for (Person p : getHouse().getRoom(pos).getUsers()) {
                if (p == person) continue; //Sich selbst ignorieren

                if (CollisionMath.overlapCircleCircle(pos.add(delta), personRadius, p.getPosition(), personRadius)) {
                    invalidPosition = true;
                    break;
                }
            }
            if(loopCounter++ > 512) return pos;
        }
        //Wiederholen, bis passende Position im aktuellen Raum gefunden.
        while (invalidPosition || getHouse().getRoom(pos.add(delta)) != originalRoom);

        return pos.add(delta);
    }

    /**
     * Passt die Ziel Position an, falls die Person auf einen Stuhl gezogen wurde
     * @param person Die Person, die verschoben werden soll
     * @param pos Die eigentliche Position, an welche die Person geschoben werden soll
     * @return Vector2 Die Position des Stuhls, auf dem die Person gezogen wurde
     */
    private Vector2 sitPersonOnChair(Person person, Vector2 pos){
        if(getHouse().getRoom(pos) == null) return pos;
        for (Content c : getHouse().getRoom(pos).getContent()) {
            if (c instanceof Chair && c.isInBounds(pos)) {
                return c.getPosition().add(new Vector2(Chair.CHAIR_SIZE.x / 2, Chair.CHAIR_SIZE.y / 2));
            }
        }

        return pos;
    }

    /**
     * Passt die target Position an, um eine Person die an den Rand eines Raums geschoben wurde, mit passenden Abstand zum Rand zu positionieren.
     * Falls Person bereits passend in einem Raum ist oder target sich in keinen Raum befindet, wird die ursprüngliche Position zurückgegeben.
     *
     * @param person Die Person, die verschoben werden soll
     * @param target Die eigentliche Position, an welche die Person geschoben werden soll
     * @return Vector2 Eine neue Position, welche die Raumränder beachtet.
     */
    private Vector2 movePersonAtRoomEdge(Person person, Vector2 target) {
        Room room = getHouse().getRoom(target);
        if (room != null) {
            Vector2 posInRoom = target.sub(room.getPosition());
            //Rand Links
            if (posInRoom.x - edgeOffset < person.getSize().x / 2) {
                target.x += person.getSize().x / 2 - posInRoom.x + edgeOffset;
            }
            //Rand Oben
            if (posInRoom.y - edgeOffset < person.getSize().x / 2) {
                target.y += person.getSize().y / 2 - posInRoom.y + edgeOffset;
            }
            //Rand Rechts
            if (posInRoom.x + person.getSize().x / 2 > room.getSize().x - edgeOffset) {
                target.x -= posInRoom.x + person.getSize().x / 2 - room.getSize().x + edgeOffset;
            }
            //Rand Unten
            if (posInRoom.y + person.getSize().y / 2 > room.getSize().y - edgeOffset) {
                target.y -= posInRoom.y + person.getSize().y / 2 - room.getSize().y + edgeOffset;
            }
            return target;
        }
        return target; //falls target in keinem Raum liegt
    }

    /**
     * Gibt die Node zurück, an der die Dokumente UI angefragt wurden
     * @return Das Group-Objekt, des Tisches
     */
    public Group getDocTable() {
        return docTable;
    }

    /**
     * Gibt die X Koordinate der angefragten Dokumente UI zurück
     * @return Die X Koordinate
     */
    public double getContextScreenX() {
        return contextScreenX;
    }

    /**
     * Gibt die Y Koordinate der angefragten Dokumente UI zurück
     * @return Die Y Koordinate
     */
    public double getContextScreenY() {
        return contextScreenY;
    }

    /**
     * Gibt das Kontext-Menü der Dokumente UI zurück
     * @return Das Kontext-Menü
     */
    public ContextMenu getDocContextMenu() {
        return docContextMenu;
    }
}
