package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import network.tcp.Client;
import network.tcp.events.PersonCreateRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Diese Klasse dient als Schnittstelle für die Interaktion um einen neuen Benutzer anzulegen
 *
 * */
public class CreateAccountController {
    @FXML    private TextField username;
    @FXML    private TextField realname;
    @FXML    private CheckBox adminCheckBox;

    /**
     * Schnittstelle mit Interaktion um einen neuen Benutzer anzulegen
     * */
    @FXML
    public void createAccount(ActionEvent event) {
        if(filledTextField()){
            try {
                String un = URLEncoder.encode(username.getText().replaceAll(" ", "%20"), "UTF-8");
                String rn = URLEncoder.encode(realname.getText().replaceAll(" ", "%20"), "UTF-8");
                Client.INSTANCE.sendEvent(new PersonCreateRequest(null, un, rn, adminCheckBox.isSelected()));
                Stage parent = (Stage) ((Button) event.getSource()).getScene().getWindow();
                parent.hide();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }else{
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initStyle(StageStyle.UTILITY);
            alert.setTitle("Benutzerdaten sind falsch");
            alert.setHeaderText("");
            alert.setContentText("Es wurden ungültige Zeichen für die Benutzerdaten verwendet");
            alert.showAndWait();
        }

    }
    /**
     * Check ob TextFields befüllt sind und ob im Benutzernamen kein Leerzeichen vorhanden ist.
     * @return true, wenn beide Felder einen Eintrag haben und username kein Leerzeichen hat.
     * */
    public boolean filledTextField() {
        return !username.getText().isEmpty() && !realname.getText().isEmpty() && !username.getText().contains(" ");
    }
}
