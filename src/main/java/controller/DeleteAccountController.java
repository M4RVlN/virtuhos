package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import network.tcp.Client;
import network.tcp.events.PersonCreateRequest;
import network.tcp.events.PersonDeleteRequest;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class DeleteAccountController {
    @FXML    private TextField username;

    @FXML
    public void deleteAccount(ActionEvent event) {
        if(filledTextField()){
            try {
                String un = URLEncoder.encode(username.getText().replaceAll(" ", "%20"), "UTF-8");
                Client.INSTANCE.sendEvent(new PersonDeleteRequest(null, un));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        Stage parent = (Stage) ((Button) event.getSource()).getScene().getWindow();
        parent.hide();
    }
    /**
     * Check ob TextFields befüllt sind
     * @return true, wenn alle drei Felder einen Eintrag haben.
     * */
    public boolean filledTextField() {
        return !username.getText().isEmpty();
    }
}
