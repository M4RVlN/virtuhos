package controller;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.UUID;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.StageStyle;
import javafx.util.Pair;
import model.Chair;
import model.Content;
import model.Hall;
import model.House;
import model.HouseElement;
import model.Office;
import model.OfficeType;
import model.Room;
import model.Table;
import util.GridRenderer;
import util.Vector2;
import util.xml.XMLUtil;
import window.SceneManager;
import window.Scenes;
/**
 * This class serves as controller to EditorView.
 * everything related to EditorView such as functions of buttons,
 * are described in this class
 * @Author Marvin Öhlerking, Alex Hahn, Philipp Brandes, Marc Herrmann
 */
public class EditorController extends BaseRenderController {
	private UUID item_id;

	private ContextMenu officeContextMenu;
	private ContextMenu hallContextMenu;

	private EditorState state = EditorState.FREE;

	private enum EditorState {
		FREE,
		TRANSLATE,
		RESIZE
	}

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		super.initialize(url, resourceBundle);

		initializeOfficeContextMenu();
		initializeHallContextMenu();
	}

	@Override
	public void setHouse(House house) {
		super.setHouse(house);
	}

	@Override
	public void renderHouse() {
		rendererCanvas.getChildren().add(getGridRenderer(house.getSize()));
		super.renderHouse();
	}

	private GridRenderer getGridRenderer(Vector2 size) {
		return new GridRenderer(size, BaseRenderController.GRID_SIZE, "lightgray", "#D3D3D333");
	}

	public void leavePressed(){
		SceneManager.loadScene(Scenes.MAIN_MENU);
	}

	protected boolean addRoom(Room room) {
		if(!room.isLegalTransform(getHouse()))
			return false;

		getHouse().addRoom(room);
		addRoomRenderer(room);
		return true;
	}

	protected boolean removeRoom(Room room) {
		boolean exists = getHouse().removeRoom(room);
		if(!exists)
			return false;

		removeRenderer(room);
		return true;
	}

	protected boolean addTable(Table table, Room room) {
		if(!table.isLegalTransform(getHouse(), room) || !getHouse().containsRoom(room))
			return false;

		getHouse().getRoom(room.getId()).getContent().add(table);
		addTableRenderer(table, room);
		return true;
	}

	protected boolean removeTable(Table table, Room room) {
		if(!getHouse().containsRoom(room))
			return false;

		ArrayList<Content> cList = getHouse().getRoom(room.getId()).getContent();
		if(!cList.contains(table))
			return false;

		cList.remove(table);
		removeRenderer(table);
		return true;
	}

	protected boolean addChair(Chair chair, Room room) {
		if(!chair.isLegalTransform(getHouse(), room) || !getHouse().containsRoom(room))
			return false;

		getHouse().getRoom(room.getId()).getContent().add(chair);
		addChairRenderer(chair, room);
		return true;
	}

	protected boolean removeChair(Chair chair, Room room) {
		if(!getHouse().containsRoom(room))
			return false;

		ArrayList<Content> cList = getHouse().getRoom(room.getId()).getContent();
		if(!cList.contains(chair))
			return false;

		cList.remove(chair);
		removeRenderer(chair);
		return true;
	}

	@Override
	protected Group addRoomRenderer(Room room) {
		Group group = super.addRoomRenderer(room);
		Node shape = group.getChildren().get(0);
		Node corner = group.getChildren().get(group.getChildren().size() - 1);

		//Events
		shape.setOnMousePressed(mouseEvent -> OnMouseDown(mouseEvent, room, EditorState.TRANSLATE));
		shape.setOnMouseDragged(mouseEvent -> OnMouseDragged_Translate(mouseEvent, room));
		shape.setOnMouseReleased(mouseEvent -> OnMouseUp(mouseEvent, room));

		if(room instanceof Office)
			shape.setOnContextMenuRequested(contextMenuRequest -> OnOfficeContextMenuRequested(contextMenuRequest, shape, (Office) room));
		else if(room instanceof Hall)
			shape.setOnContextMenuRequested(contextMenuRequest -> OnHallContextMenuRequested(contextMenuRequest, shape, (Hall) room));

		corner.setOnMousePressed(mouseEvent -> OnMouseDown(mouseEvent, room, EditorState.RESIZE));
		corner.setOnMouseDragged(mouseEvent -> OnMouseDragged_Resize(mouseEvent, room));
		corner.setOnMouseReleased(mouseEvent -> OnMouseUp(mouseEvent, room));
		return group;
	}

	@Override
	protected Group addTableRenderer(Table table, Room room) {
		Group group = super.addTableRenderer(table, room);
		Node shape = group.getChildren().get(0);
		Node corner = group.getChildren().get(group.getChildren().size() - 1);

		//Events
		shape.setOnMousePressed(mouseEvent -> OnMouseDown(mouseEvent, table, EditorState.TRANSLATE));
		shape.setOnMouseDragged(mouseEvent -> OnMouseDragged_Translate(mouseEvent, table));
		shape.setOnMouseReleased(mouseEvent -> OnMouseUp(mouseEvent, table));

		//shape.setOnContextMenuRequested(contextMenuRequest -> OnContextMenuRequested(contextMenuRequest, roomRenderGroup, room));

		corner.setOnMousePressed(mouseEvent -> OnMouseDown(mouseEvent, table, EditorState.RESIZE));
		corner.setOnMouseDragged(mouseEvent -> OnMouseDragged_Resize(mouseEvent, table));
		corner.setOnMouseReleased(mouseEvent -> OnMouseUp(mouseEvent, table));
		return group;
	}

	@Override
	protected Group addChairRenderer(Chair chair, Room room) {
		Group group = super.addChairRenderer(chair, room);
		Node shape = group.getChildren().get(0);

		//Events
		shape.setOnMousePressed(mouseEvent -> OnMouseDown(mouseEvent, chair, EditorState.TRANSLATE));
		shape.setOnMouseDragged(mouseEvent -> OnMouseDragged_Translate(mouseEvent, chair));
		shape.setOnMouseReleased(mouseEvent -> OnMouseUp(mouseEvent, chair));

		//shape.setOnContextMenuRequested(contextMenuRequest -> OnContextMenuRequested(contextMenuRequest, roomRenderGroup, room));
		return group;
	}

	@Override
	public boolean isEditor() {
		return true;
	}

//======================================================================================================================
//                                                  GUI Events
//======================================================================================================================


	public void addOfficeAction(ActionEvent event) {
		Office office = new Office(new Vector2(0, 0), new Vector2(BaseRenderController.GRID_SIZE * 2, BaseRenderController.GRID_SIZE * 2), OfficeType.DEFAULT);
		boolean result = addRoom(office);
		//Error message
		if(!result) {
			Alert errormessage = new Alert(AlertType.ERROR);
			errormessage.initStyle(StageStyle.UTILITY);
			errormessage.setHeaderText("Fehler beim hinzuf\u00fcgen eines B\u00fcros");
			errormessage.setContentText("Dem Geb\u00e4ude konnte kein weitereres B\u00fcro hinzugef\u00fcgt werden.");
			errormessage.showAndWait();
		}
		event.consume();
	}

	public void addConferenceAction(ActionEvent event) {
		Office office = new Office(new Vector2(0, 0), new Vector2(BaseRenderController.GRID_SIZE * 2, BaseRenderController.GRID_SIZE * 2), OfficeType.CONFERENCE);
		boolean result = addRoom(office);
		//Error message
		if(!result) {
			Alert errormessage = new Alert(AlertType.ERROR);
			errormessage.initStyle(StageStyle.UTILITY);
			errormessage.setHeaderText("Fehler beim hinzuf\u00fcgen eines Konferenzraums");
			errormessage.setContentText("Es konnte kein weitererer Konferenzraum hinzugef\u00fcgt werden.");
			errormessage.showAndWait();
		}
		event.consume();
	}

	/**
	 * this method is used to create a table in already existing room,
	 * if this room is a Halle,then table won't be created
	 * @param event of ActionEvent type
	 * @Author Alex Hahn, Marvin Öhlerking, Marc Herrmann
	 * @Version 1.3
	 */
	public void addTableAction(ActionEvent event) {
		try {
			Room room = getHouse().getRoom(item_id);
			if(room instanceof Hall)
				return;

			Table table = new Table(room.getPosition(), new Vector2(BaseRenderController.GRID_SIZE, BaseRenderController.GRID_SIZE));
			boolean result = addTable(table, room);
			//Error message
			if(!result) {
				Alert errormessage = new Alert(AlertType.ERROR);
				errormessage.initStyle(StageStyle.UTILITY);
				errormessage.setHeaderText("Fehler beim hinzuf\u00fcgen eines Tisches");
				errormessage.setContentText("Diesem Raum konnte kein weiterer Tisch hinzugef\u00fcgt werden.");
				errormessage.showAndWait();
			}
			event.consume();
		} catch (Exception ignore) { }
	}
	/**
	 * this method is used to create a Chair in already existing room,
	 * if this room is a Halle, then chair won't be created
	 * @param event of ActionEvent type
	 * @Author Alex Hahn, Marvin Öhlerking, Marc Herrmann
	 * @Version 1.3
	 */
	public void addChairAction(ActionEvent event) {
		try {
			Room room = getHouse().getRoom(item_id);
			if (room instanceof Hall)
				return;

			Chair chair = new Chair(room.getPosition());
			boolean result = addChair(chair, room);
			//Error message
			if(!result) {
				Alert errormessage = new Alert(AlertType.ERROR);
				errormessage.initStyle(StageStyle.UTILITY);
				errormessage.setHeaderText("Fehler beim hinzuf\u00fcgen eines Stuhls");
				errormessage.setContentText("Diesem Raum konnte kein weiterer Stuhl hinzugef\u00fcgt werden.");
				errormessage.showAndWait();
			}
			event.consume();
		} catch (Exception ignore) { }
	}
	/**
	 * this method is used to add a Halle in already existing house
	 * @param event of ActionEvent type
	 * @Author Alex Hahn, Marvin Öhlerking, Marc Herrmann
	 * @Version 1.3
	 */
	public void addHallAction(ActionEvent event) {
		Hall halle = new Hall(new Vector2(0, 0), new Vector2(BaseRenderController.GRID_SIZE * 2, BaseRenderController.GRID_SIZE * 2));
		boolean result = addRoom(halle);
		//Error message
		if(!result) {
			Alert errormessage = new Alert(AlertType.ERROR);
			errormessage.initStyle(StageStyle.UTILITY);
			errormessage.setHeaderText("Fehler beim hinzuf\u00fcgen einer Halle");
			errormessage.setContentText("Es konnte keine weiterere Halle hinzugef\u00fcgt werden.");
			errormessage.showAndWait();
		}
		event.consume();
	}
	/**
	 * this method is used to delete Items, created in Editor, such as chair,tish and rooms
	 * if there is only one hall,then it cannot be deleted
	 * @param event of ActionEvent type
	 * @Author Alex Hahn, Marvin Öhlerking, Marc Herrmann
	 * @Version 1.3
	 */
	public void deleteAction(ActionEvent event) {
		Room room = getHouse().getRoom(item_id);
		if(room != null) {
			//If there is only one hall, don't delete it
			if(room instanceof Hall) {
				if(getHouse().getHallCount() <= 1) {
					//Error message
					Alert errormessage = new Alert(AlertType.WARNING);
					errormessage.initStyle(StageStyle.UTILITY);
					errormessage.setHeaderText("Warnung");
					errormessage.setContentText("Es muss mindestens eine Halle existieren.");
					errormessage.showAndWait();
					event.consume();
					return;
				}
			}

			removeRoom(room);
			removeRenderer(room);
		} else {
			for(Room r : getHouse().getRoomList()) {
				Content c = r.getContent(item_id);

				if(c == null)
					continue;

				if(c instanceof Chair){
					removeChair((Chair)c, r);
					removeRenderer(c);
					break;
				} else if(c instanceof Table){
					removeTable((Table)c, r);
					removeRenderer(c);
					break;
				}
			}
		}
		event.consume();
	}

	public void saveAction(ActionEvent event) {
		try {
			XMLUtil.houseToXML(getHouse());
		} catch (Exception ignore) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.initStyle(StageStyle.UTILITY);
			alert.setHeaderText("Das Haus konnte nicht gespeichert werden");
			alert.setContentText("Bitte \u00fcberpr\u00fcfen Sie, ob Sie Zugriff auf den Speicherort besitzen");
			alert.showAndWait();
		}
		event.consume();
	}

	public void loadAction(ActionEvent event) {
		try {
			House house = XMLUtil.houseFromXML();
			if(house == null) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initStyle(StageStyle.UTILITY);
				alert.setHeaderText("Das Haus konnte nicht geladen werden");
				alert.setContentText("Bitte \u00fcberpr\u00fcfen Sie, ob die Datei gültig ist oder Sie Zugriff darauf besitzen");
				alert.showAndWait();
				return;
			}
			loadHouse(house);
		} catch (Exception ignore) { }
		event.consume();
	}

	public void scaleAction(ActionEvent event) {
		Dialog<Pair<Integer, Integer>> dialog = new Dialog<>();
		dialog.initStyle(StageStyle.UTILITY);
		dialog.setTitle("");
		dialog.setHeaderText(null);
		dialog.setGraphic(null);
		dialog.setResizable(false);

		Label header = new Label("Gebäudegr\u00f6ße \u00e4ndern");
		header.setFont(Font.font ("Arial Bold", 16));
		header.setTextFill(Color.web("#0080F0"));
		dialog.getDialogPane().setContent(header);

		//Add Buttons
		dialog.getDialogPane().getButtonTypes().addAll(ButtonType.APPLY, ButtonType.CANCEL);
		Button applyButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.APPLY);
		Button cancelButton = (Button) dialog.getDialogPane().lookupButton(ButtonType.CANCEL);

		applyButton.setFont(Font.font ("Arial Bold", 12));
		cancelButton.setFont(Font.font ("Arial Bold", 12));
		applyButton.setStyle("-fx-background-color: #0080F0; -fx-color: #000000;");
		cancelButton.setStyle("-fx-background-color: #0080F0; -fx-color: #000000;");
		applyButton.setDefaultButton(true);

		//Add TextFields
		GridPane grid = new GridPane();
		grid.setHgap(5);
		grid.setVgap(5);
		grid.setPadding(new Insets(10, 10, 10, 10));

		Vector2 houseSize = getHouse().getSize();
		TextField width = new TextField(String.format("%.0f", houseSize.x / BaseRenderController.GRID_SIZE));
		width.setPrefWidth(130);
		TextField height = new TextField(String.format("%.0f", houseSize.y / BaseRenderController.GRID_SIZE));
		height.setPrefWidth(130);

		//Filter for numeric values
		width.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				width.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});

		height.textProperty().addListener((observable, oldValue, newValue) -> {
			if (!newValue.matches("\\d*")) {
				height.setText(newValue.replaceAll("[^\\d]", ""));
			}
		});

		//Add Labels
		Label width_label = new Label("Breite:");
		Label height_label = new Label("H\u00f6he:");

		width_label.setFont(Font.font ("Arial Bold", 12));
		height_label.setFont(Font.font ("Arial Bold", 12));
		width_label.setTextFill(Color.web("#919191"));
		height_label.setTextFill(Color.web("#919191"));

		grid.add(header, 1,0);
		grid.add(width_label, 0, 1);
		grid.add(width, 1, 1);
		grid.add(height_label, 0, 2);
		grid.add(height, 1, 2);

		dialog.getDialogPane().setContent(grid);

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == ButtonType.APPLY) {
				try {
					return new Pair<>(Integer.parseInt(width.getText()) * BaseRenderController.GRID_SIZE, Integer.parseInt(height.getText()) * BaseRenderController.GRID_SIZE);
				} catch (NumberFormatException ignore) {
					return null;
				}
			}
			return null;
		});

		Optional<Pair<Integer, Integer>> result = dialog.showAndWait();

		result.ifPresent(data -> {
			boolean valid = getHouse().setSize(new Vector2(data.getKey(), data.getValue()));

			//Error if size too small
			if(!valid) {
				Alert alert = new Alert(AlertType.ERROR);
				alert.initStyle(StageStyle.UTILITY);
				alert.setHeaderText("Ung\u00fcltige Geb\u00e4udegr\u00f6ße");
				alert.setContentText("Die gewählte Gr\u00f6ße ist zu klein für ihren momentanen Inhalt");
				alert.showAndWait();
				return;
			}

			Vector2 size = getHouse().getSize();
			((GridRenderer)rendererCanvas.getChildren().get(0)).setSize(size);
			rendererCanvas.setPrefSize(size.x, size.y);
		});

		event.consume();
	}

//======================================================================================================================
//                                                  Editor Events
//======================================================================================================================

	private Vector2 dragEventStartPosition;
	private Vector2 dragEventStartSize;
	/**
	 * Handler des Startevents für das Bearbeiten eines Elements in der Renderansicht
	 * @param mouseEvent Parameter des Auslöserevents
	 * @param element Das Element das Verändert wird
	 */
	private void OnMouseDown(MouseEvent mouseEvent, HouseElement element, EditorState state) {
		//Verschieben/Vergrößern nur bei Linksklick
		if(mouseEvent.getButton() != MouseButton.PRIMARY)
			return;

		if(this.state != EditorState.FREE)
			return;
		this.state = state;

		//Mark selected
		if(getNodes().containsKey(item_id))
			getNodes().get(item_id).getChildren().get(0).getStyleClass().remove("selected");
		getNodes().get(element.getId()).getChildren().get(0).getStyleClass().add("selected");

		item_id = element.getId();

		dragEventStartPosition = element.getPosition();
		dragEventStartSize = element.getSize();
		mouseEvent.consume();
	}

	/**
	 * Handler des Events während des Verschiebens eines Elements in der Renderansicht
	 * @param mouseEvent Parameter des Auslöserevents
	 * @param element Die Daten des Elements
	 */
	private void OnMouseDragged_Translate(MouseEvent mouseEvent, HouseElement element) {
		//Verschieben/Vergrößern nur bei Linksklick
		if(mouseEvent.getButton() != MouseButton.PRIMARY)
			return;

		//Transform the relative screen position of the cursor to the canvas space and apply fake grid function
		Vector2 mousePos = toGridPosition(sceneToRenderSpace(new Vector2(mouseEvent.getSceneX(), mouseEvent.getSceneY())));

		//Move room content
		if(element instanceof Room) {
			for(Content c : ((Room)element).getContent()) {
				Vector2 diff = c.getPosition().sub(element.getPosition());
				c.setPosition(mousePos.add(diff));
			}
		}
		//Move room
		element.setPosition(mousePos);

		updateRenderer(element);
		mouseEvent.consume();
	}

	/**
	 * Handler des Events w?hrend des Skalierens eines Elements in der Renderansicht
	 * @param mouseEvent Parameter des Ausl?serevents
	 * @param element Die Daten des Elements
	 */
	private void OnMouseDragged_Resize(MouseEvent mouseEvent, HouseElement element) {
		//Verschieben/Vergrößern nur bei Linksklick
		if(mouseEvent.getButton() != MouseButton.PRIMARY)
			return;

		//Transform the relative screen position of the cursor to the canvas space and apply fake grid function
		Vector2 mousePos = toGridPosition(sceneToRenderSpace(new Vector2(mouseEvent.getSceneX(), mouseEvent.getSceneY())));
		double newWidth =  mousePos.x - element.getPosition().x;
		double newHeight = mousePos.y - element.getPosition().y;

		//Limit Width
		if(newWidth < GRID_SIZE)
			newWidth = GRID_SIZE;
		else if(newWidth > maxRoomWidth)
			newWidth = maxRoomWidth;

		//Limit Height
		if(newHeight < GRID_SIZE)
			newHeight = GRID_SIZE;
		else if(newHeight > maxRoomHeight)
			newHeight = maxRoomHeight;

		element.setSize(new Vector2(newWidth, newHeight));
		updateRenderer(element);
		mouseEvent.consume();
	}

	/**
	 * Handler des Schlussevents für das Bearbeiten eines Elements in der Renderansicht
	 * @param mouseEvent Parameter des Auslöserevents
	 * @param element Die Daten des Objekts
	 */
	private void OnMouseUp(MouseEvent mouseEvent, HouseElement element) {
		//Verschieben/Vergrößern nur bei Linksklick
		if(mouseEvent.getButton() != MouseButton.PRIMARY)
			return;

		this.state = EditorState.FREE;

		//Detect collision with other elements or out of bounds
		if(!element.isLegalTransform(getHouse())) {
			//Move room content back
			if(element instanceof Room) {
				for(Content c : ((Room)element).getContent()) {
					Vector2 diff = c.getPosition().sub(element.getPosition());
					c.setPosition(dragEventStartPosition.add(diff));
				}
			}

			//Move/scale room back
			element.setPosition(dragEventStartPosition);
			element.setSize(dragEventStartSize);
		}
		updateRenderer(element);
		mouseEvent.consume();
	}

//======================================================================================================================
//                                                  Context Menus
//======================================================================================================================

	private void initializeHallContextMenu() {
		hallContextMenu = new ContextMenu();
		MenuItem roomName = new MenuItem("Raumname \u00e4ndern");
		RadioMenuItem radioLobby = new RadioMenuItem("Zur Lobby machen");

		radioLobby.setOnAction(actionEvent -> {
			Hall hall = (Hall)hallContextMenu.getUserData();
			getHouse().setLobby(hall);
		});
		roomName.setOnAction(actionEvent -> roomNameContextMenu(hallContextMenu));

		hallContextMenu.getItems().addAll(roomName, new SeparatorMenuItem(), radioLobby);
		hallContextMenu.setOnShowing(windowEvent -> {
			boolean isSelected = ((Hall)hallContextMenu.getUserData()).getId().equals(getHouse().getLobby().getId());
			radioLobby.setSelected(isSelected);
		});
	}

	/**
	 * Initialisiert das roomContextMenu Object. Fügt die Auswahlmöglichkeiten "Tisch hinzufügen",
	 * "Stuhl hinzufügen", "Raumname ändern", "Raumart - Büro" und "Raumart - Hall" hinzu.
	 * Beinhaltet auch die Event Handler der Menu Items.
	 */
	private void initializeOfficeContextMenu() {
		officeContextMenu = new ContextMenu();
		MenuItem addTable = new MenuItem("Tisch hinzuf\u00fcgen");
		MenuItem addChair = new MenuItem("Stuhl hinzuf\u00fcgen");
		MenuItem roomName = new MenuItem("Raumname \u00e4ndern");
		RadioMenuItem radioOffice = new RadioMenuItem("Raumart - B\u00fcro");
		RadioMenuItem radioConference = new RadioMenuItem("Raumart - Konferenz");

		ToggleGroup toggleGroup = new ToggleGroup();
		radioOffice.setToggleGroup(toggleGroup);
		radioConference.setToggleGroup(toggleGroup);

		radioOffice.setOnAction(actionEvent -> {
			changeOfficeType((Office)officeContextMenu.getUserData(), OfficeType.DEFAULT);
		});
		radioConference.setOnAction(actionEvent -> {
			changeOfficeType((Office)officeContextMenu.getUserData(), OfficeType.CONFERENCE);
		});
		addTable.setOnAction(actionEvent -> {
			Room room = ((Room) officeContextMenu.getUserData());
			Vector2 pos = toGridPosition(sceneToRenderSpace(screenToSceneSpace(new Vector2(officeContextMenu.getX(), officeContextMenu.getY()))));
			boolean result = addTable(new Table(pos, new Vector2(BaseRenderController.GRID_SIZE, BaseRenderController.GRID_SIZE)), room);
			if(!result) { //Error message
				Alert errormessage = new Alert(AlertType.ERROR);
				errormessage.initStyle(StageStyle.UTILITY);
				errormessage.setHeaderText("Fehler beim hinzufügen eines Tisches");
				errormessage.setContentText("An dieser Stelle konnte kein Tisch hinzugefügt werden.");
				errormessage.showAndWait();
			}
		});
		addChair.setOnAction(actionEvent -> {
			Room room = ((Room) officeContextMenu.getUserData());
			Vector2 pos = toGridPosition(sceneToRenderSpace(screenToSceneSpace(new Vector2(officeContextMenu.getX(), officeContextMenu.getY()))));
			boolean result = addChair(new Chair(pos), room);
			if(!result) { //Error message
				Alert errormessage = new Alert(AlertType.ERROR);
				errormessage.initStyle(StageStyle.UTILITY);
				errormessage.setHeaderText("Fehler beim hinzufügen eines Stuhles");
				errormessage.setContentText("An dieser Stelle konnte kein Stuhl hinzugefügt werden.");
				errormessage.showAndWait();
			}
		});
		roomName.setOnAction(actionEvent -> roomNameContextMenu(officeContextMenu));

		officeContextMenu.getItems().addAll(addTable, addChair, roomName, new SeparatorMenuItem(), radioOffice, radioConference);
		officeContextMenu.setOnShowing(windowEvent -> {
			if (((Office)officeContextMenu.getUserData()).getType() == OfficeType.DEFAULT) {
				toggleGroup.selectToggle(toggleGroup.getToggles().get(0));
			} else if (((Office)officeContextMenu.getUserData()).getType() == OfficeType.CONFERENCE)  {
				toggleGroup.selectToggle(toggleGroup.getToggles().get(1));
			}
		});
	}

	/**
	 * Dialog um Raumnamen zu ändern
	 * @param menu Kontextmenü über das es aufgerufen wurde
	 */
	private void roomNameContextMenu(ContextMenu menu) {
		//Öffnet Dialog, um einen Namen einzugeben
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle("Raumname \u00e4ndern");
		dialog.setHeaderText(null);
		dialog.setGraphic(null);
		dialog.setContentText("Neuer Raumname:");
		Optional<String> result = dialog.showAndWait();
		result.ifPresent(action -> {
			if(!action.equals("")){
				Room room = (Room) menu.getUserData();
				room.setName(action);
				updateRenderer(room);
			}
		});
	}
	
	private void OnHallContextMenuRequested(ContextMenuEvent contextMenuEvent, Node shape, Hall room) {
		if(this.state != EditorState.FREE)
			return;

		hallContextMenu.setUserData(room);
		hallContextMenu.show(shape, contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY());
		contextMenuEvent.consume();
	}

	private void OnOfficeContextMenuRequested(ContextMenuEvent contextMenuEvent, Node shape, Office room) {
		if(this.state != EditorState.FREE)
			return;

		officeContextMenu.setUserData(room);
		officeContextMenu.show(shape, contextMenuEvent.getScreenX(), contextMenuEvent.getScreenY());
		contextMenuEvent.consume();
	}
}