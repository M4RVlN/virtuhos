package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.StageStyle;
import network.tcp.Client;
import util.HardwareCheck;
import window.SceneManager;
import window.Scenes;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Diese Klasse interagiert mit der WB-Interaktion-3 Gruppe um den Login zu verarbeiten.
 * Daraufhin wird die entsprechende View geöffnet.
 * */

public class LoginController extends Controller implements Initializable {
    @FXML
    private TextField name;

    @FXML
    private TextField url;

    @FXML
    private Button loginButton;

    @FXML
    private Button backButton;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        loginButton.setOnAction(e -> nextView());
        backButton.setOnAction(e -> SceneManager.loadScene(Scenes.MAIN_MENU));
    }

    /**
     * Schickt Request an Interaktion um die Logindaten zu vergleichen. Öffnet nach erfolgreicher Anmeldung die Ansicht.
     * */
    public void nextView(){
        if(filledTextField()){
            Pattern p = Pattern.compile("(.+)(?::([0-9]+$))");
            Matcher m = p.matcher(url.getText());
            if (m.find() && m.groupCount() != 2)
                return;

            boolean hasMic = HardwareCheck.checkMic();
            boolean hasAudio = HardwareCheck.checkAudio();
            boolean hasCam = HardwareCheck.checkCam();
            if (!hasMic){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Fehlende Hardware");
                alert.setHeaderText("");
                alert.setContentText("Ohne Mikrophon kann keine Verbindung hergestellt werden!");
                alert.showAndWait();
            }
            else if(!hasAudio){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Fehlende Hardware");
                alert.setHeaderText("");
                alert.setContentText("Ohne Audioausgabe kann keine Verbindung hergestellt werden!");
                alert.showAndWait();
            }
            else if(!hasCam){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initStyle(StageStyle.UTILITY);
                alert.setTitle("Fehlende Hardware");
                alert.setHeaderText("");
                alert.setContentText("Ohne Kamera kann keine Verbindung hergestellt werden!");
                alert.showAndWait();
            }
            else {
                boolean success = Client.INSTANCE.connect(m.group(1), Integer.parseInt(m.group(2)), name.getText(), "");

                if (!success) {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.initStyle(StageStyle.UTILITY);
                    alert.setTitle("Server nicht erreichbar");
                    alert.setHeaderText("");
                    alert.setContentText("Es konnte keine Verbindung zum Server hergestellt werden");
                    alert.showAndWait();
                }
            }
        }
    }

    /**
     * Check ob TextFields befüllt sind
     * @return true, wenn alle drei Felder einen Eintrag haben.
     * */
    public boolean filledTextField() {
        if(name.getText().length() == 0) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initStyle(StageStyle.UTILITY);
            alert.setHeaderText("");
            alert.setContentText("Bitte Benutzernamen angeben");
            alert.showAndWait();
        }
        return !name.getText().isEmpty() && !url.getText().isEmpty();
    }
}
