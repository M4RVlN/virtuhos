package controller;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import window.SceneManager;
import window.Scenes;

public class MainMenuController extends Controller implements Initializable {

    @FXML
    private Button serverBetretenButton;

    @FXML
    private Button serverHostenButton;
    @FXML private Button serverHosten;

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		serverHostenButton.setOnAction(event -> SceneManager.loadScene(Scenes.EDITOR));
		serverBetretenButton.setOnAction(event -> SceneManager.loadScene(Scenes.LOGIN));
		serverHosten.setOnAction(event -> SceneManager.loadScene(Scenes.SERVER));
	}
}
