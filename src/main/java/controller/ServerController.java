package controller;

import config.AppConfig;
import config.AppProperties;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import model.House;
import network.tcp.Server;
import util.xml.XMLUtil;
import window.SceneManager;
import window.Scenes;
/**
 * Diese Klasse dient dazu einen Server local auf den Rechner zu starten. Dabei kann ein Server erst gestartet werden,
 * wenn zu der Datenbank und der BBB API eine Verbindung steht. *
 * */
public class ServerController extends Controller implements Initializable {
    @FXML
    private Button startServer;

    @FXML
    private Button stopServer;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        startServer.setDisable(false);
        stopServer.setDisable(true);
    }
    /**
     * Funktion um den Server zu starten
     * */
    public void startServer() {
        try {
            House house = XMLUtil.houseFromXML();

            //No house selected
            if(house == null)
                return;

            //Invalid house file
            if(!house.isValid())
                throw new Exception("House not valid.");

            Server.INSTANCE.start(AppProperties.SERVER_PORT.getInt(), house);
        } catch (SQLException sqlException) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Datenbankfehler");
            alert.setHeaderText("Es konnte keine Verbindung zur Datenbank hergestellt werden.");
            alert.setContentText("Bitte \u00fcberpr\u00fcfen sie, ob die Datenbank erreichbar ist und die Daten in der " + AppConfig.fileName + " korrekt sind.");
            alert.showAndWait();
            return;
        } catch (IOException ioException) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Server Socket konnte nicht ge\u00f6ffnet werden");
            alert.setHeaderText(ioException.getLocalizedMessage());
            alert.setContentText("Bitte \u00fcberpr\u00fcfen sie, ob der angegebene Port frei und zul\u00e4ssig und die URL korrekt ist.");
            alert.showAndWait();
            return;
        } catch (Exception e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(e.getLocalizedMessage());
            alert.setHeaderText("");
            alert.setContentText("Es gab einen Fehler beim Starten des Servers");
            alert.showAndWait();
            return;
        }

        startServer.setDisable(true);
        stopServer.setDisable(false);
    }
    /**
     * Funktion um den Server zu stoppen
     */
    public void stopServer() {
        Server.INSTANCE.stop();

        startServer.setDisable(false);
        stopServer.setDisable(true);
    }
    /**
     * Scenenwechsel zum Main Menu
     * */
    public void back() {
        Server.INSTANCE.stop();
        SceneManager.loadScene(Scenes.MAIN_MENU);
    }
}
