package controller;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Diese Klasse soll dem normalen Nutzer widerspiegeln.
 * */
public class UserController extends ClientRenderController{
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        super.initialize(url, resourceBundle);
    }

    @Override
    public boolean isEditor() {
        return false;
    }


}
