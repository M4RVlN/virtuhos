package model;

import controller.BaseRenderController;
import org.simpleframework.xml.Element;
import util.Vector2;

import java.util.UUID;

/**
 * Diese Klasse implementiert ein Stühl als Inhalt eines Raumes.
 * Alle Parameter und zugehörige Funktionen eines Stuhls werden in dieser Klasse beschrieben.
 *
 * @version 12.01.2021
 *
 * @author Marvin Öhlerking, Marc Herrmann
 */

public class Chair extends Content {
	public static final Vector2 CHAIR_SIZE = new Vector2(BaseRenderController.GRID_SIZE, BaseRenderController.GRID_SIZE); //Chairs should have a fixed size

	/**
	 * Erstellt eine Stuhl Instanz mit der angegebenen Position als Parameter
	 *
	 * @param position Position des Stuhls
	 */
	public Chair(@Element(name = "position") Vector2 position) {
		super(position, CHAIR_SIZE);
	}

	/**
	 * Erstellt eine Stuhl Instanz mit der angegebenen UUID und Position als Parameter
	 *
	 * @param id UUID des Stuhls
	 * @param position Position des Stuhls
	 */
	public Chair(@Element(name = "id") UUID id,
				 @Element(name = "position") Vector2 position) {
		super(id, position, CHAIR_SIZE);
	}

	/**
	 * Gibt an ob eine Stuhl Instanz eine erlaubte transformation innerhalb des Gebäudes darstellt
	 *
	 * @param house das Gebäude indem sich der Stuhl befinden soll
	 * @return true, falls der Stuhl sich an einer erlaubten position befindet, ansonsten false
	 */
	@Override
	public boolean isLegalTransform(House house) {
		if (!house.isInBounds(this))
			return false;

		Room containedRoom = null;
		//Check if in a room
		for(Room r : house.getRoomList())
		{
			if(r.getContent().contains(this)) {
				containedRoom = r;
				break;
			}
		}

		if(containedRoom == null || !containedRoom.isInBounds(this))
			return false;

		for(Content c : containedRoom.getContent()) {
			if(c.getId().equals(id))
				continue;

			if(c.intersect(this))
				return false;
		}
		return true;
	}

	/**
	 * Gibt an ob eine Stuhl Instanz eine erlaubte transformation innerhalb des Gebäudes darstellt
	 *
	 * @param house das Gebäude indem sich der Stuhl befinden soll
	 * @return true, falls der Stuhl sich an einer erlaubten position befindet, ansonsten false
	 */
	public boolean isLegalTransform(House house, Room room) {
		if (!house.isInBounds(this) || !room.isInBounds(this))
			return false;

		for(Content c : room.getContent()) {
			if(c.getId().equals(id))
				continue;

			if(c.intersect(this))
				return false;
		}
		return true;
	}
}
