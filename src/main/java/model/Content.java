package model;

import org.simpleframework.xml.Element;
import util.Vector2;

import java.util.UUID;

/**
 * Elternklasse für alle Möbelstücke im Gebäude.
 * @author Marc Herrmann
 */
public abstract class Content extends HouseElement {

    /**
     * Konstruktor für die abstrakte Klasse Content.
     * @param position Die Position des Content im Gebäude
     * @param size Die Größe des Content im Gebäude
     */
    public Content(@Element(name = "position") Vector2 position,
                   @Element(name = "size") Vector2 size) {
        super(position, size);
    }
    /**
     * Konstruktor für die abstrakte Klasse Content.
     * @param id Die UUID des Content-Objekts
     * @param position Die Position des Content im Gebäude
     * @param size Die Größe des Content im Gebäude
     */
    public Content(@Element(name = "id") UUID id,
                   @Element(name = "position") Vector2 position,
                   @Element(name = "size") Vector2 size) {
        super(id, position, size);
    }
}
