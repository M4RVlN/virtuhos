package model;

import java.util.Arrays;

/**
 * Diese Klasse implementiert ein Dokument.
 * Alle Parameter und zugehörige Funktionen eines Dokumentes werden in dieser Klasse beschrieben.
 *
 * @version 19.01.2021
 *
 * @author Marc Herrmann
 */

public class Document {
    public byte[] data;
    public String name;
    public String uploader;
    public String meetingId;
    public int id = -1;

    /**
     * Konstruktor eines Dokument Objektes. Gibt eine Dokument Instanz mit den angegebenen Parametern zurück.
     *
     * @param data ByteArray des zu speichernden Dokumentes (erhältlich über Files.readAllBytes(Dateipfad))
     * @param name Name des Dokumentes inklusive Dateiendung
     * @param uploaderId UUID der Person die das Dokument hochlädt
     * @param meetingId UUID des Meetings in dem sich das Dokument befindet
     */
    public Document (byte[] data, String name, String uploaderId, String meetingId) {
        this.data = Arrays.copyOf(data, data.length);
        this.name = name;
        this.uploader = uploaderId;
        this.meetingId = meetingId;
    }
}
