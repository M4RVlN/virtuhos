package model;

import java.util.ArrayList;
import java.util.UUID;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import util.Vector2;

/**
 * Diese Klasse implementiert die Halle als Sonderfall eines Raumes ohne Möbel.
 * Alle Parameter und zugehörige Funktionen einer Halle werden in dieser Klasse beschrieben.
 *
 * @version 19.01.2021
 *
 * @author Marvin Öhlerking
 */

public class Hall extends Room {
	@Element(name = "mode")
	private HallSortingMode hallMode = HallSortingMode.RANDOM;

	/**
	 * Konstruktor einer Halle der einen Raum übergeben bekommt und eine Halle Instanz mit den selben Parametern zurückgibt.
	 *
	 * @param room Raum der in einer Halle konvertiert werden soll.
	 */
	public Hall(Room room) {
		super(room.position, room.size, room.name, room.content, room.usersInRoom);
	}

	/**
	 * Konstruktor für eine Halle. Gibt eine Halle Instanz mit den angegebenen Parametern zurück
	 *
	 * @param id UUID der zu erstellenden Halle
	 * @param position Position der Halle als Vector2
	 * @param size Größe der Halle als Vector2
	 * @param name Name der zu erstellenden Halle
	 * @param usersInRoom Array der Nutzer die sich in der Halle befinden.
	 */
	public Hall(@Element(name = "id") UUID id,
				@Element(name = "position") Vector2 position,
				@Element(name = "size") Vector2 size,
				@Element(name = "name") String name,
				@ElementList(name = "users") ArrayList<Person> usersInRoom) {
		super(id, position, size, name, null, usersInRoom);
	}

	/**
	 * Konstruktor für eine Halle. Gibt eine Halle Instanz mit den angegebenen Parametern zurück
	 *
	 * @param position Position der Halle als Vector2
	 * @param size Größe der Halle als Vector2
	 * @param name Name der zu erstellenden Halle
	 * @param usersInRoom Array der Nutzer die sich in der Halle befinden.
	 */
	public Hall(@Element(name = "position") Vector2 position,
				@Element(name = "size") Vector2 size,
				@Element(name = "name") String name,
				@ElementList(name = "users") ArrayList<Person> usersInRoom) {
		super(position, size, name, null, usersInRoom);
	}

	/**
	 * Konstruktor für eine Halle. Gibt eine Halle Instanz mit den angegebenen Parametern zurück
	 *
	 * @param position Position der Halle als Vector2
	 * @param size Größe der Halle als Vector2
	 * @param name Name der zu erstellenden Halle
	 */
	public Hall(@Element(name = "position") Vector2 position,
				@Element(name = "size") Vector2 size,
				@Element(name = "name") String name) {
		super(position, size, name);
	}

	/**
	 * Konstruktor für eine Halle. Gibt eine Halle Instanz mit den angegebenen Parametern zurück
	 *
	 * @param position Position der Halle als Vector2
	 * @param size Größe der Halle als Vector2
	 */
	public Hall(Vector2 position, Vector2 size) {
		super(position, size, "Halle");
	}

	/**
	 * Gibt den Inhalt einer Halle an der angegebenen Position zurück. Überschrieben da die Halle immer leer ist.
	 *
	 * @param  Pos Postition des gesuchten Contents
	 *
	 * @return null da Halle keine Möbel haben darf
	 */
    @Override
    public Content getContent(Vector2 Pos) {
        return null;
    }

	/**
	 * Gibt den aktuellen Sortiermodus der Halle zurück.
	 *
	 * @return hallMode Sortiermodus der Halle
	 */
	public HallSortingMode getHallMode(){
		return this.hallMode;
	}

	/**
	 * Setzt den aktuellen Sortiermodus der Halle fest.
	 *
	 * @param mode festgelegter Sortiermodus
	 */
	public void setHallMode(HallSortingMode mode){
		this.hallMode = mode;
	}
}