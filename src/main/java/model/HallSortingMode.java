package model;

/**
 * Dieses Enum implementiert die unterschiedlichen Sortiermodi der Halle.
 * Zur Auswahl stehen Random, Usual und Inverse.
 *
 * @version 19.01.2021
 *
 * @author Marvin Öhlerking
 */
public enum HallSortingMode {
    RANDOM,
    USUAL,
    INVERSE;
}
