package model;

import java.util.ArrayList;
import java.util.UUID;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;
import util.Bounds;
import util.Vector2;

/**
 * Die Haus Klasse für das Model. Sie beinhaltet alle Räume und Personen, die im Haus modelliert werden sollen.
 * @author Nicolas Fröhlich
 */
@Root(name = "house")
public class House {
    @Element(name = "size")
    protected Vector2 size;
    @Element(name = "lobby")
    protected UUID lobby;
    @ElementList(name = "rooms")
    protected ArrayList<Room> rooms = new ArrayList<>();

    /**
     * Der Konstruktor der Klasse House. Erzeugt eine Instanze mit Größe size.
     *
     * @param size Die Größe des Gebäudes
     */
    public House(@Element(name = "size") Vector2 size) {
        this.size = size;
    }

    /**
     * Änder die Größe des Gebäudes
     *
     * @param size Die neue Gebäudegröße
     * @return Gibt zurück, ob das ändern der Gebäudegröße erfolgreich war
     */
    public boolean setSize(Vector2 size) {
        Vector2 bounds = new Vector2(0, 0);
        for (Room room : rooms) {
            Vector2 roomBounds = room.getPosition().add(room.getSize());
            if(roomBounds.x > bounds.x)
                bounds.x = roomBounds.x;
            if(roomBounds.y > bounds.y)
                bounds.y = roomBounds.y;
        }

        if(bounds.x <= size.x && bounds.y <= size.y) {
            this.size = size;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Gibt die Größe des Gebäudes zurück
     *
     * @return Die Größe des Raums
     */
    public Vector2 getSize() {
        return size;
    }

    /**
     * Findet den Raum mit der UUID id
     *
     * @param id Die UUID des gesucht Raums
     * @return Der Raum mit der UUID id. null, falls kein Raum gefunden wurde
     */
    public Room getRoom(UUID id) {
        for (Room r : rooms) {
            if (r.id.equals(id))
                return r;
        }
        return null;
    }

    /**
     * Findet den Raum, an der Position pos
     *
     * @param pos Die Position, an welche ein Raum gesucht wird
     * @return Der Raum an der Position pos. null, falls kein Raum gefunden wurde
     */
    public Room getRoom(Vector2 pos) {
        for (Room r : rooms) {
            if (r.isInBounds(pos))
                return r;
        }
        return null;
    }

    /**
     * Findet den Raum, in welchem sich die Person user befindet
     *
     * @param user Die Person, deren Raum gesucht wird
     * @return Der Raum in dem sich user befindet. null, falls kein Raum gefunden wurde
     */
    public Room getRoom(Person user) {
        for (Room r : rooms) {
            if (r.getUsers().contains(user))
                return r;
        }
        return null;
    }

    /**
     * Fügt dem Gebäude einen neuen Raum hinzu
     *
     * @param room Der Raum der hinzugefügt werden soll
     */
    public void addRoom(Room room) {
        rooms.add(room);
    }

    /**
     * Entfernt den Raum aus dem Gebäude
     *
     * @param room Der Raum, welcher entfernt werden soll.
     * @return true, falls der Raum erfolgreich entfernt wurde. Sonst false
     */
    public boolean removeRoom(Room room) {
        if (rooms.contains(room)) {
            rooms.remove(room);
            return true;
        }
        return false;
    }

    /**
     * Überprüft, ob der Übergebene Raum Teil des Gebäudes ist.
     *
     * @param room Der Raum, welcher überprüft werden soll
     * @return true, falls es room im Gebäude gibt. Sonst false
     */
    public boolean containsRoom(Room room) {
        return rooms.contains(room);
    }

    /**
     * Gibt alle Räume im Gebäude als Liste zurück.
     *
     * @return Die Liste aller Räume, die es im Gebäude gibt
     */
    public ArrayList<Room> getRoomList() {
        return rooms;
    }

    /**
     * Überpüft, ob die Position pos innerhalb des Gebäudes liegt.
     *
     * @param pos Die zu überprüfende Position
     * @return true, falls pos des Gebäudes liegt. Sonst false"
     */
    public boolean isInBounds(Vector2 pos) {
        boolean inLowerBound = pos.x >= 0 && pos.y >= 0;
        boolean inUpperBound = pos.x <= size.x && pos.y <= size.y;
        return inLowerBound && inUpperBound;
    }

    /**
     * Überpüft, ob ein Objekt an der Position pos mit der Größe objectSize innerhalb des Gebäudes liegt.
     *
     * @param pos Die Position des Objekts, welches überprüft wird
     * @param objectSize Die Größe des Objekts, welches überprüft wird
     * @return true, falls das Objekt innerhalb des Gebäudes liegt. Sonst false
     */
    public boolean isInBounds(Vector2 pos, Vector2 objectSize) {
        return isInBounds(pos) && isInBounds(pos.add(objectSize));
    }

    /**
     * Überpüft, ob das HouseElement innerhalb des Gebäudes liegt.
     *
     * @param element Das HouseElement, welches überprüft wird
     * @return true, falls element innerhalb des Gebäudes liegt. Sonst false
     */
    public boolean isInBounds(HouseElement element) {
        return isInBounds(element.getPosition(), element.getSize());
    }

    /**
     * Fügt dem Model eine neue Person hinzu.
     * Falls sich die Person an keiner passenden Position befindet, wird sie der Halle hinzugefügt.
     *
     * @param person Die Person, die hinzugefügt werden soll
     * @return Gibt zurück, ob das Hinzufügen einer Person erfolgreich war
     */
    public boolean addPerson(Person person) {
        if(person == null)
            return false;

        Room room = getRoom(person.getPosition());
        return room == null ? addPerson(person, getLobby()) : room.addUser(person);
    }

    /**
     * Fügt dem Model eine neue Person hinzu
     *
     * @param person Die Person, die hinzugefügt werden soll
     * @param room Der Raum, in dem sich die Person befinden soll
     * @return Gibt zurück, ob das Hinzufügen einer Person erfolgreich war
     */
    public boolean addPerson(Person person, Room room) {
        if(person == null || room == null)
            return false;

        if(!room.isInBounds(person)) {
            person.setPosition(room.position.add(person.size.add(new Vector2(10, 10))));
        }
        return room.addUser(person);
    }

    /**
     * Entfernt eine Person aus dem Model
     *
     * @param person Die zu entfernende Person
     * @return Gibt zurück, ob das entfernen der Person erfolgreich war
     */
    public boolean removePerson(Person person) {
        Room room = getRoom(person);

        if(room == null)
            return false;

        return room.removeUser(person);
    }

    /**
     * Findet eine Person im Model anhand ihrer UUID. null, falls keine Person zu der ID gefunden wurde
     *
     * @param id Die UUID der Person
     * @return Die dazu gehörende Person oder null, falls keine Person gefunden
     */
    public Person getPerson(UUID id) {
        for (Room r : rooms) {
            Person p = r.getUser(id);
            if(p != null)
                return p;
        }
        return null;
    }


    /**
     * Bewegt eine Person im Model. Dabei wird sowohl ihre Position, als der Raum in der sie sich befindet angepasst.
     *
     * @param user Die Person, die bewegt wird
     * @param pos Die Position, an welche die Person bewegt wird
     * @return true, falls die Verschiebung möglich war. Sonst false
     */
    public boolean movePerson(Person user, Vector2 pos) {
        Room oldRoom = getRoom(user);
        Room newRoom = getRoom(pos);

        if(newRoom == null)
            return false;

        if(newRoom.equals(oldRoom)) {
            user.setPosition(pos);
            return true;
        } else if (oldRoom != null) {
            if(!newRoom.addUser(user))
                return false;

            oldRoom.removeUser(user);
            user.setPosition(pos);
            return true;
        }
        return false;
    }

    /**
     * Bewegt eine Person im Model. Dabei wird sowohl ihre Position, als der Raum in der sie sich befindet angepasst.
     *
     * @param user Die Person, die bewegt wird
     * @param pos Die Position, an welche die Person bewegt wird
     * @return true, falls die Verschiebung möglich war. Sonst false.
     */
    public boolean movePersonChecked(Person user, Vector2 pos) {
        Room oldRoom = getRoom(user);
        Room newRoom = getRoom(pos);

        if(newRoom == null)
            return false;

        if(newRoom.equals(oldRoom)) {
            Vector2 oldPos = user.getPosition();
            user.setPosition(pos);

            //Check if position is valid
            if(!user.isLegalTransform(this)) {
                user.setPosition(oldPos);
                return false;
            }

            return true;
        } else if (oldRoom != null) {
            Vector2 oldPos = user.getPosition();
            user.setPosition(pos);

            //Check if position is valid / user can be added
            if(!user.isLegalTransform(this) || !newRoom.addUser(user)) {
                user.setPosition(oldPos);
                return false;
            }
            oldRoom.removeUser(user);
            return true;
        }
        return false;
    }

    /**
     * Überprüft of das Gebäude eine Halle besitzt
     * @return true falls vorhanden, false sonst
     */
    public boolean hasHall() {
        for(Room r : rooms) {
            if(r instanceof Hall)
                return true;
        }
        return false;
    }

    /**
     * Überprüft of das Gebäude eine Halle besitzt
     * @return true falls vorhanden, false sonst
     */
    public int getHallCount() {
        int c = 0;
        for(Room r : rooms) {
            if(r instanceof Hall)
                c++;
        }
        return c;
    }

    /**
     * Gibt die Halle zurück. Falls die UUID nicht einer Halle gehört, wird null zurückgegeben
     * @param id Die UUID der Halle
     * @return Die Halle des Hauses, null falls keine vorhanden
     */
    public Hall getHall(UUID id) {
        Room room = getRoom(id);
        return room instanceof Hall ? (Hall)room : null;
    }

    /**
     * Gibt ArrayList mit allen Hallen zurück
     * @return ArrayList mit allen Hallen im Gebäude
     */
    public ArrayList<Room> getHallList(){
        ArrayList<Room> hallList = new ArrayList<>();
        for(Room room : getRoomList()){
            if(room instanceof Hall){
                hallList.add(room);
            }
        }
        return hallList;
    }

    /**
     * Gibt die Lobby zurück
     * @return Die Halle die als Lobby des Hauses gilt, null falls keine vorhanden
     */
    public Hall getLobby() {
        Hall hall = getHall(lobby);
        if(hall != null) {
            return hall;
        } else {
            for (Room r : rooms) {
                if (r instanceof Hall) {
                    return (Hall) r;
                }
            }
            return null;
        }
    }

    /**
     * Setzt die Lobby für das Haus
     * @param lobby Die Halle welche als Lobby dienen soll
     */
    public void setLobby(Hall lobby) {
        this.lobby = lobby.getId();
    }

    /**
     * Gibt die Grenzen des Hausinhalts zurück
     * @return Die Grenzen des Hausinhalts
     */
    public Bounds getContentBounds() {
        Vector2 min = new Vector2();
        Vector2 max = new Vector2();

        for(Room room : rooms) {
            Vector2 rMin = room.getPosition();
            Vector2 rMax = room.getPosition().add(room.getSize());

            if(rMin.x < min.x)
                min.x = rMin.x;
            if(rMin.y < min.y)
                min.y = rMin.y;
            if(rMax.x > max.x)
                max.x = rMax.x;
            if(rMax.y > max.y)
                max.y = rMax.y;
        }

        return new Bounds(min, max);
    }

    /**
     * Überprüft ob es genau eine Halle gibt und der Inhalt des Hauses in der Größe ist
     * @return true falls zulässig, sonst false
     */
    public boolean isValid() {
        Bounds bounds = getContentBounds();

        return getLobby() != null && bounds.max.x <= size.x && bounds.max.y <= size.y && bounds.min.x >= 0 && bounds.min.y >= 0;
    }
}
