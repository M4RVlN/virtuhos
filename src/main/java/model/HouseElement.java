package model;

import java.util.UUID;
import org.simpleframework.xml.Element;
import util.Vector2;

/**
 * Diese Klasse implementiert ein abstraktes HouseElement. (Inhalt/Möbel des Raumes)
 * Alle Parameter und zugehörige Funktionen eines HouseElement werden in dieser Klasse beschrieben.
 *
 * @version 19.01.2021
 *
 * @author Marvin Öhlerking
 */
public abstract class HouseElement {
    @Element(name="id")
    protected UUID id;
    @Element(name="position")
    protected Vector2 position;
    @Element(name="size")
    protected Vector2 size;

    /**
     * Konstruktor für ein HouseElement. Gibt eine HouseElement Instanz mit den angegebenen Parametern zurück
     *
     * @param id UUID des HouseElement
     * @param position Position des HouseElement als Vector2
     * @param size Größe des HouseElement als Vector2
     */
    public HouseElement(@Element(name="id") UUID id,
                        @Element(name = "position") Vector2 position,
                        @Element(name = "size") Vector2 size) {
        this.id = id;
        this.position = position;
        this.size = size;
    }

    /**
     * Konstruktor für ein HouseElement. Gibt eine HouseElement Instanz mit den angegebenen Parametern zurück
     *
     * @param position Position des HouseElement als Vector2
     * @param size Größe des HouseElement als Vector2
     */
    public HouseElement(@Element(name = "position") Vector2 position,
                        @Element(name = "size") Vector2 size) {
        this(UUID.randomUUID(), position, size);
    }

    /**
     * Gibt an ob sich eine Position innerhalb eines HouseElement befindet
     *
     * @param pos zu überprüfende Position
     * @return true, falls angegebene Koordinaten innerhalb HouseElement, ansonsten false
     */
    public boolean isInBounds(Vector2 pos) {
        boolean inLowerBound = pos.x >= position.x && pos.y >= position.y;
        boolean inUpperBound = pos.x <= size.x + position.x && pos.y <= size.y + position.y;
        return inLowerBound && inUpperBound;
    }

    /**
     * Gibt an ob sich ein Objekt unter angabe von Position und Größe innerhalb eines HouseElement befindet
     *
     * @param pos Position des Objektes
     * @param objectSize Größe des Objektes
     * @return true falls die angegebene Position und Größe vollständig innerhalb des HouseElements, ansonsten false
     */
    public boolean isInBounds(Vector2 pos, Vector2 objectSize) {
        return isInBounds(pos) && isInBounds(pos.add(objectSize));
    }

    /**
     * Gibt an on sich ein anderes HouseElement innerhalb eines HouseElement befindet.
     *
     * @param element anderes HouseElement
     * @return true falls anderes HouseElement in HouseElement, ansonsten false
     */
    public boolean isInBounds(HouseElement element) {
        return isInBounds(element.getPosition(), element.getSize());
    }

    /**
     * Gibt an on sich eine Person innerhalb eines HouseElement befindet.
     *
     * @param person zu überprüfende Person
     * @return true falls Person innerhalb HouseElement, ansonsten false
     */
    public boolean isInBounds(Person person){
        Vector2 halfSize = new Vector2(person.getSize().x / 2, person.getSize().y / 2);
        return isInBounds(person.getPosition().sub(halfSize), person.getSize());
    }

    /**
     * Gibt an ob sich ein Objekt unter Angabe von Position und Größe mit einem HouseElement überschneidet
     *
     * @param pos Position des Objekt
     * @param objectSize Größe des Objekt
     * @return true falls es zu einer Überschneidung kommt, ansonsten false
     */
    public boolean intersect(Vector2 pos, Vector2 objectSize) {
        return !(position.x + size.x <= pos.x || pos.x + objectSize.x <= position.x || position.y + size.y <= pos.y || pos.y + objectSize.y <= position.y);
    }

    /**
     * ibt an ob sich ein anderes HouseElement mit einem HouseElement überschneidet
     *
     * @param element anderes HouseElement
     * @return true falls es zu einer Überschneidung mit dem anderen HouseElement kommt, ansonsten false
     */
    public boolean intersect(HouseElement element) {
        return intersect(element.getPosition(), element.getSize());
    }

    /**
     * Setzt die Position eines HouseElements fest
     *
     * @param position Position als Vector2
     */
    public void setPosition(Vector2 position) {
        this.position = position;
    }

    /**
     * Setzt die Größe eines HouseElements fest
     *
     * @param size Größe als Vector2
     */
    public void setSize(Vector2 size) {
        this.size = size;
    }

    /**
     * Gibt die Position eines HouseElements zurück
     *
     * @return Position als Vector2
     */
    public Vector2 getPosition() {
        return position;
    }

    /**
     * Gibt die Größe eines HouseElements zurück
     *
     * @return Größe als Vector2
     */
    public Vector2 getSize() {
        return size;
    }

    /**
     * Gibt die UUID eines HouseElements zurück
     *
     * @return UUID
     */
    public UUID getId() {
        return id;
    }

    /**
     * Gibt an ob eine HouseElement Instanz eine legale Transformation innerhalb eines Gebäudes darstellt.
     *
     * @param house das Gebäude in dem sich das HouseElement befinden soll
     * @return true, falls das HouseElement sich an einer erlaubten position befindet, ansonsten false
     */
    public abstract boolean isLegalTransform(House house);
}
