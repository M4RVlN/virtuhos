package model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import util.Vector2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.UUID;

/**
 * Diese Klasse implementiert ein Büro als eine Erweiterung eines Raumes.
 * Alle Parameter und zugehörige Funktionen eines Büros die über die eines Raumes hinausgehen werden in dieser Klasse beschrieben.
 *
 * @version 12.01.2021
 *
 * @author Marvin Öhlerking, Marc Herrmann
 */

public class Office extends Room {
	@Element(name = "type")
	private OfficeType type; //conference or standard office

	/**
	 * Konstruktor eines Office Objektes.
	 *
	 * @param room Raum der ein Büro werden soll
	 * @param type Typ des Büros
	 */
	public Office(Room room, OfficeType type){
		super(room.position, room.size, room.name, room.content, room.usersInRoom);
		this.setType(type);
	}

	/**
	 * Konstruktor eine Office Objektes. Erstellt eine Office Instanz mit den angegebenen Parametern.
	 *
	 * @param id UUID
	 * @param position Position im Gebäude
	 * @param size Abmessungen des Büros
	 * @param name Name des Büros für die GUI Darstellung
	 * @param type Typ des Büros
	 * @param content Inhalt des Büros (Tische/Stühle)
	 * @param usersInRoom Personen im Büro
	 */
	public Office(@Element(name = "id") UUID id,
				  @Element(name = "position") Vector2 position,
				  @Element(name = "size") Vector2 size,
				  @Element(name = "name") String name,
				  @Element(name = "type") OfficeType type,
				  @ElementList(name = "content") ArrayList<Content> content,
				  @ElementList(name = "users") ArrayList<Person> usersInRoom) {
		super(id, position, size, name, content, usersInRoom);
		this.setType(type);
	}

	/**
	 * Konstruktor eine Office Objektes. Erstellt eine Office Instanz mit den angegebenen Parametern.
	 *
	 * @param position Position im Gebäude
	 * @param size Abmessungen des Büros
	 * @param name Name des Büros für die GUI Darstellung
	 * @param type Typ des Büros
	 * @param content Inhalt des Büros (Tische/Stühle)
	 * @param usersInRoom Personen im Büro
	 */
	public Office(@Element(name = "position") Vector2 position,
				  @Element(name = "size") Vector2 size,
				  @Element(name = "name") String name,
				  @Element(name = "type") OfficeType type,
				  @Element(name = "content") ArrayList<Content> content,
				  @Element(name = "users") ArrayList<Person> usersInRoom) {
		super(position, size, name, content, usersInRoom);
		this.setType(type);
	}

	/**
	 * Konstruktor eine Office Objektes. Erstellt eine Office Instanz mit den angegebenen Parametern.
	 *
	 * @param position Position im Gebäude
	 * @param size Abmessungen des Büros
	 * @param name Name des Büros für die GUI Darstellung
	 * @param type Typ des Büros
	 */
	public Office(@Element(name = "position") Vector2 position,
				  @Element(name = "size") Vector2 size,
				  @Element(name = "name") String name,
				  @Element(name = "type") OfficeType type) {
		super(position, size, name);
		this.setType(type);
	}

	/**
	 * Konstruktor eine Office Objektes. Erstellt eine Office Instanz mit den angegebenen Parametern.
	 *
	 * @param position Position im Gebäude
	 * @param size Abmessungen des Büros
	 * @param type Typ des Büros
	 */
	public Office(@Element(name = "position") Vector2 position,
				  @Element(name = "size") Vector2 size,
				  @Element(name = "type") OfficeType type) {
		super(position, size, "B\u00fcro");
		this.setType(type);

		if(type == OfficeType.CONFERENCE)
			this.name = "Konferenz";
	}

	/**
	 * Fügt den übergebenen Inhalt einem Büro hinzu
	 *
	 * @param content Inhalt der hinzugefügt werden soll
	 */
	public void addContent(Content content) {
        this.content.add(content);
        this.content.sort(Comparator.comparing(Content::getId));
    }

	/**
	 * Gibt die Kapazität für Personen in einem Büro zurück.
	 * Diese ergibt sich aus der Anzahl der Stühle im Büro
	 *
	 * @return Anzahl der Stühle im Raum
	 */
	public int getCapacity() {
		int chairsInRoom = 0;

		for (Content value : this.content) {
			if (value instanceof Chair)
				chairsInRoom++;
		}
		return chairsInRoom;
	}

	/**
	 * Fügt einem Büro eine Person hinzu, falls noch freie Stühle vorhanden sind.
	 *
	 * @param person hinzuzufügende Person
	 * @return true falls die Person hinzugefügt werden konnte, ansonsten false
	 */
	@Override
	public boolean addUser(Person person) {
		if (usersInRoom.size() < getCapacity())
			return super.addUser(person);
		return false;
	}

	/**
	 * Legt den Typ des Büros fest
	 *
	 * @param type Bürotyp
	 */
	public void setType(OfficeType type) {
		this.type = type;
	}

	/**
	 * Gibt den Typ des Büros zurück
	 *
	 * @return Bürotyp
	 */
	public OfficeType getType() {
		return type;
	}

	/**
	 * Entfernt den übergebenen Inhalt aus dem Büro
	 *
	 * @param content zu entfernender Inhalt
	 */
	public void removeContent(Content content) {
        this.content.remove(content);
    }
}
