package model;

/**
 * Dieses Enum implementiert den Typ eines Büros.
 * Dieser ist entweder ein Standard Büro oder ein Konferenzraum.
 *
 * @version 12.01.2021
 *
 * @author Marc Herrmann
 */

public enum OfficeType { //to set if the room is a conference room
        DEFAULT, CONFERENCE
}
