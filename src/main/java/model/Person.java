package model;

import controller.BaseRenderController;
import org.simpleframework.xml.Element;
import util.Vector2;

import java.util.UUID;

/**
 * Diese Klasse implementiert eine Person, welche sich in Räumen aufhalten und bewegen kann.
 * @author Nicolas Fröhlich
 */
public class Person extends HouseElement {
    /**
     * Die Höhe und Breite einer Person im Model und der Ansicht.
     */
    public static final Vector2 PERSON_SIZE = new Vector2(BaseRenderController.GRID_SIZE-6, BaseRenderController.GRID_SIZE-2);

    @Element(name = "name")
    protected String name;
    @Element(name = "isAdmin")
    protected boolean isAdmin;

    /**
     * Ein Konstruktor der Klasse Person
     *
     * @param id Die UUID der Person
     * @param position Die Position der Person
     * @param name Der Name der Person
     * @param isAdmin true, falls die Person ein Admin ist. Sonst false
     */
    public Person(@Element(name = "id") UUID id,
                  @Element(name = "position") Vector2 position,
                  @Element(name = "name") String name,
                  @Element(name = "isAdmin") boolean isAdmin) {
        super(id, position, PERSON_SIZE);
        this.name = name;
        this.isAdmin = isAdmin;
    }

    /**
     * Ein Konstruktor der Klasse Person
     *
     * @param id Die UUID der Person
     * @param position Die Position der Person
     * @param name Der Name der Person
     */
    public Person(@Element(name = "id") UUID id,
                  @Element(name = "position") Vector2 position,
                  @Element(name = "name") String name) {
        this(id, position, name, false);
    }

    /**
     * Ein Konstruktor der Klasse Person
     *
     * @param position Die Position der Person
     * @param name Der Name der Person
     */
    public Person(@Element(name = "position") Vector2 position,
                  @Element(name = "name") String name) {
        super(position, PERSON_SIZE);
        this.name = name;
    }

    @Override
    public void setSize(Vector2 size) { }

    /**
     * Gibt den Name der Person zurück
     * @return Der Name der Person
     */
    public String getName(){
        return name;
    }

    /**
     * Ändert den Name der Person
     * @param name Der neue Name der Person
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Zeigt an ob die Person ein Admin ist
     * @return true wenn ein Admin, sonst false
     */
    public boolean isAdmin(){
        return isAdmin;
    }

    /**
     * Gibt zurück, ob die aktuelle Position der Person erlaubt ist.
     * Eine Position ist erlaubt, wenn sie innerhalb des Gebäudes liegt und innerhalb eines Raums.
     * Dabei wird auch die Größe der Person beachtet.
     *
     * @param house Das Gebäude Objekt, indem sich die Person befinden muss
     * @return true, falls die aktuelle Position der Person erlaubt ist. Sonst false
     */
    @Override
    public boolean isLegalTransform(House house) {
        if (!house.isInBounds(this))
            return false;

        for(Room r : house.getRoomList()) {
            if(r.isInBounds(this))
                return true;
        }

        return false;
    }
}

