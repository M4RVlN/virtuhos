package model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.UUID;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import util.Vector2;

/**
 * Diese Klasse implementiert das abstrakte Konzept eines Raumes für das Modell des Hauses im VirtuHos Projekt.
 * Alle Parameter und zugehörige Funktionen eines Raumes werden in dieser Klasse beschrieben.
 *
 * @version 12.01.2021
 *
 * @author Marvin Öhlerking, Marc Herrmann
 */

public abstract class Room extends HouseElement {
    @Element(name = "name")
    protected String name;
    @ElementList(name = "content", required = false)
    protected ArrayList<Content> content;
    @ElementList(name = "users", required = false)
    protected ArrayList<Person> usersInRoom;

    /**
     * Konstruktor der Klasse Room, erzeugt eine Room Instanz, mit den Angegebenen Parametern.
     *
     * @param id UUID
     * @param position Position der linken oberen Ecke des Raumes
     * @param size Größe des Raumes (Höhe, Breite)
     * @param name Name des Raumes der in der GUI angezeigt werden soll
     * @param content Inhalt des Raumes (Stühle, Tische)
     * @param usersInRoom Personen die sich im Raum befinden
     */
    public Room(@Element(name = "id") UUID id,
                @Element(name = "position") Vector2 position,
                @Element(name = "size") Vector2 size,
                @Element(name = "name") String name,
                @ElementList(name = "content") ArrayList<Content> content,
                @ElementList(name = "users") ArrayList<Person> usersInRoom) {
        super(id, position, size);
        this.name = name;
        this.content = content;
        this.usersInRoom = usersInRoom;
    }

    /**
     * Konstruktor der Klasse Room, erzeugt eine Room Instanz, mit den Angegebenen Parametern.
     *
     * @param position Position der linken oberen Ecke des Raumes
     * @param size Größe des Raumes (Höhe, Breite)
     * @param name Name des Raumes der in der GUI angezeigt werden soll
     * @param content Inhalt des Raumes (Stühle, Tische)
     * @param usersInRoom Personen die sich im Raum befinden
     */
    public Room(@Element(name = "position") Vector2 position,
                @Element(name = "size") Vector2 size,
                @Element(name = "name") String name,
                @ElementList(name = "content") ArrayList<Content> content,
                @ElementList(name = "users") ArrayList<Person> usersInRoom) {
        super(position, size);
        this.name = name;
        this.content = content;
        this.usersInRoom = usersInRoom;
    }

    /**
     * Konstruktor der Klasse Room, erzeugt eine Room Instanz, mit den Angegebenen Parametern.
     *
     * @param position Position der linken oberen Ecke des Raumes
     * @param size Größe des Raumes (Höhe, Breite)
     * @param name Name des Raumes der in der GUI angezeigt werden soll
     */
    public Room(@Element(name = "position") Vector2 position,
                @Element(name = "size") Vector2 size,
                @Element(name = "name") String name) {
        this(position, size, name, new ArrayList<>(), new ArrayList<>());
    }

    /**
     * Konstruktor der Klasse Room, erzeugt eine Room Instanz, mit den Angegebenen Parametern.
     *
     * @param position Position der linken oberen Ecke des Raumes
     * @param size Größe des Raumes (Höhe, Breite)
     */
    public Room(Vector2 position, Vector2 size) {
        this(position, size, "Untitled", new ArrayList<>(), new ArrayList<>());
    }

    /**
     * Gibt den Namen des Raumes zurück
     *
     * @return Name des Raumes
     */
    public String getName(){
        return this.name;
    }

    /**
     * Legt den Namen eines Raumes fest
     *
     * @param name Bezeichnung für den Raum
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * Gibt den Inhalt eines Raumes zurück
     *
     * @return Inhalt des Raumes
     */
    public ArrayList<Content> getContent(){
        return this.content;
    }

    /**
     * Legt den Inhalt eines Raumes fest
     *
     * @param content Inhalt für den Raum
     */
    public void setContent(ArrayList<Content> content){
        this.content = content;
    }

    /**
     * Gibt die Personen die sich im Raum befinden zurück
     *
     * @return Personen im Raum
     */
    public ArrayList<Person> getUsers(){
        return this.usersInRoom;
    }

    /**
     * Legt die Personen die sich im Raum befinden fest
     *
     * @param usersInRoom Personen die in den Raum sollen
     */
    public void setUsers(ArrayList<Person> usersInRoom){
        this.usersInRoom = usersInRoom;
    }

    /**
     * Gibt den Inhalt des Raumes an einer bestimmten Position im Raum zurück, oder null falls sich nichts an der Position befindet
     *
     * @param pos Position des gesuchten Inhalts
     * @return das Gefundene Content Objekt, null falls sich kein Content an der angegebenen Position befindet
     */
    public Content getContent(Vector2 pos) {
        for (Content value : getContent()) {
            if (value.getPosition().equals(pos))
                return value;
        }
        return null;
    }

    /**
     * Gibt den Inhalt des Raumes mit einer bestimmten UUID zurück, oder null falls es keinen Inhalt mit der UUID gibt
     *
     * @param id UUID des gesuchten Inhalts
     * @return das Gefundene Content Objekt, null falls der Raum kein Content mit der angegebenen UUID enthält
     */
    public Content getContent(UUID id) {
        for (Content value : getContent()) {
            if (value.getId().equals(id))
                return value;
        }
        return null;
    }

    /**
     * Gibt eine Person im Raum mit einer bestimmten UUID zurück, oder null falls es keinen Person mit der UUID im Raum ist
     *
     * @param id UUID der gesuchten Person
     * @return die Gefundene Person, null falls der Raum keine Person mit der angegebenen UUID enthält
     */
    public Person getUser(UUID id) {
        for(Person p : usersInRoom) {
            if (p.id.equals(id)) {
                return p;
            }
        }
        return null;
    }

    /**
     * Fügt dem Raum eine Person hinzu.
     *
     * @param person hinzuzufügende Person
     * @return true, nachdem die Person hinzugefügt wurde
     */
    public boolean addUser(Person person) {
        usersInRoom.add(person);
        usersInRoom.sort(Comparator.comparing(Person::getName));
        return true;
    }

    /**
     * Entfernt eine Person aus dem Raum
     *
     * @param person zu entfernende Person
     * @return true, falls die Person erfolgreich entfernt wurde
     */
    public boolean removeUser(Person person) {
        return usersInRoom.remove(person);
    }

    /**
     * Gibt an ob eine Raum Instanz eine erlaubte transformation innerhalb des Gebäudes darstellt
     *
     * @param house das Gebäude in dem der Raum platziert werden soll
     * @return true, falls erlaubt, sonst false
     */
    @Override
    public boolean isLegalTransform(House house) {
        if (!house.isInBounds(this))
            return false;

        for(Room r : house.getRoomList())
        {
            if(r.getId().equals(getId()))
                continue;

            if(r.intersect(this))
                return false;
        }

        for(Content c : getContent()) {
            if(!isInBounds(c.getPosition(), c.getSize()))
                return false;
        }

        return true;
    }
}