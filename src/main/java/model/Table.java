package model;

import org.simpleframework.xml.Element;
import util.Vector2;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Diese Klasse implementiert ein Tisch als Inhalt eines Raumes.
 * Alle Parameter und zugehörige Funktionen eines Tisches werden in dieser Klasse beschrieben.
 *
 * @version 12.01.2021
 *
 * @author Marvin Öhlerking, Marc Herrmann
 */

public class Table extends Content {
    //private final ArrayList<Document> attachedDocuments = new ArrayList<>();

    /**
     * Konstruktor eines Tisches. Erstellt eine Tisch-Instanz der angegebenen Position und Größe.
     *
     * @param position Position des Tisches
     * @param size Größe des Tisches
     */
    public Table(@Element(name = "position") Vector2 position,
                 @Element(name = "size") Vector2 size) {
        super(position, size);
    }

    /**
     * Konstruktor eines Tisches. Erstellt eine Tisch-Instanz der angegebenen UUID, Position und Größe.
     *
     * @param id UUID des Tisches
     * @param position Position des Tisches
     * @param size Größe des Tisches
     */
    public Table(@Element(name = "id") UUID id,
                 @Element(name = "position") Vector2 position,
                 @Element(name = "size") Vector2 size) {
        super(id, position, size);
    }

    /**
     * Gibt an ob eine Tisch Instanz eine erlaubte transformation innerhalb des Gebäudes darstellt
     *
     * @param house das Gebäude indem sich der Tisch befinden soll
     * @return true, falls der Tisch sich an einer erlaubten position befindet, ansonsten false
     */
    @Override
    public boolean isLegalTransform(House house) {
        if (!house.isInBounds(this))
            return false;

        Room containedRoom = null;
        //Check if in a room
        for(Room r : house.getRoomList())
        {
            if(r.getContent().contains(this)) {
                containedRoom = r;
                break;
            }
        }

        if(containedRoom == null || !containedRoom.isInBounds(this))
            return false;

        for(Content c : containedRoom.getContent()) {
            if(c.getId().equals(id))
                continue;

            if(c.intersect(this))
                return false;
        }
        return true;
    }

    /**
     * Gibt an ob eine Tisch Instanz eine erlaubte transformation innerhalb des Gebäudes darstellt
     *
     * @param house das Gebäude indem sich der Tisch befinden soll
     * @return true, falls der Tisch sich an einer erlaubten position befindet, ansonsten false
     */
    public boolean isLegalTransform(House house, Room room) {
        if (!house.isInBounds(this) || !room.isInBounds(this))
            return false;

        for(Content c : room.getContent()) {
            if(c.getId().equals(id))
                continue;

            if(c.intersect(this))
                return false;
        }
        return true;
    }

    /**
     * Gibt die an einen Tisch angehefteten Dokumente zurück
     *
     * @return Dokumente vom Tisch
     */

    //public ArrayList<Document> getAttachedDocuments() { return attachedDocuments; }

    /**
     * Fügt einem Tisch ein Dokument hinzu
     *
     * @param doc Dokument das dem Tisch hinzugefügt werden soll
     */
    //public void addDocument(Document doc){attachedDocuments.add(doc);}

    /**
     * Entfernt das angegebene Dokument vom Tisch
     *
     * @param doc zu entfernendes Dokument
     */
    //public void removeDocument(Document doc){attachedDocuments.remove(doc);}
}