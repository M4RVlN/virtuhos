package network.database;

import java.util.UUID;
import javafx.scene.paint.Color;
import org.simpleframework.xml.Element;

/**
 * Datenstruktur für Kreise der Analyse
 */
public class AnalysisCircle {
    @Element(name = "circleId")
    private UUID circleId;
    @Element(name = "circleRadius")
    private double circleRadius;
    @Element(name = "circeColor")
    private String circeColor;

    public AnalysisCircle(@Element(name = "circleId") UUID circleId,
                          @Element(name = "circleRadius") double circleRadius,
                          @Element(name = "circeColor") String circeColor) {
        this.circleId = circleId;
        this.circleRadius = circleRadius;
        this.circeColor = circeColor;
    }

    public AnalysisCircle(UUID circleId, double circleRadius, Color circeColor) {
        this(circleId, circleRadius, circeColor.toString());
    }

    public UUID getCircleId() {
        return circleId;
    }

    public void setCircleId(UUID circleId) {
        this.circleId = circleId;
    }

    public double getCircleRadius() {
        return circleRadius;
    }

    public void setCircleRadius(double circleRadius) {
        this.circleRadius = circleRadius;
    }

    public Color getCirceColor() {
        return Color.valueOf(circeColor);
    }

    public void setCirceColor(Color circeColor) {
        this.circeColor = circeColor.toString();
    }
}
