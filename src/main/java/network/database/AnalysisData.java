package network.database;

import java.util.ArrayList;
import org.simpleframework.xml.ElementList;

/**
 * Datenstruktur für Kreise und Linen der Analyse
 */
public class AnalysisData {
    @ElementList(name = "lines")
    private final ArrayList<AnalysisLine> lines;
    @ElementList(name = "circles")
    private final ArrayList<AnalysisCircle> circles;

    public AnalysisData(@ElementList(name = "lines") ArrayList<AnalysisLine> lines,
                        @ElementList(name = "circles") ArrayList<AnalysisCircle> circles) {
        this.lines = lines;
        this.circles = circles;
    }

    public ArrayList<AnalysisLine> getLines() {
        return lines;
    }

    public ArrayList<AnalysisCircle> getCircles() {
        return circles;
    }
}
