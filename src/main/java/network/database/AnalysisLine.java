package network.database;

import java.util.UUID;
import javafx.scene.paint.Color;
import org.simpleframework.xml.Element;

/**
 * Datenstruktur für Linien der Analyse
 */
public class AnalysisLine {
    @Element(name = "lineIdFrom")
    private UUID lineIdFrom;
    @Element(name = "lineIdTo")
    private UUID lineIdTo;
    @Element(name = "lineWidth")
    private double lineWidth;
    @Element(name = "lineColor")
    private String lineColor;

    public AnalysisLine(@Element(name = "lineIdFrom") UUID lineIdFrom,
                        @Element(name = "lineIdTo") UUID lineIdTo,
                        @Element(name = "lineWidth") double lineWidth,
                        @Element(name = "lineColor") String lineColor) {
        this.lineIdFrom = lineIdFrom;
        this.lineIdTo = lineIdTo;
        this.lineWidth = lineWidth;
        this.lineColor = lineColor;
    }

    public AnalysisLine(UUID lineIdFrom, UUID lineIdTo, double lineWidth, Color lineColor) {
        this(lineIdFrom, lineIdTo, lineWidth, lineColor.toString());
    }

    public UUID getLineIdFrom() { return lineIdFrom; }

    public UUID getLineIdTo() { return lineIdTo; }

    public void setLineIdFrom(UUID lineIdFrom) { this.lineIdFrom = lineIdFrom; }

    public void setLineIdTo(UUID lineIdTo) { this.lineIdTo = lineIdTo; }

    public double getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(double lineWidth) {
        this.lineWidth = lineWidth;
    }

    public Color getLineColor() {
        return Color.valueOf(lineColor);
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor.toString();
    }
}
