package network.database;
/**
 * This class represents all constants, needed
 * to establish connection with a database
 * of other subparts of VirtuHoS project.
 * @Author Alex Hahn, Philipp Brandes.
 */
public class DBConst {
    public static final String TABLE_GUI_CIRCLES = "ANALYSE_gui_circles";
    public static final String TABLE_GUI_LINES = "ANALYSE_gui_lines";

    public static final String ID = "id";
    public static final String ID_TO = "idTo";
    public static final String ID_FROM = "idFrom";
    public static final String RADIUS = "radius";
    public static final String COLOR = "color";
    public static final String WIDTH = "width";
}
