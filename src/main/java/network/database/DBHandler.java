package network.database;

import config.AppProperties;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;
import javafx.scene.paint.Color;

/**
 * This class is used to establish a connection to Database for a data exchange
 * with other subparts of VirtuHoS project.
 * All parameters for this class such as name of database, port's numbers,
 * password and a host name are set in Config file
 * @author Alex Hahn, Philipp Brandes.
 */
public class DBHandler {
    /**
     * this is the main method of a DBHandler class which used to establish a
     * connection to database located on server.
     * @return DriverManager.getConnection(connection_string, user, password) in success case, (successful connected)
     * @throws SQLException in case of error
     */
    public static Connection getConnection() throws SQLException{
        String dbHost = AppProperties.DB_HOST.getString();
        String dbPort = AppProperties.DB_PORT.getString();
        String dbName = AppProperties.DB_NAME.getString();
        String dbUser = AppProperties.DB_USER.getString();
        String dbPassword = AppProperties.DB_PASSWORD.getString();

        String connectStr = "jdbc:mysql://" + dbHost + ":" +dbPort + "/" + dbName;
        return DriverManager.getConnection(connectStr, dbUser, dbPassword);
    }

    /**
     * this method creates all lines from Analyse3 table and places them in ArrayList<AnalysisLine>
     * @return lineList - list of all lines from Analyse3 table
     */
    public static ArrayList<AnalysisLine> getLines() {
        try(Connection connection = getConnection()) {
            PreparedStatement pst = connection.prepareStatement("SELECT * FROM " + DBConst.TABLE_GUI_LINES);
            ResultSet lines =  pst.executeQuery();
            ArrayList<AnalysisLine> lineList = new ArrayList<>();

            while (lines.next()) {
                UUID from = UUID.fromString(lines.getString(DBConst.ID_FROM));
                UUID to = UUID.fromString(lines.getString(DBConst.ID_TO));
                double width = Double.parseDouble(lines.getString(DBConst.WIDTH));
                Color color = Color.web(lines.getString(DBConst.COLOR));
                lineList.add(new AnalysisLine(from, to, width, color));
            }

            connection.close();
            return lineList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * this method creates all circles from Analyse3 table and places them in ArrayList<AnalysisCircle>
     * @return circleList - list of all circles from Analyse3 table
     */
    public static ArrayList<AnalysisCircle> getCircles() {
        try(Connection connection = getConnection()) {
            PreparedStatement pst = connection.prepareStatement("SELECT * FROM " + DBConst.TABLE_GUI_CIRCLES);
            ResultSet circles =  pst.executeQuery();
            ArrayList<AnalysisCircle> circleList = new ArrayList<>();

            while (circles.next()) {
                UUID id = UUID.fromString(circles.getString(DBConst.ID));
                double radius = Double.parseDouble(circles.getString(DBConst.RADIUS));
                Color color = Color.web(circles.getString(DBConst.COLOR));
                circleList.add(new AnalysisCircle(id, radius, color));
            }

            connection.close();
            return circleList;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * this method creates places all lines and circles from Analyse3 in AnalysisData class for further use
     * @return AnalysisData - object which has all lines and circles from Analyse3 table
     */
    public static AnalysisData getAnalysisData() {
        ArrayList<AnalysisLine> lines = getLines();
        ArrayList<AnalysisCircle> circles = getCircles();

        if(lines == null || circles == null)
            return null;

        return new AnalysisData(lines, circles);
    }
}
