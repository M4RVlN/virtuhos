package network.http;

import config.AppProperties;
import network.http.events.InteractionAddDocument;
import network.http.events.InteractionEvent;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

/**
 * Diese Klasse implementiert die Kommunikation mit dem HTTP Server der Interaktion.
 * Es sind GET und POST Request implementiert. Diese werden über InteractionEvents an die Interaktion übermittelt.
 * @author Marvin Öhlerking, Nicolas Fröhlich
 */
public class InteractionHandler {
    /**
     * Sendet eine Anfrage an den HTTP Server der Interaktion
     * @param <T> Die InteractionEvent.Response Klasse, welche zurückgegeben werden soll
     * @param event Das Event welches ausgelöst werden soll
     * @param responseType Der Typ der Response des Events
     * @return Gibt das Response Objekt der empfangenen Nachricht zurück
     */
    public static <T extends InteractionEvent.Response> T sendPostRequest(InteractionEvent.Request event, Class<T> responseType) {
        String response;
        try {
            response = sendPostRequest(event.getSubURL(), event.getParameters());
            System.out.println("Interaction: " + response);
        } catch (IOException e) {
            //Error during communication
            System.err.println("Error during communication to interaction server");
            return null;
        }

        // Create response object
        try {
            if(responseType == InteractionAddDocument.Response.class){
                return responseType.getDeclaredConstructor(JSONObject.class).newInstance((new JSONObject()).put("success", Boolean.parseBoolean(response)));
            }
            Object obj = new JSONTokener(response).nextValue();

            if(obj instanceof JSONObject) {
                JSONObject json = (JSONObject) obj;
                //Error on interaction side
                if(json.has("returnCode") && !json.getBoolean("returnCode"))
                    return null;

                return responseType.getDeclaredConstructor(JSONObject.class).newInstance(json);
            } else if(obj instanceof JSONArray) {
                return responseType.getDeclaredConstructor(JSONArray.class).newInstance(obj);
            } else {
                return null;
            }
        } catch (Exception e) {
            //Not a valid response
            System.err.println("Error during parsing of response from interaction server");
            return null;
        }
    }

    /**
     * Sendet eine Anfrage an den HTTP Server der Interaktion
     * @param event Das Event welches ausgelöst werden soll
     * @param responseType Der Typ der Response des Events
     * @return Gibt das Response Objekt der empfangenen Nachricht zurück
     */
    public static <T extends InteractionEvent.Response> T sendGetRequest(InteractionEvent.Request event, Class<T> responseType) {
        String response;
        try {
            response = sendGetRequest(event.getSubURL());
            System.out.println("Interaction: " + response);
        } catch (IOException e) {
            //Error during communication
            System.err.println("Error during communication to interaction server");
            return null;
        }

        // Create response object
        try {
            Object obj = new JSONTokener(response).nextValue();

            if(obj instanceof JSONObject) {
                JSONObject json = (JSONObject) obj;
                //Error on interaction side
                if(json.has("returnCode") && !json.getBoolean("returnCode"))
                    return null;

                return responseType.getDeclaredConstructor(JSONObject.class).newInstance(json);
            } else if(obj instanceof JSONArray) {
                return responseType.getDeclaredConstructor(JSONArray.class).newInstance((JSONArray)obj);
            } else {
                return null;
            }
        } catch (Exception e) {
            //Not a valid response
            System.err.println("Error during parsing of response from interaction server");
            return null;
        }
    }

    /**
     * Sendet eine Anfrage an den HTTP Server der Interaktion
     * @param event Das Event welches ausgelöst werden soll
     * @return Gibt die empfangenen Nachricht zurück
     */
    public static String sendGetRequest(InteractionEvent.Request event){
        String response;
        try {
            response = sendGetRequest(event.getSubURL());
            System.out.println("Interaction: " + response);
            return response;
        } catch (IOException e) {
            //Error during communication
            System.err.println("Error during communication to interaction server");
            e.printStackTrace();
            return "";
        }
    }

    /**
     * Sendet eine POST Request an den HTTP Server der Interaktion
     * @param subUrl Der URL Pfad
     * @param parameters Das JSON Objekt mit den Parametern für das Event
     * @return Gibt den empfangenen String zurück
     * @throws IOException Fehler bei der Kommunikation mit dem Server
     */
    private static String sendPostRequest(String subUrl, JSONObject parameters) throws IOException {
        //Init connection
        URL url = new URL("http://" + AppProperties.INTERACTION_HOST.getString() + ":" + AppProperties.INTERACTION_PORT.getString() + subUrl);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        //Set communication type
        con.setRequestMethod("POST");
        //Only allow json format
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        //Set timeout
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        con.setDoOutput(true);

        //Send request
        DataOutputStream out = new DataOutputStream(con.getOutputStream());
        byte[] input = parameters.toString().getBytes(StandardCharsets.UTF_8);
        System.out.write(input);
        System.out.println();
        out.write(input, 0, input.length);
        out.flush();
        out.close();

        //Read response
        int status = con.getResponseCode();
        Reader streamReader;
        if (status > 299) { //Failed
            System.err.println("Error Status: " + status);
            streamReader = new InputStreamReader(con.getErrorStream());
        } else { //Success
            streamReader = new InputStreamReader(con.getInputStream());
        }
        BufferedReader in = new BufferedReader(streamReader);

        //Build string from response
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();

        //Close connection
        con.disconnect();

        return content.toString();
    }

    /**
     * Sendet eine GET Request an den HTTP Server der Interaktion
     * @param subUrl Der URL Pfad
     * @return Gibt den empfangenen String zurück
     * @throws IOException Fehler bei der Kommunikation mit dem Server
     */
    public static String sendGetRequest(String subUrl) throws IOException{
        //Init connection
        System.out.println("http://" + AppProperties.INTERACTION_HOST.getString() + ":" + AppProperties.INTERACTION_PORT.getString() + subUrl);
        URL url = new URL("http://" + AppProperties.INTERACTION_HOST.getString() + ":" + AppProperties.INTERACTION_PORT.getString() + subUrl);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        //Set communication type
        con.setRequestMethod("GET");

        //Set timeout
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        con.setDoOutput(true);

        //Read response
        int status = con.getResponseCode();

        System.out.println(status);

        Reader streamReader;
        if (status > 299) { //Failed
            System.err.println("Error Status: " + status);
            streamReader = new InputStreamReader(con.getErrorStream());
        } else { //Success
            streamReader = new InputStreamReader(con.getInputStream());
        }
        BufferedReader in = new BufferedReader(streamReader);

        //Build string from response
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();

        //Close connection
        con.disconnect();

        return content.toString();
    }
}
