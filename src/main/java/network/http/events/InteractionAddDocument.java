package network.http.events;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.UUID;

/**
 * Diese Klasse dient zur Kommunikation mit der Interaktion, um Dokumente zu erstellen.
 * @author Nicolas Fröhlich
 */
public class InteractionAddDocument extends InteractionEvent {

    /**
     * Definiert die Parameter für die Anfrage des AddDocument
     */
    public static class Request extends InteractionEvent.Request {
        private final String data;
        private final String name;
        private final UUID uploader;
        private final UUID meetingId;
        private final int id;
        private final String loginToken;

        /**
         * Der Konstruktor, um ein Request-Objekt zuerstellen, welches an die Interaktion über HTTP übermittelt wird
         * @param data Der Inhalt des Dokuments
         * @param name Der Name des Dokuemnts, inklusive Dateiendung
         * @param uploader Die UUID des Erstellers des Dokuments
         * @param meetingId Die UUID des Raums, in dem das Dokument erstellt werden soll
         * @param id Die ID des Dokuments. Wird i. d. R. von der Interaktion festgelegt
         * @param loginToken Das LokinToken, welches mit an die Interaktion übermittelt wird. Muss Adminrechte beinhalten
         */
        public Request(String data, String name, UUID uploader, UUID meetingId, int id, String loginToken) {
            this.data = data;
            this.name = name;
            this.uploader = uploader;
            this.meetingId = meetingId;
            this.id = id;
            this.loginToken = loginToken;
        }

        @Override
        public String getSubURL() {
            return "/" + loginToken + "/virtuhos/add-doc";
        }

        @Override
        public JSONObject getParameters() {
            JSONObject params = new JSONObject();

            params.put("name", name);
            params.put("uploader", uploader.toString());
            params.put("meetingId", meetingId.toString());
            params.put("id", id);
            params.put("data", data);
            return params;
        }
    }

    /**
     * Definiert die Auflösung der Parameter für die Antwort des AddDocument
     */
    public static class Response extends InteractionEvent.Response {
        /**
         * Gib an, ob das Erstellen des übermittelten Dokuments erfolgreich war.
         */
        public final boolean success;

        /**
         * Der Konstruktor für die Response der Interation
         * @param json Das von der Interaktion übermittelte JSON-Objekt
         */
        public Response(JSONObject json) {
            super(json);
            success = json.getBoolean("success");
        }
    }
}
