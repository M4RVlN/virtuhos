package network.http.events;

import org.json.JSONObject;
/**
 * Diese Klasse dient zur Kommunikation mit der Interaktion, um Benutzer zu erstellen.
 * @author Nicolas Fröhlich
 */
public class InteractionAddUser extends InteractionEvent {

    /**
     * Definiert die Parameter für die Anfrage der AddUser
     */
    public static class Request extends InteractionEvent.Request {
        private final String userName;
        private final String displayName;
        private final boolean isAdmin;
        private final String loginToken;

        /**
         * Der Konstruktor, um ein Request-Objekt zuerstellen, welches an die Interaktion über HTTP übermittelt wird
         * @param userName Der Benutzername, mit dem sich der Benutzer einloggen kann
         * @param displayName Der Displayname, welcher andern Nutzern angezeigt wird
         * @param isAdmin Gibt an, ob der Benutzer als Administrator erstellt werden soll
         * @param loginToken Das LokinToken, welches mit an die Interaktion übermittelt wird. Muss Adminrechte beinhalten
         */
        public Request(String userName, String displayName, boolean isAdmin, String loginToken) {
            this.userName = userName;
            this.displayName = displayName;
            this.isAdmin = isAdmin;
            this.loginToken = loginToken;
        }

        @Override
        public String getSubURL() {
            return "/" + loginToken + "/add-user/" + userName + "/" + displayName + "/" + (isAdmin ? 1 : 0);
        }

        @Override
        public JSONObject getParameters() {
            JSONObject params = new JSONObject();
            return params;
        }
    }

    /**
     * Definiert die Auflösung der Parameter für die Antwort der AddUser
     */
    public static class Response extends InteractionEvent.Response {

        /**
         * Der Konstruktor für die Response der Interation
         * @param json Das von der Interaktion übermittelte JSON-Objekt
         */
        public Response(JSONObject json) {
            super(json);
        }
    }
}
