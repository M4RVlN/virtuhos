package network.http.events;

import config.AppProperties;
import java.util.UUID;
import network.tcp.Server;
import org.json.JSONException;
import org.json.JSONObject;
import util.JSONUtil;

public class InteractionCreateMeeting extends InteractionEvent {

    /**
     * Definiert die Parameter für die Anfrage des CreateMeetingEvents
     */
    public static class Request extends InteractionEvent.Request {

        public final UUID meetingID;
        public final String meetingName;
        public final String attendeePW;
        public final String moderatorPW;
        public final UUID parentMeetingID;
        public final int maxParticipants;
        public final int breakoutSequenceNr;

        /**
         * Erstellt ein BBB-Meeting
         * @param meetingID
         * @param meetingName
         * @param maxParticipants
         */
        public Request(UUID meetingID, String meetingName, int maxParticipants) {
            this(meetingID, meetingName, maxParticipants, null, 0);
        }

        /**
         * Erstellt einen BBB-Breakout Room in einem Meeting
         * @param meetingID
         * @param meetingName
         * @param maxParticipants
         * @param parentMeetingID
         * @param breakoutSequenceNr
         */
        public Request(UUID meetingID, String meetingName, int maxParticipants, UUID parentMeetingID, int breakoutSequenceNr) {
            this.meetingID = meetingID;
            this.meetingName = meetingName;
            this.attendeePW = AppProperties.BBB_PASSWORD.getString();
            this.moderatorPW = AppProperties.BBB_ADMIN_PASSWORD.getString();
            this.maxParticipants = maxParticipants;
            this.parentMeetingID = parentMeetingID;
            this.breakoutSequenceNr = breakoutSequenceNr;
        }

        @Override
        public String getSubURL() {
            return "/" + Server.INSTANCE.getStaticToken() + "/bbb/create-meeting";
        }

        @Override
        public JSONObject getParameters() {
            JSONObject params = new JSONObject();

            boolean isBreakout = parentMeetingID != null;
            String welcomeMsg = "";
            boolean record = false;

            params.put("apiName", "create");
            params.put("meetingName", meetingName);
            params.put("meetingID", meetingID.toString());
            params.put("attendeePW", attendeePW);
            params.put("moderatorPW", moderatorPW);
            params.put("breakout", Boolean.toString(isBreakout));
            params.put("welcomeMsg", welcomeMsg);
            params.put("maxParticipants", Integer.toString(maxParticipants));
            params.put("record", Boolean.toString(record));

            params.put("parentMeetingID", isBreakout ? parentMeetingID.toString() : "");
            params.put("sequence", breakoutSequenceNr);

            JSONObject request = new JSONObject();
            request.put("moderatorPW", moderatorPW);
            request.put("record", Boolean.toString(record));
            request.put("name", meetingName);
            request.put("attendeePW", attendeePW);
            request.put("meetingID", meetingID.toString());
            request.put("isBreakout", isBreakout);
            request.put("welcome", welcomeMsg);
            request.put("maxParticipants", Integer.toString(maxParticipants));
            if (isBreakout) {
                request.put("parentMeetingID", parentMeetingID.toString());
                request.put("sequence", breakoutSequenceNr);
            }
            params.put("requestParameters", request);

            return params;
        }
    }

    /**
     * Definiert die Auflösung der Parameter für die Antwort des CreateMeetingEvents
     */
    public static class Response extends InteractionEvent.Response {

        /**
         * ID of the Meeting
         */
        public final String meetingID;
        /**
         * ID of the Internal Meeting
         */
        public final String internalMeetingID;
        /**
         * ID of the parent Meeting
         */
        public final String parentMeetingID; // <- bbb-none => NULL, nur für breakout-rooms

        public final String attendeePW;
        public final String moderatorPW;

        public final String name;

        public Response(JSONObject json) throws JSONException {
            super(json);
            this.meetingID = json.getString("meetingID");
            this.internalMeetingID = json.getString("internalMeetingID");
            this.parentMeetingID = JSONUtil.getOrNull(json.get("parentMeetingID"), String.class);
            this.name = json.getString("name");
            this.attendeePW = json.getString("attendeePW");
            this.moderatorPW = json.getString("moderatorPW");
        }
    }
}
