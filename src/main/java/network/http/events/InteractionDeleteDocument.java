package network.http.events;

import org.json.JSONObject;

import java.util.UUID;

/**
 * Diese Klasse dient zur Kommunikation mit der Interaktion, um Dokumente zu entfernen.
 * @author Nicolas Fröhlich
 */

public class InteractionDeleteDocument extends InteractionEvent {

    /**
     * Definiert die Parameter für die Anfrage der DeleteDocument
     */
    public static class Request extends InteractionEvent.Request {
        private final int id;
        private final String loginToken;

        /**
         * Der Konstruktor, um ein Request-Objekt zuerstellen, welches an die Interaktion über HTTP übermittelt wird
         * @param id         Die ID des zu löschenden Dokuments
         * @param loginToken Das LokinToken, welches mit an die Interaktion übermittelt wird. Muss Adminrechte beinhalten
         */
        public Request(int id, String loginToken) {
            this.id = id;
            this.loginToken = loginToken;
        }

        @Override
        public String getSubURL() {
            return "/" + loginToken + "/virtuhos/del-doc/" + this.id;
        }

        @Override
        public JSONObject getParameters() {
            JSONObject params = new JSONObject();
            return params;
        }
    }

    /**
     * Definiert die Auflösung der Parameter für die Antwort der DeleteDocument
     */
    public static class Response extends InteractionEvent.Response {

        /**
         * Der Konstruktor für die Response der Interation
         * @param json Das von der Interaktion übermittelte JSON-Objekt
         */
        public Response(JSONObject json) {
            super(json);
        }
    }
}
