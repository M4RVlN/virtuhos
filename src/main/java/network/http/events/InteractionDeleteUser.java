package network.http.events;

import org.json.JSONObject;
/**
 * Diese Klasse dient zur Kommunikation mit der Interaktion, um User zu entfernen.
 * @author Nicolas Fröhlich
 */

public class InteractionDeleteUser {
    /**
     * Definiert die Parameter für die Anfrage der DeleteUser
     */
    public static class Request extends InteractionEvent.Request {
        private final String userName;
        private final String loginToken;

        /**
         * Der Konstruktor, um ein Request-Objekt zuerstellen, welches an die Interaktion über HTTP übermittelt wird
         * @param userName Der Username des zu löschenden Benutzers
         * @param loginToken Das LokinToken, welches mit an die Interaktion übermittelt wird. Muss Adminrechte beinhalten
         */
        public Request(String userName, String loginToken) {
            this.userName = userName;
            this.loginToken = loginToken;
        }

        @Override
        public String getSubURL() {
            return "/" + loginToken + "/delete-user/" + userName;
        }

        @Override
        public JSONObject getParameters() {
            JSONObject params = new JSONObject();
            return params;
        }
    }

    /**
     * Definiert die Auflösung der Parameter für die Antwort der DeleteUser
     */
    public static class Response extends InteractionEvent.Response {
        /**
         * Der Konstruktor für die Response der Interation
         * @param json Das von der Interaktion übermittelte JSON-Objekt
         */
        public Response(JSONObject json) {
            super(json);
        }
    }
}
