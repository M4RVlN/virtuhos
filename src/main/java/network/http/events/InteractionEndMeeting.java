package network.http.events;

import config.AppProperties;
import java.util.UUID;
import network.tcp.Server;
import org.json.JSONObject;

public class InteractionEndMeeting extends InteractionEvent {

    /**
     * Definiert die Parameter für die Anfrage des EndMeetingEvents
     */
    public static class Request extends InteractionEvent.Request {
        private final UUID meetingID;

        public Request(UUID meetingID) {
            this.meetingID = meetingID;
        }

        @Override
        public String getSubURL() {
            return "/" + Server.INSTANCE.getStaticToken() + "/bbb/end-meeting";
        }

        @Override
        public JSONObject getParameters() {
            JSONObject params = new JSONObject();
            params.put("apiName", "end");
            params.put("meetingID", meetingID.toString());
            params.put("moderatorPW", AppProperties.BBB_ADMIN_PASSWORD.getString());

            JSONObject request = new JSONObject();
            request.put("password", AppProperties.BBB_ADMIN_PASSWORD.getString());
            request.put("meetingID", meetingID.toString());
            params.put("requestParameters", request);

            return params;
        }
    }

    /**
     * Definiert die Auflösung der Parameter für die Antwort des EndMeetingEvents
     */
    public static class Response extends InteractionEvent.Response {
        public final String meetingID;

        public Response(JSONObject json) {
            super(json);
            this.meetingID = json.getString("meetingID");
        }
    }
}
