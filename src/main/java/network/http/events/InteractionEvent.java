package network.http.events;

import org.json.JSONObject;

public abstract class InteractionEvent {

    /**
     * Definiert die Parameter für die Anfrage des Events
     */
    public abstract static class Request {

        public abstract String getSubURL();

        public abstract JSONObject getParameters();
    }

    /**
     * Definiert die Auflösung der Parameter für die Antwort des Events
     */
    public static class Response {

        /**
         * Der Konstruktor für die Response der Interation
         * @param json Das von der Interaktion übermittelte JSON-Objekt
         */
        public Response(JSONObject json) {

        }
    }
}
