package network.http.events;

import org.json.JSONObject;

import java.util.UUID;
/**
 * Diese Klasse dient zur Kommunikation mit der Interaktion, um Informationen über alle Dokumente zu erhalten.
 * @author Nicolas Fröhlich
 */
public class InteractionGetDocInfoDocument extends InteractionEvent {

    /**
     * Definiert die Parameter für die Anfrage der GetDocInfoDocument
     */
    public static class Request extends InteractionEvent.Request {
        private final String loginToken;

        /**
         * @param loginToken Das LokinToken, welches mit an die Interaktion übermittelt wird. Muss Adminrechte beinhalten
         */
        public Request(String loginToken) {
            this.loginToken = loginToken;
        }

        @Override
        public String getSubURL() {
            return "/" + loginToken + "/virtuhos/get-doc-info";
        }

        @Override
        public JSONObject getParameters() {
            JSONObject params = new JSONObject();
            return params;
        }
    }

    /**
     * Definiert die Auflösung der Parameter für die Antwort der GetDocInfoDocument
     */
    public static class Response extends InteractionEvent.Response {
        /**
         * Der Konstruktor für die Response der Interation
         * @param json Das von der Interaktion übermittelte JSON-Objekt
         */
        public Response(JSONObject json) {
            super(json);
        }
    }
}
