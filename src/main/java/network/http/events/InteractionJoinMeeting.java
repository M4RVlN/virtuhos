package network.http.events;

import config.AppProperties;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class InteractionJoinMeeting extends InteractionEvent {
    public static class Request extends InteractionEvent.Request {
        private final String displayName;
        private final UUID userId;
        private final UUID meetingId;
        private final String password;
        private final boolean cameraAutoOn;
        private final String loginToken;

        public Request(String displayName, UUID userId, UUID meetingID, boolean isAdmin, boolean cameraAutoOn, String loginToken) {
            this.displayName = displayName;
            this.userId = userId;
            this.meetingId = meetingID;
            this.password = isAdmin ? AppProperties.BBB_ADMIN_PASSWORD.getString() : AppProperties.BBB_PASSWORD.getString();
            this.cameraAutoOn = cameraAutoOn;
            this.loginToken = loginToken;
        }

        @Override
        public String getSubURL() {
            return "/" + loginToken + "/bbb/join-meeting";
        }

        @Override
        public JSONObject getParameters() {
            JSONObject params = new JSONObject();

            params.put("cameraAutoOn",Boolean.toString(cameraAutoOn));
            params.put("apiName", "join");
            params.put("fullName", displayName);
            params.put("meetingId", meetingId.toString());
            params.put("password", password);
            params.put("userId", userId.toString());

            JSONObject request = new JSONObject();
            request.put("redirect", "true");
            request.put("userdata-bbb_auto_share_webcam", Boolean.toString(cameraAutoOn));
            request.put("password", password);
            request.put("userdata-bbb_listen_only_mode", "false");
            request.put("userdata-bbb_skip_check_audio", "true");
            request.put("fullName", displayName);
            request.put("userdata-bbb_skip_video_preview", "true");
            request.put("meetingID", meetingId.toString());
            request.put("userdata-bbb_auto_join_audio", "true");
            request.put("userID", userId.toString());
            params.put("requestParameters", request);

            return params;
        }
    }

    public static class Response extends InteractionEvent.Response {
        /** ID of the Meeting */
        public final String meetingID;
        /** URL of the Meeting */
        public final String joinUrl;

        public Response(JSONObject json) throws JSONException {
            super(json);
            this.meetingID = json.getString("meetingID");
            this.joinUrl = json.getString("joinUrl");
        }
    }
}
