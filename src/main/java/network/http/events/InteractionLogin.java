package network.http.events;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;
import util.JSONUtil;

public class InteractionLogin extends InteractionEvent {
    public static class Request extends InteractionEvent.Request {
        private final String userName;
        private final String password;

        public Request(String userName, String password) {
            this.userName = userName;
            this.password = password;
        }

        @Override
        public String getSubURL() {
            String userName = "";
            try {
                userName = URLEncoder.encode(this.userName, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return "/login/do-login/" + userName;
        }

        @Override
        public JSONObject getParameters() {
            JSONObject params = new JSONObject();
            params.put("password", password);
            return params;
        }
    }

    public static class Response extends InteractionEvent.Response {
        // Id of the token
        public final String loginToken;
        // Creation-time of token (=login time)
        public final String created;

        public final boolean isAdmin;

        //Person --------------
        public final UUID userID;//UserId
        public final String userName;//LoginName
        public final String displayName;

        // the meeting in which the person is supposed to be (joined itself / joined by admin)
        public final InteractionCreateMeeting.Response meeting;
        // the meeting which the user is truly in, in a BBB room
        public final InteractionCreateMeeting.Response trueMeeting;

        // for timeout purposes
        public final long lastHeartbeatTime;

        public Response(JSONObject json) throws JSONException {
            super(json);
            this.loginToken = json.getString("tokId");
            this.created = json.getString("created");
            this.isAdmin = json.getBoolean("isAdmin");

            JSONObject person = json.getJSONObject("person");
            this.userID = UUID.fromString(person.getString("uuid"));
            this.userName = person.getString("userName");
            this.displayName = person.getString("displayName");
            JSONObject meeting = JSONUtil.getOrNull(person.get("meeting"), JSONObject.class);
            this.meeting = meeting != null ? new InteractionCreateMeeting.Response(meeting) : null;
            JSONObject trueMeeting = JSONUtil.getOrNull(person.get("trueMeeting"), JSONObject.class);
            this.trueMeeting = trueMeeting != null ? new InteractionCreateMeeting.Response(trueMeeting) : null;
            this.lastHeartbeatTime = person.getLong("lastHeartbeatTime");
        }
    }
}
