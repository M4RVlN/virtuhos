package network.tcp;

import java.io.IOException;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import network.tcp.communication.CommunicationTarget;
import network.tcp.communication.CommunicationThread;
import network.tcp.events.NetworkEvent;
import network.tcp.events.ServerJoinRequest;
import network.tcp.events.ServerLeaveRequest;
import network.tcp.events.listener.ClientEventListener;
import network.tcp.events.listener.EventListener;
import network.tcp.events.listener.EventTarget;

public class Client implements EventTarget {
    private Socket socket;

    private String host;
    private int port;

    private EventListener listener = new ClientEventListener();
    private CommunicationThread communicationThread;

    public static final Client INSTANCE = new Client();

    /**
     * Stellt eine Verbindung zum Server her und loggt sich ein
     * @param host Die IP/URL des Hosts
     * @param port Der Port auf dem der Server läuft
     * @param name Der Name des Benutzers
     * @param password Das Passwort des Benutzers
     * @return <code>true</code> falls das Verbinden und Anmelden erfolgreich
     */
    public synchronized boolean connect(String host, int port, String name, String password) {
        try {
            if(socket != null && !socket.isClosed())
                close();

            this.host = host;
            this.port = port;

            socket = new Socket(host, port);
            CommunicationTarget target = new CommunicationTarget(socket);

            communicationThread = new CommunicationThread(this, target);
            communicationThread.start();

            sendEvent(new ServerJoinRequest(null, name, password));
        } catch (UnknownHostException hostException) {
            System.out.println("Server not found");
            return false;
        } catch (ConnectException connectException) {
            System.out.println("Server unavailable");
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Prüft ob eine offene Verbindung besteht
     * @return true falls verbunden
     */
    public synchronized boolean isConnected() {
        return socket != null && socket.isConnected() && !socket.isClosed();
    }

    /**
     * Sendet eine Nachricht an den Server
     * @param message Die Nachricht
     */
    public void sendMessage(String message) {
        communicationThread.sendMessage(message);
    }

    /**
     * Sendet ein Event an den Server
     * @param event Das Event
     */
    public void sendEvent(NetworkEvent event) {
        sendMessage(event.toString());
    }

    /**
     * Beendet die Verbindung zum Server und sendet dies als Nachricht an den Server
     */
    public synchronized void close() {
        close(true);
    }

    /**
     * Beendet die Verbindung zum Server
     * @param sendLeaveRequest Soll eine Nachricht an den Server gesendet werden dass die Verbindung unterbrochen wird
     */
    public synchronized void close(boolean sendLeaveRequest) {
        if(communicationThread != null && !socket.isClosed()) {
            if(sendLeaveRequest)
                sendEvent(new ServerLeaveRequest(null));

            communicationThread.close();
        }
    }

    @Override
    public EventListener getEventListener() {
        return listener;
    }

    @Override
    public void setEventListener(EventListener listener) {
        this.listener = listener;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }
}
