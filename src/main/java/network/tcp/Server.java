package network.tcp;

import config.AppProperties;
import model.Hall;
import model.House;
import model.Office;
import model.Room;
import network.database.DBHandler;
import network.http.InteractionHandler;
import network.http.events.InteractionCreateMeeting;
import network.http.events.InteractionEndMeeting;
import network.http.events.InteractionLogin;
import network.tcp.communication.CommunicationThread;
import network.tcp.communication.ServerCommunicationTarget;
import network.tcp.events.NetworkEvent;
import network.tcp.events.ServerLeaveEvent;
import network.tcp.events.ServerShutdownEvent;
import network.tcp.events.listener.EventListener;
import network.tcp.events.listener.EventTarget;
import network.tcp.events.listener.HallModule;
import network.tcp.events.listener.ServerEventListener;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.*;

public class Server implements Runnable, EventTarget {

    public static final Server INSTANCE = new Server();
    private static final long timeOutDelay = 5000;

    private final HashMap<ServerCommunicationTarget, CommunicationThread> communicationThreads = new HashMap<>();

    private static final String staticLoginName = "system-editor";
    private String staticToken;

    private ServerSocket serverSocket;
    private boolean isRunning;
    private int port;

    private House house;

    private EventListener listener = new ServerEventListener();

    @Override
    public void run() {
        System.out.println("Server started");
        while (isRunning()) {
            System.out.println("Waiting for connection...");
            try {
                Socket clientSocket = serverSocket.accept();

                ServerCommunicationTarget target = new ServerCommunicationTarget(clientSocket);
                CommunicationThread thread = new CommunicationThread(this, target);
                communicationThreads.put(target, thread);
                thread.start();

                //Timeout if connection established but not logged in
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (communicationThreads.containsKey(target) && target.isSilent()) {
                            Server.INSTANCE.disconnect(target, "Keine Login-Anfrage gestellt.");
                        }
                    }
                }, timeOutDelay);

                System.out.println("Connection established");
            } catch (SocketException ignore) {
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println("Error accepting client connection");
            }
        }
    }

    public synchronized boolean isRunning() {
        return isRunning;
    }

    /**
     * Startet einen Server auf dem port mit dem Haus
     *
     * @param port  Der Port auf dem der Server läuft
     * @param house Das Haus Modell welches der Server verwenden soll
     * @throws Exception Datenbank konnte nicht erreicht werden, Interaktionsserver konnte nicht erreicht werden, TCP Socket konnte nicht
     *                   geöffnet werden
     */
    public synchronized void start(int port, House house) throws Exception {
        if (serverSocket != null && !serverSocket.isClosed()) {
            stop();
        }
        this.port = port;
        this.house = house;

        //Test if database is running
        DBHandler.getConnection().close();

        //Get Server token for interaction
        InteractionLogin.Response login = InteractionHandler.sendPostRequest(
                new InteractionLogin.Request(
                        staticLoginName,
                        AppProperties.BBB_PASSWORD.getString()),
                InteractionLogin.Response.class);

        if (login == null) {
            throw new Exception("Could not login to Interaction");
        }
        staticToken = login.loginToken;

        killOldMeetings();

        //Create BBB rooms
        for (Room room : house.getRoomList()) {
            //Get capacity
            int capacity = Integer.MAX_VALUE;
            if (room instanceof Office) {
                capacity = ((Office) room).getCapacity();
            }

            InteractionCreateMeeting.Request request = new InteractionCreateMeeting.Request(
                    room.getId(),
                    room.getName(),
                    capacity);

            InteractionCreateMeeting.Response response = InteractionHandler.sendPostRequest(
                    request,
                    InteractionCreateMeeting.Response.class);

            if (response == null) {
                throw new Exception("Could not create room for BBB");
            }
        }

        //Start server socket
        serverSocket = new ServerSocket(port);
        Thread serverThread = new Thread(this);
        serverThread.start();

        isRunning = true;
    }

    /**
     * Stoppt den Server
     */
    public synchronized void stop() {
        if (!isRunning) {
            return;
        }

        //Close BBB rooms
        for (Room room : house.getRoomList()) {
            InteractionHandler.sendPostRequest(new InteractionEndMeeting.Request(room.getId()), InteractionEndMeeting.Response.class);

            if (room instanceof Hall) {
                HallModule.closeAllBBBRooms(room.getId());
            }
        }

        isRunning = false;
        house = null;
        try {
            broadcastEvent(new ServerShutdownEvent(null, "Der Server wurde geschlossen"));
            communicationThreads.clear();

            if (serverSocket != null) {
                serverSocket.close();
            }

            System.out.println("Server stopped");
        } catch (Exception e) {
            System.err.println("Error closing server");
        }
    }

    /**
     * Sendet eine Nachricht an das Ziel
     * @param target Das Ziel der Nachricht
     * @param message Die Nachricht
     * @return <code>true</code> falls Versenden erfolgreich
     */
    public boolean sendMessage(ServerCommunicationTarget target, String message) {
        if (!communicationThreads.containsKey(target)) {
            return false;
        }

        return communicationThreads.get(target).sendMessage(message);
    }

    /**
     * Sendet ein Event an das Ziel
     * @param target Das Ziel der Nachricht
     * @param event Das Event
     * @return <code>true</code> falls Versenden erfolgreich
     */
    public boolean sendEvent(ServerCommunicationTarget target, NetworkEvent event) {
        return sendMessage(target, event.toString());
    }

    /**
     * Sendet eine Nachricht an alle aktiven Verbindungen
     * @param message Die Nachricht
     */
    public void broadcastMessage(String message) {
        for (CommunicationThread t : communicationThreads.values()) {
            t.sendMessage(message);
        }
    }

    /**
     * Sendet eine Nachricht an alle aktiven Verbindungen
     * @param event Das Event
     */
    public void broadcastEvent(NetworkEvent event) {
        broadcastMessage(event.toString());
    }

    /**
     * Sendet eine Nachricht an alle aktiven Verbindungen, außer den Ausgewählten
     * @param event Das Event
     * @param ignore Liste von aktiven Verbindungen, welche ignoriert werden sollen
     */
    public void broadcastEvent(NetworkEvent event, List<ServerCommunicationTarget> ignore) {
        ArrayList<ServerCommunicationTarget> targets = new ArrayList<>(
                communicationThreads.keySet());
        targets.removeAll(ignore);

        for (ServerCommunicationTarget t : targets) {
            sendEvent(t, event);
        }
    }

    /**
     * Trennt die Verindung zu einem Ziel
     * @param target Das Ziel
     * @param message Grund für Verbindungsabbruch
     */
    public synchronized void disconnect(ServerCommunicationTarget target, String message) {
        disconnect(target, message, true);
    }

    /**
     * Trennt die Verindung zu einem Ziel
     * @param target Das Ziel
     * @param message Grund für Verbindungsabbruch
     * @param sendShutdownEvent Soll eine Nachricht mit dem Grund an das Ziel gesendet werden
     */
    public synchronized void disconnect(ServerCommunicationTarget target, String message, boolean sendShutdownEvent) {
        if (!communicationThreads.containsKey(target)) {
            return;
        }

        if (target.socket == null || target.socket.isClosed()) {
            communicationThreads.remove(target);
            return;
        }
        if (sendShutdownEvent) {
            sendEvent(target, new ServerShutdownEvent(null, message));
        }

        if (!target.isSilent()) {
            removeFromHouse(target);
        }

        communicationThreads.get(target).close();
        communicationThreads.remove(target);

    }

    /**
     * Entfernt ein Ziel aus dem Haus
     * @param target Das Ziel
     */
    public void removeFromHouse(ServerCommunicationTarget target) {
        House house = Server.INSTANCE.getHouse();
        boolean success = house.removePerson(house.getPerson(target.id));

        if (success) {
            ArrayList<ServerCommunicationTarget> ignore = new ArrayList<>();
            ignore.add(target);
            Server.INSTANCE.broadcastEvent(new ServerLeaveEvent(null, target.id), ignore);
        }
    }

    @Override
    public EventListener getEventListener() {
        return listener;
    }

    @Override
    public void setEventListener(EventListener listener) {
        this.listener = listener;
    }

    public int getPort() {
        return port;
    }

    public InetAddress getInetAdress() {
        return serverSocket.getInetAddress();
    }

    public ServerCommunicationTarget getCommunicationTarget(UUID id) {
        for (ServerCommunicationTarget target : communicationThreads.keySet()) {
            if (target.id.equals(id)) {
                return target;
            }
        }
        return null;
    }

    public House getHouse() {
        return house;
    }

    /**
     * Beendet alle Meetings die noch laufen
     */
    private void killOldMeetings() {
        if (AppProperties.BBB_KILL_OLD_MEETINGS.getBoolean()) {
            try {
                String response = InteractionHandler.sendGetRequest("/" + staticToken + "/virtuhos/get-meeting-info");
                JSONArray meetingArray = (JSONArray) new JSONTokener(response).nextValue();

                for (int i = 0; i < meetingArray.length(); i++) {
                    JSONObject meeting = meetingArray.getJSONObject(i);
                    UUID meetingId = UUID.fromString(meeting.getString("meetingID"));

                    InteractionHandler.sendPostRequest(new InteractionEndMeeting.Request(meetingId), InteractionEndMeeting.Response.class);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getStaticToken() {
        return staticToken;
    }
}
