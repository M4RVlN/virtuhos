package network.tcp.communication;

import java.net.Socket;

public class CommunicationTarget {
    public final Socket socket;

    public CommunicationTarget(Socket socket) {
        this.socket = socket;
    }
}
