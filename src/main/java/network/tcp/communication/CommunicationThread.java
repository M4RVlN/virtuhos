package network.tcp.communication;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javafx.application.Platform;
import network.tcp.Client;
import network.tcp.Server;
import network.tcp.events.*;
import network.tcp.events.listener.EventListener;
import network.tcp.events.listener.EventTarget;
import org.json.JSONObject;
import org.json.JSONTokener;
import window.SceneManager;
import window.Scenes;

/**
 * Ein Thread für die Kommunikation mit <strong>einem</strong> Client oder Server
 * @author Marvin Öhlerking, Nicolas Fröhlich
 */
public class CommunicationThread extends Thread {
    private static final int MAX_MESSAGE_LENGTH = 60416;

    protected final CommunicationTarget communicationTarget;
    protected final EventTarget eventTarget;

    protected DataInputStream dataInputStream;
    protected DataOutputStream dataOutputStream;

    private boolean isRunning;
    private Map<Integer, String> packetPuffer = new HashMap<>();

    public CommunicationThread(EventTarget eventTarget, CommunicationTarget communicationTarget) {
        this.eventTarget = eventTarget;
        this.communicationTarget = communicationTarget;

        try {
            dataInputStream = new DataInputStream(communicationTarget.socket.getInputStream());
            dataOutputStream = new DataOutputStream(communicationTarget.socket.getOutputStream());

            isRunning = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while (isRunning) {
                String received = dataInputStream.readUTF();
                if (!received.isEmpty()) {
                    readMessage(received);
                }
            }
        } catch (Exception ignore) {
            //A client terminated
            if(Server.INSTANCE.isRunning()) {
                Server.INSTANCE.disconnect((ServerCommunicationTarget) communicationTarget, "", false);
                System.err.println("Lost connection to " + communicationTarget.socket.getInetAddress());
            }
            //The server terminated
            if(Client.INSTANCE.isConnected()) {
                Client.INSTANCE.close(false);
                Platform.runLater(() -> SceneManager.loadScene(Scenes.LOGIN));
                System.err.println("Lost connection to " + communicationTarget.socket.getInetAddress());
            }
        }

        isRunning = false;
    }

    /**
     * Decodiert empfangene Nachricht und übergibt sie an den EventListener.
     * Falls die Nachricht nur ein Teil einer in einzelne Pakete aufgeteilten Nachricht ist, wird ihr Inhalt
     * zwischengespeichert und auf das letzte Paket gewartet, bevor die vollständige Nachricht an den EventListener übergeben wird.
     * @param message Die Nachricht
     */
    protected void readMessage(String message) {
        try {
            System.out.println("Editor:" + message);
            JSONObject packet = (JSONObject) new JSONTokener(message).nextValue();

            int sequenceNr = packet.getInt("s");
            packetPuffer.put(sequenceNr, packet.getString("b"));

            if(sequenceNr == 0) {
                //Packet wieder zusammenbauen
                StringBuilder body = new StringBuilder();
                for (int i = packetPuffer.size() - 1; i >= 0; i--)
                    body.append(packetPuffer.get(i));

                packetPuffer.clear();

                JSONObject json = (JSONObject) new JSONTokener(body.toString()).nextValue();
                EventListener listener = eventTarget.getEventListener();

                switch (NetworkEvents.valueOf(json.getString("type"))) {
                    case SERVER_JOIN_REQUEST -> listener.onServerJoinRequest(ServerJoinRequest.fromJson(communicationTarget, json));
                    case SERVER_JOIN_REPLY -> listener.onServerJoinReply(ServerJoinReply.fromJson(communicationTarget, json));
                    case SERVER_JOIN_EVENT -> listener.onServerJoin(ServerJoinEvent.fromJson(communicationTarget, json));
                    case SERVER_LEAVE_REQUEST -> listener.onServerLeaveRequest(new ServerLeaveRequest(communicationTarget));
                    case SERVER_LEAVE_EVENT -> listener.onServerLeave(ServerLeaveEvent.fromJson(communicationTarget, json));
                    case SERVER_SHUTDOWN_EVENT -> listener.onServerShutdown(ServerShutdownEvent.fromJson(communicationTarget, json));
                    case PERSON_MOVE_REQUEST -> listener.onPersonMoveRequest(PersonMoveRequest.fromJson(communicationTarget, json));
                    case PERSON_MOVE_EVENT -> listener.onPersonMove(PersonMoveEvent.fromJson(communicationTarget, json));
                    case ANALYSIS_DATA_REQUEST -> listener.onAnalysisDataRequest(AnalysisDataRequest.fromJson(communicationTarget, json));
                    case ANALYSIS_DATA_REPLY -> listener.onAnalysisDataReply(AnalysisDataReply.fromJson(communicationTarget, json));
                    case HALL_REGROUP_EVENT -> listener.onHallRegroup(HallRegroupEvent.fromJson(communicationTarget, json));
                    case HALL_REGROUP_REQUEST -> listener.onHallRegroupRequest(HallRegroupRequest.fromJson(communicationTarget, json));
                    case OPEN_BBB_COMMAND -> listener.onOpenBBBCommand(OpenBBBCommand.fromJson(communicationTarget, json));
                    case PERSON_CREATE_REQUEST -> listener.onPersonCreateRequest(PersonCreateRequest.fromJson(communicationTarget, json));
                    case PERSON_DELETE_REQUEST -> listener.onPersonDeleteRequest(PersonDeleteRequest.fromJson(communicationTarget, json));
                    case DOCUMENT_CREATE_REQUEST -> listener.onDocumentCreateRequest(DocumentCreateRequest.fromJson(communicationTarget, json));
                    case DOCUMENT_CREATE_REPLY -> listener.onDocumentCreateReply(DocumentCreateReply.fromJson(communicationTarget, json));
                    case DOCUMENT_DELETE_REQUEST -> listener.onDocumentDeleteRequest(DocumentDeleteRequest.fromJson(communicationTarget, json));
                    case DOCUMENT_GET_REQUEST -> listener.onDocumentGetRequest(DocumentGetRequest.fromJson(communicationTarget, json));
                    case DOCUMENT_GET_INFO_REQUEST -> listener.onDocumentGetInfoRequest(DocumentGetInfoRequest.fromJson(communicationTarget, json));
                    case DOCUMENT_GET_REPLY -> listener.onDocumentGetReply(DocumentGetReply.fromJson(communicationTarget, json));
                    case DOCUMENT_GET_INFO_REPLY -> listener.onDocumentGetInfoReply(DocumentGetInfoReply.fromJson(communicationTarget, json));
                    case PERSON_CREATE_REPLY -> listener.onPersonCreateReply(PersonCreateReply.fromJson(communicationTarget, json));
                }
            }
        } catch (Exception e) {
            System.out.println("Invalid message from " + communicationTarget.socket.getInetAddress().toString());
            e.printStackTrace();
        }
    }

    /**
     * Versendet eine Nachricht an das verbundene Ziel.
     * Falls die Nachricht zugroß für ein einzelnes TCP Packet ist, wird sie aufgeteilt und nummeriert in einzelne Teilpakete verschickt.
     * @param message Die Nachricht
     * @return true falls Erfolgreich
     */
    public boolean sendMessage(String message) {
        if (!isRunning)
            return false;

        int messageCount = (message.length() + MAX_MESSAGE_LENGTH - 1) / MAX_MESSAGE_LENGTH;
        for (int start = 0; start < message.length(); start += MAX_MESSAGE_LENGTH) {
            JSONObject JSONPacket = new JSONObject();

            JSONPacket.put("s", messageCount - start / MAX_MESSAGE_LENGTH - 1); //Sequenz Nummer des Packets. 0 = letztes Packet
            JSONPacket.put("b", message.substring(start, Math.min(message.length(), start + MAX_MESSAGE_LENGTH))); //Body des Packets
            String packet = JSONPacket.toString();

            try {
                dataOutputStream.writeUTF(packet);
                dataOutputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    /**
     * Schließt die Datenverbindung zum Ziel
     */
    public void close() {
        isRunning = false;
        try {
            dataInputStream.close();
            dataOutputStream.close();
            communicationTarget.socket.close();
            System.out.println("Communication to " + communicationTarget.socket.getInetAddress() + " closed");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
