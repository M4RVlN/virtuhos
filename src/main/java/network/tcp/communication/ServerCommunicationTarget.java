package network.tcp.communication;

import java.net.Socket;
import java.util.UUID;

public class ServerCommunicationTarget extends CommunicationTarget {
    public UUID id;
    public String userName;
    public String displayName;
    public String loginToken;
    public boolean isAdmin;

    public ServerCommunicationTarget(Socket socket, boolean isAdmin, UUID id, String userName, String displayName, String loginToken) {
        super(socket);
        setUserData(id, userName, displayName, isAdmin, loginToken);
    }

    public ServerCommunicationTarget(Socket socket) {
        this(socket, false, null, "", "", "");
    }

    public boolean isSilent() {
        return id == null || userName.isEmpty();
    }

    /**
     * Setzt die Benutzerdaten
     */
    public void setUserData(UUID id, String userName, String displayName, boolean isAdmin, String loginToken) {
        this.id = id;
        this.userName = userName;
        this.displayName = displayName;
        this.isAdmin = isAdmin;
        this.loginToken = loginToken;
    }
}
