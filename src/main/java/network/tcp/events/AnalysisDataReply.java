package network.tcp.events;

import network.database.AnalysisData;
import network.tcp.communication.CommunicationTarget;
import org.json.JSONObject;
import util.xml.XMLUtil;

public class AnalysisDataReply extends NetworkEvent {
    public final String data;

    public AnalysisDataReply(CommunicationTarget sender, String data) {
        super(NetworkEvents.ANALYSIS_DATA_REPLY, sender);
        this.data = data;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("data", data);
        } catch (Exception ignore) {
        }
        return root;
    }

    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues AnalysisDataReply Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static AnalysisDataReply fromJson(CommunicationTarget target, JSONObject json) throws Exception {
        String data = json.getString("data");

        return new AnalysisDataReply(target, data);
    }
}
