package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

public class AnalysisDataRequest extends NetworkEvent {
    public AnalysisDataRequest(CommunicationTarget sender) {
        super(NetworkEvents.ANALYSIS_DATA_REQUEST, sender);
    }

    @Override
    protected JSONObject buildData() {
        return super.buildData();
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues AnalysisRequest Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static AnalysisDataRequest fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        return new AnalysisDataRequest(target);
    }
}
