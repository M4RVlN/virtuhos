package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

public class DocumentCreateReply extends NetworkEvent  {
    public final boolean success;

    public DocumentCreateReply(CommunicationTarget sender, boolean success) {
        super(NetworkEvents.DOCUMENT_CREATE_REPLY, sender);
        this.success = success;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("success", success);
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues DocumentCreateReply Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static DocumentCreateReply fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        boolean success = json.getBoolean("success");

        return new DocumentCreateReply(target, success);
    }
}