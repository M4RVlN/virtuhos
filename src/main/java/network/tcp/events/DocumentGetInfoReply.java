package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DocumentGetInfoReply extends NetworkEvent  {
    public final String[] docNames;
    public final int[] docIds;

    public DocumentGetInfoReply(CommunicationTarget sender, String[] docNames, int[] docIds) {
        super(NetworkEvents.DOCUMENT_GET_INFO_REPLY, sender);
        this.docNames = docNames;
        this.docIds = docIds;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        JSONArray JSONdocNames = new JSONArray();
        JSONArray JSONdocIds = new JSONArray();
        try {
            for(String s : this.docNames)
                JSONdocNames.put(s);
            root.put("docNames", JSONdocNames);
            for(Integer x : this.docIds)
                JSONdocIds.put(x);
            root.put("docIds", JSONdocIds);
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON. JSON kann aus mehreren Paketen bestehen, die zusammengefügt werden
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues DocumentGetInfoReply Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static DocumentGetInfoReply fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        String[] docNames = new String[json.getJSONArray("docNames").length()];
        for(int i = 0; i < json.getJSONArray("docNames").length(); i++) {
            docNames[i] = json.getJSONArray("docNames").getString(i);
        }
        int[] docIds = new int[json.getJSONArray("docIds").length()];
        for(int i = 0; i < json.getJSONArray("docIds").length(); i++) {
            docIds[i] = json.getJSONArray("docIds").getInt(i);
        }

        return new DocumentGetInfoReply(target, docNames, docIds);
    }
}