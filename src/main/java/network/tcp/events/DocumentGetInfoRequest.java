package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class DocumentGetInfoRequest extends NetworkEvent  {
    public final UUID meetingId;
    public DocumentGetInfoRequest(CommunicationTarget sender, UUID meetingId1) {
        super(NetworkEvents.DOCUMENT_GET_INFO_REQUEST, sender);
        this.meetingId = meetingId1;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("meetingId", meetingId.toString());
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues DocumentGetInfoRequest Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static DocumentGetInfoRequest fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        UUID meetingId = UUID.fromString(json.getString("meetingId"));
        return new DocumentGetInfoRequest(target, meetingId);
    }
}