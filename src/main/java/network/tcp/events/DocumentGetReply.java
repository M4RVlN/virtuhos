package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class DocumentGetReply extends NetworkEvent  {
    public final String data;
    public final String name;
    public final UUID uploader;

    public DocumentGetReply(CommunicationTarget sender, String data, String name, UUID uploader) {
        super(NetworkEvents.DOCUMENT_GET_REPLY, sender);
        this.data = data;
        this.name = name;
        this.uploader = uploader;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
            try {
                root.put("data", data);
                root.put("name", name);
                root.put("uploader", uploader.toString());
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues DocumentGetReply Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static DocumentGetReply fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        String data = json.getString("data");
        String name = json.getString("name");
        UUID uploader = UUID.fromString(json.getString("uploader"));

        return new DocumentGetReply(target, data, name, uploader);
    }
}