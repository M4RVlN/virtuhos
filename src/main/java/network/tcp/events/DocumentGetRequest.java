package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

public class DocumentGetRequest extends NetworkEvent  {
    public final int docId;


    public DocumentGetRequest(CommunicationTarget sender, int docId) {
        super(NetworkEvents.DOCUMENT_GET_REQUEST, sender);
        this.docId = docId;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("docId", docId);
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues DocumentGetRequest Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static DocumentGetRequest fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        int docId = json.getInt("docId");

        return new DocumentGetRequest(target, docId);
    }
}