package network.tcp.events;

import model.Hall;
import network.tcp.communication.CommunicationTarget;
import org.json.JSONObject;
import util.xml.XMLUtil;

public class HallRegroupEvent extends NetworkEvent {
    public final Hall hall;

    public HallRegroupEvent(CommunicationTarget sender, Hall hall) {
        super(NetworkEvents.HALL_REGROUP_EVENT, sender);
        this.hall = hall;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("data", XMLUtil.parseToXML(hall));
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues HallRegroup Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static HallRegroupEvent fromJson(CommunicationTarget target, JSONObject json) throws Exception {
        Hall hall = XMLUtil.parseFromXML(json.getString("data"), Hall.class);
        
        return new HallRegroupEvent(target, hall);
    }
}
