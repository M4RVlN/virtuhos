package network.tcp.events;

import java.util.UUID;
import model.HallSortingMode;
import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

public class HallRegroupRequest extends NetworkEvent {
    public final HallSortingMode mode;
    public final UUID hallId;

    public HallRegroupRequest(CommunicationTarget sender, UUID hallId, HallSortingMode mode) {
        super(NetworkEvents.HALL_REGROUP_REQUEST, sender);
        this.mode = mode;
        this.hallId = hallId;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("mode", mode.toString());
            root.put("id", hallId.toString());
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues HallRegroupRequest Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static HallRegroupRequest fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        HallSortingMode mode = HallSortingMode.valueOf(json.getString("mode"));
        UUID hallId = UUID.fromString(json.getString("id"));

        return new HallRegroupRequest(target, hallId, mode);
    }
}
