package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONObject;

public abstract class NetworkEvent {
    public final NetworkEvents eventType;
    public final CommunicationTarget sender;

    public NetworkEvent(NetworkEvents eventType, CommunicationTarget sender) {
        this.eventType = eventType;
        this.sender = sender;
    }

    protected JSONObject buildData() {
        JSONObject root = new JSONObject();
        root.put("type", eventType.toString());
        return root;
    }

    @Override
    public String toString() {
        return buildData().toString();
    }
}
