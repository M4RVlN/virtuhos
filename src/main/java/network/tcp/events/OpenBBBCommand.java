package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

public class OpenBBBCommand extends NetworkEvent {
    public final String url;

    public OpenBBBCommand(CommunicationTarget sender, String url) {
        super(NetworkEvents.OPEN_BBB_COMMAND, sender);
        this.url = url;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("url", url);
        } catch (Exception ignore) { }
        return root;
    }

    public static OpenBBBCommand fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        String url = json.getString("url");

        return new OpenBBBCommand(target, url);
    }
}
