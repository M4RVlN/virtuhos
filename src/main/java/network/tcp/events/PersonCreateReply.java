package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONObject;

public class PersonCreateReply extends NetworkEvent {
    public final boolean success;

    public PersonCreateReply(CommunicationTarget sender, boolean success) {
        super(NetworkEvents.PERSON_CREATE_REPLY, sender);
        this.success = success;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("success", success);
        } catch (Exception ignore) {
        }
        return root;
    }

    public static PersonCreateReply fromJson(CommunicationTarget target, JSONObject json) throws Exception {
        boolean success = json.getBoolean("success");

        return new PersonCreateReply(target, success);
    }
}
