package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

public class PersonCreateRequest extends NetworkEvent {
    public final String userName;
    public final String displayName;
    public final boolean isAdmin;

    public PersonCreateRequest(CommunicationTarget sender, String userName, String displayName, boolean isAdmin) {
        super(NetworkEvents.PERSON_CREATE_REQUEST, sender);
        this.userName = userName;
        this.displayName = displayName;
        this.isAdmin = isAdmin;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("userName", userName);
            root.put("displayName", displayName);
            root.put("isAdmin", isAdmin);
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues PersonCreateRequest Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static PersonCreateRequest fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        String userName = json.getString("userName");
        String displayName = json.getString("displayName");
        boolean isAdmin = json.getBoolean("isAdmin");

        return new PersonCreateRequest(target, userName, displayName, isAdmin);
    }
}
