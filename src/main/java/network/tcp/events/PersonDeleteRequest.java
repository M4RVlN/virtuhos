package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

public class PersonDeleteRequest extends NetworkEvent{
    public final String userName;

    public PersonDeleteRequest(CommunicationTarget sender, String userName) {
        super(NetworkEvents.PERSON_DELETE_REQUEST, sender);
        this.userName = userName;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("userName", userName);
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues PersonDeleteRequest Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static PersonDeleteRequest fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        String userName = json.getString("userName");

        return new PersonDeleteRequest(target, userName);
    }
}
