package network.tcp.events;

import java.util.UUID;
import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;
import util.Vector2;

public class PersonMoveEvent extends NetworkEvent {
    public final UUID id;
    public final UUID senderId;
    public final Vector2 position;
    public final boolean success;
    public final String message;

    public PersonMoveEvent(CommunicationTarget sender, UUID id, UUID senderId, Vector2 position, boolean success, String message) {
        super(NetworkEvents.PERSON_MOVE_EVENT, sender);
        this.id = id;
        this.senderId = senderId;
        this.position = position;
        this.success = success;
        this.message = message;
    }

    public PersonMoveEvent(CommunicationTarget sender, UUID id, UUID senderId, Vector2 position) {
        this(sender, id, senderId, position, true, "");
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("success", success);
            root.put("id", id.toString());
            root.put("senderId", senderId.toString());
            root.put("posX", position.x);
            root.put("posY", position.y);
            if(!success)
                root.put("message", message);
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues PersonMoveEvent Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static PersonMoveEvent fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        boolean success = json.getBoolean("success");
        UUID id = UUID.fromString(json.getString("id"));
        UUID senderId = UUID.fromString(json.getString("senderId"));
        double posX = json.getDouble("posX");
        double posY = json.getDouble("posY");
        String message = "";
        if(!success)
            message = json.getString("message");

        return new PersonMoveEvent(target, id, senderId, new Vector2(posX, posY), success, message);
    }
}
