package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;
import util.Vector2;

import java.util.UUID;

public class PersonMoveRequest extends NetworkEvent {
    public final UUID id;
    public final Vector2 position;

    public PersonMoveRequest(CommunicationTarget sender, UUID id, double posX, double posY) {
        super(NetworkEvents.PERSON_MOVE_REQUEST, sender);
        this.id = id;
        this.position = new Vector2(posX, posY);
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("id", id.toString());
            root.put("posX", position.x);
            root.put("posY", position.y);
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues PersonMoveRequest Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static PersonMoveRequest fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        UUID id = UUID.fromString(json.getString("id"));
        double posX = json.getDouble("posX");
        double posY = json.getDouble("posY");

        return new PersonMoveRequest(target, id, posX, posY);
    }
}
