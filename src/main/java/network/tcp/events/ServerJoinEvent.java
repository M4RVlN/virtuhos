package network.tcp.events;

import model.Person;
import network.tcp.communication.CommunicationTarget;
import org.json.JSONObject;
import util.xml.XMLUtil;

public class ServerJoinEvent extends NetworkEvent {
    public Person person;

    public ServerJoinEvent(CommunicationTarget sender, Person person) {
        super(NetworkEvents.SERVER_JOIN_EVENT, sender);
        this.person = person;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("data", XMLUtil.parseToXML(person));
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues ServerJoin Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static ServerJoinEvent fromJson(CommunicationTarget target, JSONObject json) throws Exception {
        Person person = XMLUtil.parseFromXML(json.getString("data"), Person.class);

        return new ServerJoinEvent(target, person);
    }
}
