package network.tcp.events;

import java.util.UUID;
import model.House;
import network.tcp.communication.CommunicationTarget;
import org.json.JSONObject;
import util.xml.XMLUtil;

public class ServerJoinReply extends NetworkEvent {
    public final UUID id;
    public final House house;
    public final boolean admin;

    public ServerJoinReply(CommunicationTarget sender, UUID id, House house, boolean admin) {
        super(NetworkEvents.SERVER_JOIN_REPLY, sender);
        this.id = id;
        this.house = house;
        this.admin = admin;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("id", id.toString());
            root.put("admin", admin);
            root.put("data", XMLUtil.parseToXML(house));
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues ServerJoinReply Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static ServerJoinReply fromJson(CommunicationTarget target, JSONObject json) throws Exception {
        UUID id = UUID.fromString(json.getString("id"));
        boolean admin = json.getBoolean("admin");
        House house = XMLUtil.parseFromXML(json.getString("data"), House.class);

        return new ServerJoinReply(target, id, house, admin);
    }
}
