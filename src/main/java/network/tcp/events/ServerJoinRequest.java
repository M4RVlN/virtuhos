package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerJoinRequest extends NetworkEvent {
    public final String password;
    public final String name;

    public ServerJoinRequest(CommunicationTarget sender, String name, String password) {
        super(NetworkEvents.SERVER_JOIN_REQUEST, sender);
        this.password = password;
        this.name = name;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("name", name);
            root.put("password", password);
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues ServerJoinRequest Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static ServerJoinRequest fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        String name = json.getString("name");
        String password = json.getString("password");

        return new ServerJoinRequest(target, name, password);
    }
}
