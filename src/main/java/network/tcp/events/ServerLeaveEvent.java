package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

public class ServerLeaveEvent extends NetworkEvent {
    public final UUID id;

    public ServerLeaveEvent(CommunicationTarget sender, UUID id) {
        super(NetworkEvents.SERVER_LEAVE_EVENT, sender);
        this.id = id;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("id", id.toString());
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues ServerLeave Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static ServerLeaveEvent fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        UUID id = UUID.fromString(json.getString("id"));

        return new ServerLeaveEvent(target, id);
    }
}
