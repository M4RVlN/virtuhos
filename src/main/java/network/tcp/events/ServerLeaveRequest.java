package network.tcp.events;

import network.tcp.communication.CommunicationTarget;

public class ServerLeaveRequest extends NetworkEvent {
    public ServerLeaveRequest(CommunicationTarget sender) {
        super(NetworkEvents.SERVER_LEAVE_REQUEST, sender);
    }
}
