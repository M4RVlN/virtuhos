package network.tcp.events;

import network.tcp.communication.CommunicationTarget;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerShutdownEvent extends NetworkEvent {
    public final String message;

    public ServerShutdownEvent(CommunicationTarget sender, String message) {
        super(NetworkEvents.SERVER_SHUTDOWN_EVENT, sender);
        this.message = message;
    }

    @Override
    protected JSONObject buildData() {
        JSONObject root = super.buildData();
        try {
            root.put("message", message);
        } catch (Exception ignore) { }
        return root;
    }
    /**
     * Erstellt Event aus empfangener JSON
     * @param target Empfänger
     * @param json Erhaltene JSON
     * @return Neues ServerShutdown Event
     * @throws Exception Fehler beim parsen der JSON
     */
    public static ServerShutdownEvent fromJson(CommunicationTarget target, JSONObject json) throws JSONException {
        String message = json.getString("message");

        return new ServerShutdownEvent(target, message);
    }
}
