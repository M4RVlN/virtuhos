package network.tcp.events.listener;

import controller.AdminController;
import controller.ClientRenderController;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import model.House;
import model.Person;
import network.database.AnalysisData;
import network.tcp.Client;
import network.tcp.events.*;
import util.xml.XMLUtil;
import window.SceneManager;
import window.Scenes;

/**
 * EventListener des Clients. Verarbeitet eingehende Server-Events.
 * @author Marvin Öhlerking, Nicolas Fröhlich
 */
public class ClientEventListener extends EventListener {
    @Override
    public void onServerJoinReply(ServerJoinReply event) {
        Platform.runLater(() -> {
            if(event.admin) {
                SceneManager.loadScene(Scenes.ADMIN);
            } else {
                SceneManager.loadScene(Scenes.USER);
            }
            ClientRenderController controller = SceneManager.getController(ClientRenderController.class);
            controller.init(event.house, event.id);
        });
    }

    @Override
    public void onServerJoin(ServerJoinEvent event) {
        Platform.runLater(() -> {
            SceneManager.getController(ClientRenderController.class).onPersonJoin(event.person);
        });
    }

    @Override
    public void onServerLeave(ServerLeaveEvent event) {
        Platform.runLater(() -> {
            ClientRenderController controller = SceneManager.getController(ClientRenderController.class);
            House house = controller.getHouse();

            controller.onPersonLeave(house.getPerson(event.id));
        });
    }

    @Override
    public void onServerShutdown(ServerShutdownEvent event) {
        Client.INSTANCE.close();

        Platform.runLater(() -> {
            SceneManager.loadScene(Scenes.LOGIN);

            if(!event.message.isEmpty()) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.initStyle(StageStyle.UTILITY);
                alert.setHeaderText("");
                alert.setContentText(event.message);
                alert.showAndWait();
            }
        });
    }

    @Override
    public void onPersonMove(PersonMoveEvent event) {
        Platform.runLater(() -> {
            if(!event.success) {
                Alert alert = new Alert(AlertType.WARNING);
                alert.initStyle(StageStyle.UTILITY);
                alert.setHeaderText(event.message);
                alert.show();
            }

            ClientRenderController controller = SceneManager.getController(ClientRenderController.class);
            House house = controller.getHouse();

            boolean animation = !controller.getMyId().equals(event.senderId);
            controller.onPersonMove(house.getPerson(event.id), event.position, animation);
        });
    }

    @Override
    public void onAnalysisDataReply(AnalysisDataReply event) {
        Platform.runLater(() -> {
            try {
                AdminController controller = SceneManager.getController(AdminController.class);
                controller.renderAnalysis(XMLUtil.parseFromXML(event.data, AnalysisData.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onHallRegroup(HallRegroupEvent event) {
        Platform.runLater(() -> {
            ClientRenderController controller = SceneManager.getController(ClientRenderController.class);
            House house = controller.getHouse();

            for(Person person : event.hall.getUsers()) {
                controller.onPersonMove(house.getPerson(person.getId()), person.getPosition(), true);
            }
        });
    }

    @Override
    public void onOpenBBBCommand(OpenBBBCommand event) {
        //Interaction blocks join event
        if(event.url.isEmpty())
            return;

        //Try open the browser
        try {
            Desktop.getDesktop().browse(new URI(event.url));
        } catch (Exception ignore) {
            Platform.runLater(() -> {
                Alert alert = new Alert(AlertType.WARNING);
                alert.initStyle(StageStyle.UTILITY);
                alert.setHeaderText("Internetbrowser konnte nicht geöffnet werden");
                alert.setContentText("Link zum Meeting: " + event.url);
                alert.show();
            });
        }
    }

    public void onDocumentCreateReply(DocumentCreateReply event){
        String header, content;
        AlertType alertType;
        if(event.success){
            header = "Dokument erstellt";
            content = "Das Dokument wurde erfolgreich hinzugefügt";
            alertType = AlertType.INFORMATION;
        }
        else{
            header = "Fehler beim Dokument erstellen";
            content = "Beim erstellen des Dokuments ist ein Fehler aufgetreten. Das Dokument konnte nicht erstellt werden!";
            alertType = AlertType.ERROR;
        }
        Platform.runLater(() -> {
            Alert alert = new Alert(alertType);
            alert.initStyle(StageStyle.UTILITY);
            alert.setHeaderText(header);
            alert.setContentText(content);
            alert.show();
        });
    }

    public void onDocumentGetReply(DocumentGetReply event) {
        Platform.runLater(() -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Dokument speichern");
            fileChooser.setInitialFileName(event.name);
            try {
                File filesrc = fileChooser.showSaveDialog(SceneManager.getStage());
                if (filesrc == null) return;
                Files.write(Path.of(filesrc.getAbsolutePath()), Base64.getDecoder().decode(event.data));
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void onDocumentGetInfoReply(DocumentGetInfoReply event) {
        Platform.runLater(() -> {
            ClientRenderController controller = SceneManager.getController(ClientRenderController.class);
            for (int i = 0; i < event.docNames.length; i++) {
                String s = event.docNames[i];

                Menu doc = new Menu(s);
                doc.setUserData(event.docIds[i]);
                MenuItem open = new MenuItem("Speichern");
                open.setOnAction(event2 ->{
                    Client.INSTANCE.sendEvent(new DocumentGetRequest(null,(int)doc.getUserData()));
                });
                MenuItem delete = new MenuItem("Löschen");
                delete.setOnAction(event2 ->{
                    Alert alert = new Alert(AlertType.CONFIRMATION, "M\u00f6chten Sie das Dokument \"" + s + "\" wirklich l\u00f6schen?", ButtonType.YES, ButtonType.CANCEL);
                    alert.initStyle(StageStyle.UTILITY);
                    alert.setTitle("Dokument l\u00f6schen");
                    alert.setHeaderText(null);
                    alert.setGraphic(null);
                    alert.setResizable(false);

                    Button yesButton = (Button) alert.getDialogPane().lookupButton(ButtonType.YES);
                    Button cancelButton = (Button) alert.getDialogPane().lookupButton(ButtonType.CANCEL);

                    yesButton.setFont(Font.font ("Arial Bold", 12));
                    cancelButton.setFont(Font.font ("Arial Bold", 12));
                    yesButton.setStyle("-fx-background-color: #0080F0; -fx-color: #000000;");
                    cancelButton.setStyle("-fx-background-color: #0080F0; -fx-color: #000000;");
                    cancelButton.setDefaultButton(true);

                    alert.showAndWait();

                    if (alert.getResult() == ButtonType.YES) {
                        Client.INSTANCE.sendEvent(new DocumentDeleteRequest(null,(int)doc.getUserData()));
                    }
                });
                doc.getItems().addAll(open, delete);
                controller.getDocContextMenu().getItems().add(doc);
            }
            controller.getDocContextMenu().show(controller.getDocTable(), controller.getContextScreenX(), controller.getContextScreenY());
        });
    }

    public void onPersonCreateReply(PersonCreateReply event){
        String header, content;
        AlertType alertType;
        if(event.success){
            header = "User erstellt";
            content = "Der User wurde erfolgreich hinzugefügt";
            alertType = AlertType.INFORMATION;
        }
        else{
            header = "Fehler beim User erstellen";
            content = "Der User konnte nicht erstellt werden!\nDer Benutzername existiert bereits!";
            alertType = AlertType.ERROR;
        }
        Platform.runLater(() -> {
            Alert alert = new Alert(alertType);
            alert.initStyle(StageStyle.UTILITY);
            alert.setHeaderText(header);
            alert.setContentText(content);
            alert.show();
        });
    }
}
