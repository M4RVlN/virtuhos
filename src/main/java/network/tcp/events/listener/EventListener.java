package network.tcp.events.listener;

import network.tcp.events.*;

/**
 * Elternklasse um Server-und Client-Events zu bearbeiten.
 * @author Marvin Öhlerking, Nicolas Fröhlich
 */
public abstract class EventListener {

    /**
     * Behandelt das ServerJoinReply-Event.
     * @param event Das erhaltene Event.
     */
    public void onServerJoinReply(ServerJoinReply event) {

    }

    /**
     * Behandelt das ServerJoinRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onServerJoinRequest(ServerJoinRequest event) {

    }

    /**
     * Behandelt das ServerJoinEvent-Event.
     * @param event Das erhaltene Event.
     */
    public void onServerJoin(ServerJoinEvent event) {

    }

    /**
     * Behandelt das ServerLeaveRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onServerLeaveRequest(ServerLeaveRequest event) {

    }

    /**
     * Behandelt das ServerLeaveEvent-Event.
     * @param event Das erhaltene Event.
     */
    public void onServerLeave(ServerLeaveEvent event) {

    }

    /**
     * Behandelt das ServerShutdownEvent-Event.
     * @param event Das erhaltene Event.
     */
    public void onServerShutdown(ServerShutdownEvent event) {

    }

    /**
     * Behandelt das PersonMoveRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onPersonMoveRequest(PersonMoveRequest event) {

    }

    /**
     * Behandelt das PersonMoveEvent-Event.
     * @param event Das erhaltene Event.
     */
    public void onPersonMove(PersonMoveEvent event) {

    }

    /**
     * Behandelt das AnalysisDataRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onAnalysisDataRequest(AnalysisDataRequest event) {

    }

    /**
     * Behandelt das AnalysisDataReply-Event.
     * @param event Das erhaltene Event.
     */
    public void onAnalysisDataReply(AnalysisDataReply event) {

    }

    /**
     * Behandelt das HallRegroupEvent-Event.
     * @param event Das erhaltene Event.
     */
    public void onHallRegroup(HallRegroupEvent event) {

    }

    /**
     * Behandelt das HallRegroupRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onHallRegroupRequest(HallRegroupRequest event) {

    }

    /**
     * Behandelt das OpenBBBCommand-Event.
     * @param event Das erhaltene Event.
     */
    public void onOpenBBBCommand(OpenBBBCommand event) {

    }

    /**
     * Behandelt das PersonCreateRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onPersonCreateRequest(PersonCreateRequest event){

    }

    /**
     * Behandelt das PersonDeleteRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onPersonDeleteRequest(PersonDeleteRequest event){

    }

    /**
     * Behandelt das DocumentCreateRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onDocumentCreateRequest(DocumentCreateRequest event){

    }

    /**
     * Behandelt das DocumentDeleteRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onDocumentDeleteRequest(DocumentDeleteRequest event){

    }

    /**
     * Behandelt das DocumentGetRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onDocumentGetRequest(DocumentGetRequest event){

    }

    /**
     * Behandelt das DocumentGetInfoRequest-Event.
     * @param event Das erhaltene Event.
     */
    public void onDocumentGetInfoRequest(DocumentGetInfoRequest event){

    }

    /**
     * Behandelt das DocumentGetReply-Event.
     * @param event Das erhaltene Event.
     */
    public void onDocumentGetReply(DocumentGetReply event){

    }

    /**
     * Behandelt das DocumentGetInfoReply-Event.
     * @param event Das erhaltene Event.
     */
    public void onDocumentGetInfoReply(DocumentGetInfoReply event){

    }

    /**
     * Behandelt das DocumentCreateReply-Event.
     * @param event Das erhaltene Event.
     */
    public void onDocumentCreateReply(DocumentCreateReply event){

    }

    /**
     * Behandelt das DocumentCreateReply-Event.
     * @param event Das erhaltene Event.
     */
    public void onPersonCreateReply(PersonCreateReply event){

    }
}
