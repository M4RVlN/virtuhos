package network.tcp.events.listener;

public interface EventTarget {
    EventListener getEventListener();
    void setEventListener(EventListener listener);
}
