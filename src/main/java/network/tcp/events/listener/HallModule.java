package network.tcp.events.listener;

import main.hall.RegroupUsers;
import model.Hall;
import model.HallSortingMode;
import model.House;
import model.Person;
import network.http.InteractionHandler;
import network.http.events.InteractionCreateMeeting;
import network.http.events.InteractionEndMeeting;
import network.http.events.InteractionJoinMeeting;
import network.tcp.Server;
import network.tcp.communication.ServerCommunicationTarget;
import network.tcp.events.OpenBBBCommand;
import util.Vector2;

import java.security.KeyException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class HallModule {
    private static final HashMap<UUID, UUID[]> breakoutList = new HashMap<>();

    /**
     * Gruppiert die Personen in der Halle neu
     * @param house Das Haus Modell
     * @param hallId Die ID der Halle im Haus
     * @param mode Der Modus der Gruppierung
     * @return true falls Gruppierung erfolgreich
     */
    @SuppressWarnings("unchecked")
    public static boolean regroupHall(House house, UUID hallId, HallSortingMode mode) {
        Hall hall = house.getHall(hallId);
        HashMap<String, Object> regrouped = regroupedPositions(hall, mode.toString().toLowerCase());
        if (regrouped == null)
            return false;

        HashMap<UUID, int[]> coords = (HashMap<UUID, int[]>) regrouped.get("coords");
        if (coords == null)
            return false;

        for (Map.Entry<UUID, int[]> pair : coords.entrySet()) {
            Person person = hall.getUser(pair.getKey());
            Vector2 newPos = new Vector2(pair.getValue()[0], pair.getValue()[1]);
            //Offset so person is not in the wall
            newPos = newPos.add(hall.getPosition()).add(new Vector2(Person.PERSON_SIZE.x / 2, Person.PERSON_SIZE.y / 2));
            person.setPosition(newPos);
        }

        //Close open rooms
        closeAllBBBRooms(hallId);
        InteractionHandler.sendPostRequest(new InteractionEndMeeting.Request(hallId), InteractionEndMeeting.Response.class);

        //Open new parent meeting
        InteractionCreateMeeting.Request parentReq = new InteractionCreateMeeting.Request(hallId, hall.getName(), Integer.MAX_VALUE);
        InteractionHandler.sendPostRequest(parentReq, InteractionCreateMeeting.Response.class);

        //Open new breakout rooms for the groups
        UUID[][] groups = (UUID[][]) regrouped.get("groups");
        UUID[] breakoutIds = new UUID[groups.length];

        for (int groupNr = 0; groupNr < groups.length; groupNr++) {
            int capacity = groups[groupNr].length;
            breakoutIds[groupNr] = UUID.randomUUID();

            //Create Rooms
            InteractionCreateMeeting.Request request = new InteractionCreateMeeting.Request(
                    breakoutIds[groupNr],
                    hall.getName() + " " + groupNr,
                    capacity);//,
                    //hallId,
                    //groupNr); doch keine breakout räume, da fehler bei zu schnellem öffnen

            InteractionHandler.sendPostRequest(request, InteractionCreateMeeting.Response.class);

            //Add Users
            for(int personNr = 0; personNr < groups[groupNr].length; personNr++) {
                ServerCommunicationTarget target = Server.INSTANCE.getCommunicationTarget(groups[groupNr][personNr]);

                //Can not find user
                if(target == null)
                    continue;

                InteractionJoinMeeting.Request joinRequest = new InteractionJoinMeeting.Request(
                        target.displayName,
                        target.id,
                        breakoutIds[groupNr],
                        target.isAdmin,
                        true,
                        target.loginToken);

                InteractionJoinMeeting.Response joinResponse = InteractionHandler.sendPostRequest(
                        joinRequest,
                        InteractionJoinMeeting.Response.class);

                //Join error
                if(joinResponse == null)
                    continue;

                Server.INSTANCE.sendEvent(target, new OpenBBBCommand(null, joinResponse.joinUrl));
            }
        }
        //track created breakout rooms
        breakoutList.put(hallId, breakoutIds);

        return true;
    }

    /**
     * Berechnet die Positionen der Perosnen
     * @param hall Das Modell der Halle
     * @param mode Der Modus der Gruppierung
     * @return Eine Liste von IDs mit Positionen
     */
    public static HashMap<String, Object> regroupedPositions(Hall hall, String mode) {
        ArrayList<Person> users = hall.getUsers();
        UUID[] uuids = new UUID[users.size()];

        for(int i = 0; i < users.size(); i++) {
            uuids[i] = users.get(i).getId();
        }

        try {
            //Offset so person is not in the wall
            Vector2 size = hall.getSize().sub(Person.PERSON_SIZE);

            return RegroupUsers.regroupUsers(uuids, mode, (int)size.x, (int)size.y);
        } catch (KeyException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Schließt alle BBB Raume, welche für die Halle geöffnet wurden
     * @param hallId Die ID der Halle
     */
    public static void closeAllBBBRooms(UUID hallId) {
        UUID[] openBreakoutRooms = breakoutList.get(hallId);
        if(openBreakoutRooms != null) {
            for (UUID openBreakoutRoom : openBreakoutRooms) {
                InteractionHandler.sendPostRequest(new InteractionEndMeeting.Request(openBreakoutRoom), InteractionEndMeeting.Response.class);
            }
        }
    }
}
