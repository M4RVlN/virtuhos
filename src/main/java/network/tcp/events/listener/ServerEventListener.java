package network.tcp.events.listener;

import model.*;
import network.database.AnalysisData;
import network.database.DBHandler;
import network.http.InteractionHandler;
import network.http.events.*;
import network.tcp.Server;
import network.tcp.communication.ServerCommunicationTarget;
import network.tcp.events.*;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import util.Vector2;
import util.xml.XMLUtil;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * EventListener des Servers. Verarbeitet eingehende Client-Events.
 * @author Marvin Öhlerking, Nicolas Fröhlich
 */
public class ServerEventListener extends EventListener {

    @Override
    public void onServerJoinRequest(ServerJoinRequest event) {
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;

        try {
            //Login request to interaction
            InteractionLogin.Response login = InteractionHandler.sendPostRequest(new InteractionLogin.Request(event.name, event.password),
                        InteractionLogin.Response.class);

            if(login == null)
                throw new Exception("Could not login");

            //Initialize communication data
            sender.setUserData(login.userID, login.userName, login.displayName, login.isAdmin, login.loginToken);

            //Add new person to house
            House house = Server.INSTANCE.getHouse();
            Person newUser = new Person(sender.id, new Vector2(0, 0), sender.displayName, sender.isAdmin);
            Hall lobby = house.getLobby();
            if (!house.addPerson(newUser, lobby))
                throw new Exception("Can't add the user to lobby");

            //Reply/Broadcast information
            Server.INSTANCE.sendEvent(sender, new ServerJoinReply(null, sender.id, house, sender.isAdmin));

            ArrayList<ServerCommunicationTarget> ignore = new ArrayList<>();
            ignore.add(sender);
            Server.INSTANCE.broadcastEvent(new ServerJoinEvent(null, newUser), ignore);

            //Regroup hall
            regroupHall(house, house.getLobby());
        } catch (Exception e) {
            System.err.println(e.getMessage() + ": " + event.name);
            Server.INSTANCE.disconnect(sender, "Benutzername existiert nicht");
        }
    }

    @Override
    public void onServerLeaveRequest(ServerLeaveRequest event) {
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;
        House house = Server.INSTANCE.getHouse();
        Person person=house.getPerson(sender.id);
        Room oldRoom=house.getRoom(person);
        Server.INSTANCE.disconnect(sender, "");
        if(oldRoom instanceof Hall) {
            regroupHall(house, house.getLobby());
        }
    }

    @Override
    public void onPersonMoveRequest(PersonMoveRequest event) {
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;

        House house = Server.INSTANCE.getHouse();
        Person person = house.getPerson(event.id);

        //User is not allowed to move this person
        if (!sender.id.equals(event.id) && !sender.isAdmin) {
            Server.INSTANCE.sendEvent(sender, new PersonMoveEvent(null, event.id, sender.id, person.getPosition(), false,
                    "Du darfst diese Person nicht bewegen"));
            return;
        }

        //Try to move person
        Room oldRoom = house.getRoom(person);
        Vector2 oldPos = person.getPosition();
        boolean result = house.movePersonChecked(person, event.position);

        //Person could not be moved (collision/capacity)
        if (!result) {
            Server.INSTANCE.sendEvent(sender, new PersonMoveEvent(null, event.id, sender.id, person.getPosition(), false,
                    "Sie versuchen sich in einen vollen Raum oder außerhalb des Gebäudes zu bewegen."));
            return;
        }

        Room newRoom = house.getRoom(person);

        if (newRoom instanceof Hall) {
            //Move person back if trying to move in hall
            if (newRoom.equals(oldRoom)) {
                house.movePerson(person, oldPos);
                Server.INSTANCE.sendEvent(sender, new PersonMoveEvent(null, event.id, sender.id, person.getPosition(), false,
                        "In der Halle ist keine freie Bewegung möglich!"));
                return;
            }

            //Test if user is still in a meeting
            ServerCommunicationTarget target = Server.INSTANCE.getCommunicationTarget(event.id);
            InteractionJoinMeeting.Response info = InteractionHandler.sendPostRequest(
                    new InteractionJoinMeeting.Request(target.displayName, target.id, newRoom.getId(), target.isAdmin, false, target.loginToken),
                    InteractionJoinMeeting.Response.class);

            boolean success = info != null && !info.joinUrl.isEmpty();

            if(!success) {
                house.movePerson(person, oldPos);
                Server.INSTANCE.sendEvent(sender, new PersonMoveEvent(null, event.id, sender.id, person.getPosition(), false,
                        "Das alte Meeting muss beendet werden, bevor der Raum verlassen werden kann!"));
                return;
            }

            regroupHall(house, (Hall)newRoom);
        } else {
            //Open BBB meeting if room changed
            if(newRoom != oldRoom) {
                if(!openBBB(Server.INSTANCE.getCommunicationTarget(event.id), newRoom)) {
                    house.movePerson(person, oldPos);
                    Server.INSTANCE.sendEvent(sender, new PersonMoveEvent(null, event.id, sender.id, person.getPosition(), false,
                            "Das alte Meeting muss beendet werden, bevor der Raum verlassen werden kann!"));
                    return;
                }
            }

            Server.INSTANCE.broadcastEvent(new PersonMoveEvent(null, event.id, sender.id, person.getPosition()));
        }

        if(oldRoom instanceof Hall) {
            regroupHall(house, (Hall)oldRoom);
        }
    }

    @Override
    public void onHallRegroupRequest(HallRegroupRequest event) {
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;

        if(!sender.isAdmin)
            return;

        House house = Server.INSTANCE.getHouse();
        Hall hall = house.getHall(event.hallId);

        HallSortingMode oldMode = hall.getHallMode();
        hall.setHallMode(event.mode);

        boolean result = regroupHall(house, hall);

        if(!result)
            hall.setHallMode(oldMode);
    }

    @Override
    public void onAnalysisDataRequest(AnalysisDataRequest event) {
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;

        AnalysisData data = DBHandler.getAnalysisData();
        if (data == null) {
            System.err.println("Could not fetch analysis data");
            return;
        }

        try {
            Server.INSTANCE.sendEvent(sender, new AnalysisDataReply(null, XMLUtil.parseToXML(data)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onPersonCreateRequest(PersonCreateRequest event) {
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;

        //Check if request is from an admin
        if(!sender.isAdmin)
            return;

        String response = InteractionHandler.sendGetRequest(new InteractionAddUser.Request(event.userName, event.displayName, event.isAdmin, sender.loginToken));
        String username = URLDecoder.decode(event.userName, StandardCharsets.UTF_8);
        Server.INSTANCE.sendEvent(sender, new PersonCreateReply(null, response.equals(username)));
    }

    public void onPersonDeleteRequest(PersonDeleteRequest event) {
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;

        //Check if request is from an admin
        if(!sender.isAdmin)
            return;

        InteractionHandler.sendGetRequest(new InteractionDeleteUser.Request(event.userName, sender.loginToken));
    }

    private boolean openBBB(ServerCommunicationTarget target, Room room) {
        boolean camOff = room instanceof Office && ((Office) room).getType() == OfficeType.CONFERENCE;

        //Let user join lobby
        InteractionJoinMeeting.Response join = InteractionHandler.sendPostRequest(
                new InteractionJoinMeeting.Request(target.displayName, target.id, room.getId(), target.isAdmin, !camOff, target.loginToken),
                InteractionJoinMeeting.Response.class);

        boolean success = join != null && !join.joinUrl.isEmpty();

        if(!success)
            return false;

        //Open BBB in users browser
        Server.INSTANCE.sendEvent(target, new OpenBBBCommand(null, join.joinUrl));
        return true;
    }

    private boolean regroupHall(House house, Hall hall) {
        HallSortingMode hallMode = hall.getHallMode();
        boolean result = HallModule.regroupHall(house, hall.getId(), hallMode);

        if(!result)
            return false;

        Server.INSTANCE.broadcastEvent(new HallRegroupEvent(null, hall));
        return true;
    }

    public void onDocumentCreateRequest(DocumentCreateRequest event) {
        System.out.println("onDocumentCreateRequest");
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;
        House house = Server.INSTANCE.getHouse();
        UUID meetingID = house.getRoom(house.getPerson(event.uploader)).getId();

        InteractionAddDocument.Request request = new InteractionAddDocument.Request(event.data, event.name, event.uploader, meetingID, -1, sender.loginToken);
        InteractionAddDocument.Response response = InteractionHandler.sendPostRequest(request, InteractionAddDocument.Response.class);

        Server.INSTANCE.sendEvent(sender, new DocumentCreateReply(sender, response != null && response.success));
    }

    public void onDocumentDeleteRequest(DocumentDeleteRequest event){
        System.out.println("DocumentDeleteRequest");
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;

        InteractionHandler.sendGetRequest(new InteractionDeleteDocument.Request(event.docId,sender.loginToken));
    }

    public void onDocumentGetRequest(DocumentGetRequest event){
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;

        String response = InteractionHandler.sendGetRequest(new InteractionGetDocDocument.Request(event.docId, sender.loginToken));
        String data, name;
        UUID uploader;
        try {
            JSONObject json = (JSONObject) new JSONTokener(response).nextValue();
            data = json.getString("data");
            name = json.getString("name");
            uploader = UUID.fromString(json.getString("uploader"));

        } catch (Exception e) {
            System.err.println("Error during parsing of response from interaction server");
            return;
        }

        Server.INSTANCE.sendEvent(sender, new DocumentGetReply(sender, data, name, uploader));
    }

    public void onDocumentGetInfoRequest(DocumentGetInfoRequest event){
        ServerCommunicationTarget sender = (ServerCommunicationTarget) event.sender;

        String response = InteractionHandler.sendGetRequest(new InteractionGetDocInfoDocument.Request(sender.loginToken));
        List<String> docNames = new ArrayList<>();
        List<Integer> docIds = new ArrayList<>();
        try {
            JSONArray json = (JSONArray) new JSONTokener(response).nextValue();
            for (int i = 0; i < json.length(); i++) {
                JSONObject doc = json.getJSONObject(i);
                if(doc.getString("meetingId").equals(event.meetingId.toString())) {
                    docNames.add(doc.getString("name"));
                    docIds.add(doc.getInt("id"));
                }
            }
        } catch (Exception e) {
            //Not a valid response
            System.err.println("Error during parsing of response from interaction server");
            return;
        }
        String[] docNamesArray = new String[docNames.size()];
        int[] docIdsArray = new int[docNames.size()];
        for(int i = 0; i< docIds.size();i++)
            docIdsArray[i] = docIds.get(i);

        Server.INSTANCE.sendEvent(sender, new DocumentGetInfoReply(sender, docNames.toArray(docNamesArray), docIdsArray));
    }
}
