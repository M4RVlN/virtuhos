package util;

public class Bounds {
    public Vector2 min;
    public Vector2 max;

    public Bounds(Vector2 min, Vector2 max) {
        this.min = min;
        this.max = max;
    }
}
