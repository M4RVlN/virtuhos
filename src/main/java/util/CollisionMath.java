package util;

/**
 * Eine Klasse mit statischen Funktionen, um Kollision zwischen Rechtecken und Rechtecken
 * oder Rechtecken und Kreisen zu bestimmen.
 * Außerdem kann mit der goldenen Spirale Klasse Punkt auf der goldenen Spirale erzeugt werden.
 * @author Nicolas Fröhlich
 */
public final class CollisionMath {
    private static CollisionMath instance = new CollisionMath();

    private CollisionMath() {
    }

    /**
     * Test ob es zwischen einem Rechteck und einem Kreis eine Überschneidung gibt
     *
     * @param pos_C    Position des Kreies
     * @param radius_C Radius des Kreies
     * @param pos_R    Position der linke oberen Ecke des Rechecks
     * @param siz_R    Größe des Rechtecks
     * @return boolean true, falls es eine Überschneidung gibt, sonst false
     */
    public static boolean overlapCircleRectangle(Vector2 pos_C, double radius_C, Vector2 pos_R, Vector2 siz_R) {
        //Außerhalb
        boolean inLowerBound = pos_C.x >= pos_R.x - radius_C && pos_C.y >= pos_R.y - radius_C;
        boolean inUpperBound = pos_C.x <= siz_R.x + pos_R.x + radius_C && pos_C.y <= siz_R.y + pos_R.y + radius_C;
        if (!(inLowerBound && inUpperBound)) return false;

        //Horizontal erweitert
        boolean inLowerBound2 = pos_C.x >= pos_R.x - radius_C && pos_C.y >= pos_R.y;
        boolean inUpperBound2 = pos_C.x <= siz_R.x + pos_R.x + radius_C && pos_C.y <= siz_R.y + pos_R.y;
        if (inLowerBound2 && inUpperBound2) return true;

        //Vertical erweitert
        boolean inLowerBound3 = pos_C.x >= pos_R.x && pos_C.y >= pos_R.y - radius_C;
        boolean inUpperBound3 = pos_C.x <= siz_R.x + pos_R.x && pos_C.y <= siz_R.y + pos_R.y + radius_C;
        if (inLowerBound3 && inUpperBound3) return true;

        //Ecken
        Vector2[] points = {pos_R, pos_R.add(new Vector2(siz_R.x, 0)), pos_R.add(new Vector2(0, siz_R.y)), pos_R.add(siz_R)};
        for (Vector2 v : points)
            if (pos_C.sub(v).magnitude() < radius_C)
                return true;
        return false;
    }

    /**
     * Test, on es zwischen zwei Kreisen eine Überschneidung gibt
     *
     * @param pos_C1 Position des ersten Kreises
     * @param rad_C1 Radius des ersten Kreises
     * @param pos_C2 Position des zweiten Kreises
     * @param rad_C2 Radius des zweiten Kreises
     * @return boolean true, falls es eine Überschneidung gibt, sonst false
     */
    public static boolean overlapCircleCircle(Vector2 pos_C1, double rad_C1, Vector2 pos_C2, double rad_C2) {
        double distance = pos_C1.sub(pos_C2).magnitude();
        return distance < rad_C1 + rad_C2;
    }

    /**
     * Erzeugt eine GoldenSpiral Instanze
     *
     * @param precision Die Genauigkeit, mit der die GoldenSpiral Punkte erzeugen soll
     * @return GoldenSpiral Das GoldenSpiral Objekt
     */
    public static GoldenSpiral newGoldenSpiral(int precision) {
        return instance.new GoldenSpiral(precision);
    }

    /**
     * Erzeugt eine GoldenSpiral Instanze
     *
     * @return GoldenSpiral Das GoldenSpiral Objekt
     */
    public static GoldenSpiral newGoldenSpiral() {
        return instance.new GoldenSpiral(512);
    }

    /**
     * Die GoldenSpiral Klasse erzeugt Punkte entlang der Goldenen Spirale
     *
     * @see <a href="https://en.wikipedia.org/wiki/Golden_spiral">Golden spiral</a>
     */
    public class GoldenSpiral {
        private final double golden_angle = Math.PI * (3 - Math.sqrt(5));
        private final int maxRadius = 100;
        private final double precision;

        private int i = 0;

        private GoldenSpiral(int p) {
            this.precision = Math.sqrt(p);
        }

        /**
         * Erzeugt den nächsten Punkt entlang der Goldenen Spirale
         *
         * @return Vector2 Der nächste Punkt
         */
        public Vector2 next() {
            double theta = i * golden_angle;
            double r = Math.sqrt(i++) / precision;

            return new Vector2(r * Math.cos(theta) * maxRadius, r * Math.sin(theta) * maxRadius);
        }
    }
}
