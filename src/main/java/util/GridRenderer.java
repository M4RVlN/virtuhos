package util;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;

public class GridRenderer extends Pane {
    public GridRenderer(Vector2 size, int spacing, String gridColor, String backgroundColor) {
        Rectangle horizontal = new Rectangle();
        horizontal.setLayoutX(0);
        horizontal.setLayoutY(0);
        horizontal.setWidth(size.x);
        horizontal.setHeight(size.y);
        horizontal.setStyle(String.format("-fx-fill: linear-gradient(from 0px %dpx to  0px 0px, repeat, %s 5%%, transparent 5%%);", spacing, gridColor));
        this.getChildren().add(horizontal);

        Rectangle vertical = new Rectangle();
        vertical.setLayoutX(0);
        vertical.setLayoutY(0);
        vertical.setWidth(size.x);
        vertical.setHeight(size.y);
        vertical.setStyle(String.format("-fx-fill: linear-gradient(from %dpx 0px to 0px  0px, repeat, %s 5%%, transparent 5%%);", spacing, gridColor));
        this.getChildren().add(vertical);

        this.setStyle(String.format(("-fx-background-color: %s;"), backgroundColor));
    }

    public void setSize(Vector2 size) {
        for(Node node : getChildren()) {
            Rectangle rect = (Rectangle) node;
            rect.setWidth(size.x);
            rect.setHeight(size.y);
        }
        setPrefSize(size.x, size.y);
    }
}
