package util;

import com.github.sarxos.webcam.Webcam;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Line;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;

/**
 * Class was delivered by Interaction-3 to Editor.
 * class to check if a user has necessary hardware to join the VirtuHOS
 * @Author Interaktion -3
 */
public class HardwareCheck {
    /**
     * @return Mic available?
     */
    public static boolean checkMic(){
        for (Mixer.Info mixerInfo : AudioSystem.getMixerInfo()) {
            for (Line.Info lineInfo : AudioSystem.getMixer(mixerInfo).getSourceLineInfo()) {
                if (lineInfo.toString().toLowerCase().contains("microphone")) return true;
            }
        }
        return false;
    }

    /**
     * Checks for Audio output devices. Returns true if it finds one
     * @return true or false based on the users output devices
     */
    public static boolean checkAudio() {
        final Mixer.Info[] devices = AudioSystem.getMixerInfo();
        final Line.Info sourceInfo = new Line.Info(SourceDataLine.class);
        for (int i = 0; i < devices.length; ++i) {
            final Mixer.Info mixerInfo = devices[i];
            final Mixer mixer = AudioSystem.getMixer(mixerInfo);
            if (mixer.isLineSupported(sourceInfo)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @return Cam available?
     */
    public static boolean checkCam(){
        return Webcam.getDefault() != null;
    }
}
