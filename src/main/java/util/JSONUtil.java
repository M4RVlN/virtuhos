package util;

public class JSONUtil {
    public static <T> T getOrNull(Object obj, Class<T> clazz) {
        return obj.getClass().equals(clazz) ? clazz.cast(obj) : null;
    }
}
