package util;

public class MathUtil {

    /**
     * Interpoliert zwischen a und b mit f
     * @param a Anfangswert
     * @param b Endwert
     * @param f Interpolationswert im Intervall [0, 1]
     * @return Interpolierter Wert zwischen den beiden Zahlen
     */
    public static double lerp(double a, double b, double f) {
        return a + MathUtil.clamp(f, 0, 1) * (b - a);
    }

    /**
     * Begrenzt eine Zahl zwischen min und max
     * @param val Der Wert
     * @param min Minimalwert
     * @param max Maximalwert
     * @return Der Wert in den Grenzen
     */
    public static double clamp(double val, double min, double max) {
        return Math.max(min, Math.min(max, val));
    }
}
