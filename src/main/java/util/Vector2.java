package util;

public class Vector2 {
    public static final Vector2 zero = new Vector2(0, 0);
    public static final Vector2 up = new Vector2(0, 1);
    public static final Vector2 down = new Vector2(0, -1);
    public static final Vector2 left = new Vector2(-1, 0);
    public static final Vector2 right = new Vector2(1, 0);
    public static final Vector2 one = new Vector2(1, 1);

    public double x;
    public double y;

    public Vector2() {
        this.x = 0.0f;
        this.y = 0.0f;
    }

    public Vector2(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2 add(Vector2 other) {
        return new Vector2(this.x + other.x, this.y + other.y);
    }

    public Vector2 sub(Vector2 other) {
        return new Vector2(this.x - other.x, this.y - other.y);
    }

    public Vector2 mul(double other) {
        return new Vector2(this.x * other, this.y * other);
    }

    public Vector2 div(double other) {
        return new Vector2(this.x / other, this.y / other);
    }

    public void normalize() {
        Vector2 norm = this.normalized();
        this.x = norm.x;
        this.y = norm.y;
    }

    public Vector2 normalized() {
        double m = magnitude();
        return new Vector2(x / m, y / m);
    }

    public double magnitude() {
        return Math.sqrt((x * x) + (y * y));
    }

    @Override
    public Vector2 clone() {
        return new Vector2(x, y);
    }

    @Override
    public boolean equals(Object obj) {
        try {
            return equals((Vector2)obj);
        } catch (Exception ex) {
            return false;
        }
    }

    // Compare two vectors
    public boolean equals(Vector2 other) {
        return (Math.abs(this.x - other.x) < Float.MIN_VALUE) && (Math.abs(this.x - other.x) < Float.MIN_VALUE);
    }

    @Override
    public String toString() {
        return String.format("(%g, %g)", x, y);
    }
}
