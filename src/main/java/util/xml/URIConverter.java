package util.xml;

import java.net.URI;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

public class URIConverter implements Converter<URI> {

    @Override
    public URI read(InputNode node) throws Exception {
        return URI.create(node.getValue());
    }

    @Override
    public void write(OutputNode node, URI value) {
        node.setValue(value.toString());
    }
}
