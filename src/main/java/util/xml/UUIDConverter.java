package util.xml;

import java.util.UUID;
import org.simpleframework.xml.convert.Converter;
import org.simpleframework.xml.stream.InputNode;
import org.simpleframework.xml.stream.OutputNode;

public class UUIDConverter implements Converter<UUID> {

    @Override
    public UUID read(InputNode node) throws Exception {
        return UUID.fromString(node.getValue());
    }

    @Override
    public void write(OutputNode node, UUID value) {
        node.setValue(value.toString());
    }
}
