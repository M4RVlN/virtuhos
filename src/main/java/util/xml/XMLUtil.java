package util.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URI;
import java.util.UUID;
import javafx.stage.FileChooser;
import model.House;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.convert.Registry;
import org.simpleframework.xml.convert.RegistryStrategy;
import org.simpleframework.xml.core.Persister;
import org.simpleframework.xml.strategy.Strategy;
import window.SceneManager;

/**
 * Klasse um Gebäude im XML-Format laden und speichern zu können. Nutzer kann die Dateien aus einem beliebigen
 * Verzeichnis laden und in ein beliebiges Verzeichnis speichern.
 * @author Philipp Brandes
 */
public final class XMLUtil
{
    private static Serializer serializer;

    /**
     * Konfiguriert den FileChooser für das Laden der Datei,
     * sodass alle Dateitypen oder nur das XML-Format angezeigt wird.
     * @param fileChooser Instanz des FileChoosers
     */
    private static void configureFileChooserLoad(FileChooser fileChooser) {
        fileChooser.setTitle("Gebäude verwalten");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Alle Dateien", "*.*"),
                new FileChooser.ExtensionFilter("XML", "*.xml")
        );

        File directory = new File(System.getProperty("user.dir"));
        if(directory.canRead())
            fileChooser.setInitialDirectory(directory);
    }

    /**
     * Konfiguriert den FileChooser für das Speichern einer Datei, sodass nur das XML-Format ausgewählt werden kann
     * @param fileChooser Instant des FileChoosers
     */
    private static void configureFileChooserSave(FileChooser fileChooser) {
        fileChooser.setTitle("Gebäude verwalten");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("XML", "*.xml")
        );

        File directory = new File(System.getProperty("user.dir"));
        if(directory.canRead() && directory.canWrite())
            fileChooser.setInitialDirectory(directory);
    }

    /**
     * Wandelt das Haus in das XML-Format um
     * @param house Aktuelle Instanz des Hauses
     * @param filedst Gewünschter Speicherort der XML
     * @throws Exception
     */
    public static void houseToXML(House house, File filedst) throws Exception {
        if(filedst == null)
            throw new Exception("No file destination selected");

        getSerializer().write(house, filedst);
    }

    /**
     * Öffnet den FileChooser um den gewünschten Speicherort zu initialisieren. Und speichert die XML.
     * @param house das in XML-Datei umgewandelt werden soll
     * @throws Exception
     */
    public static void houseToXML(House house) throws Exception {
        FileChooser fileChooser = new FileChooser();
        configureFileChooserSave(fileChooser);
        File filedst = fileChooser.showSaveDialog(SceneManager.getStage());
        houseToXML(house, filedst);
    }

    /**
     * Wandelt übergebenen InputStream in ein House-Objekt um.
     * @param filesrc Inputstream der Datei
     * @return Objekt vom Typ Haus
     * @throws Exception
     */
    public static House houseFromXML(InputStream filesrc) throws Exception {
        if(filesrc == null)
            return null;

        return getSerializer().read(House.class, filesrc);
    }

    /**
     * Prüft ob eingegebene Datei ungleich null ist.
     * @param filesrc XML-Datei des Hauses
     * @return InputStream der aus Datei erstellt wird
     * @throws Exception
     */
    public static House houseFromXML(File filesrc) throws Exception {
        if(filesrc == null)
            return null;

        return houseFromXML(new FileInputStream(filesrc));
    }

    /**
     * Öffnet den Filechooser in dem der Nutzer die gewünschte XML-Datei des Hauses auswählt
     * @return Ausgewählte Datei
     * @throws Exception
     */
    public static House houseFromXML() throws Exception {
        FileChooser fileChooser = new FileChooser();
        configureFileChooserLoad(fileChooser);
        File filesrc = fileChooser.showOpenDialog(SceneManager.getStage());
        return houseFromXML(filesrc);
    }

    /**
     * Lädt das spezifizierte Default Template
     * @return House-Objekt
     * @throws Exception
     */
    public static House defaultHouseTemplate() throws Exception {
        return houseFromXML(ClassLoader.getSystemResourceAsStream("templates/TemplateHalle.xml"));
    }

    /**
     * Parsed XML in Objekt
     * @param xml XML die geparsed werden soll
     * @param type Typ aus dem geparsed werden soll
     * @param <T>
     * @return Objekt aus der XML
     * @throws Exception
     */
    public static <T> T parseFromXML(String xml, Class<? extends T> type) throws Exception {
        return getSerializer().read(type, xml);
    }

    /**
     * Parsed Objekt in XML
     * @param obj Objekt das in XML geparsed werden soll
     * @param <T>
     * @return Objekt als String
     * @throws Exception
     */
    public static <T> String parseToXML(T obj) throws Exception {
        StringWriter string = new StringWriter();

        getSerializer().write(obj, string);
        return string.toString();
    }

    /**
     *Gibt einen Serializer zurück um Objekte in XML zu serialisieren/deserialisieren.
     * @return Serializer
     * @throws Exception
     */
    private static Serializer getSerializer() throws Exception {
        if(serializer == null) {
            Registry registry = new Registry();
            Strategy strategy = new RegistryStrategy(registry);
            serializer = new Persister(strategy);

            registry.bind(UUID.class, UUIDConverter.class);
            registry.bind(URI.class, URIConverter.class);
        }
        return serializer;
    }
}
