package window;

import controller.Controller;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.util.ArrayList;
import java.util.Objects;

public class SceneManager {
    private static FXMLLoader loader = new FXMLLoader();
    private static Stage stage;

    private static final ArrayList<EventHandler<WindowEvent>> sceneChangeStartHandler = new ArrayList<>();

    public static void Init(Stage stage, String title, Scenes startScene) {
        SceneManager.stage = stage;
        SceneManager.stage.setTitle(title);
        try {
            String[] icons = new String[] {
                    "images/icon/icon_256px.png",
                    "images/icon/icon_96px.png",
                    "images/icon/icon_48px.png",
                    "images/icon/icon_32px.png",
                    "images/icon/icon_16px.png"
            };
            for(String s : icons) {
                SceneManager.stage.getIcons().add(new Image(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(s))));
            }
        } catch (Exception ignore) { }
        loadScene(startScene);
    }

    public static Stage getStage() {
        return stage;
    }

    public static Scene getScene() {
        return stage.getScene();
    }

    public static Parent getRoot() {
        return getScene().getRoot();
    }

    public static <T extends Controller> T getController(Class<? extends T> type) {
        return type.cast(loader.getController());
    }

    public static void loadScene(Scenes scene) {
        try {
            triggerStartEvent();

            loader = new FXMLLoader(ClassLoader.getSystemResource("view/" + scene.fileName + ".fxml"));
            Parent root = loader.load();
            Scene current = getScene();
            if(current != null)
                getStage().setScene(new Scene(root, current.getWidth(), current.getHeight()));
            else
                getStage().setScene(new Scene(root));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void show() {
        if (stage != null)
            stage.show();
        else
            System.err.println("SceneManager is not initialized");
    }

    public static void hide() {
        if (stage != null)
            stage.hide();
        else
            System.err.println("SceneManager is not initialized");
    }

    public static void setOnSceneChangedStart(EventHandler<WindowEvent> eventHandler) {
        sceneChangeStartHandler.add(eventHandler);
    }

    public static void removeOnSceneChangedStart(EventHandler<WindowEvent> eventHandler) {
        sceneChangeStartHandler.remove(eventHandler);
    }

    private static void triggerStartEvent() {
        int size = sceneChangeStartHandler.size();
        for (int i = 0; i < size; i++) {
            sceneChangeStartHandler.get(i).handle(new WindowEvent(getScene().getWindow(), EventType.ROOT));

            i -= size - sceneChangeStartHandler.size();
            size = sceneChangeStartHandler.size();
        }
    }
}
