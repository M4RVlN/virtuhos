package window;

public enum Scenes {
    MAIN_MENU("MainMenuView"),
    LOGIN("LoginView"),
    EDITOR("EditorView"),
    ADMIN("AdminView"),
    USER("UserView"),
    SERVER("ServerView");

    final String fileName;

    Scenes(String fileName){
        this.fileName = fileName;
    }
}
